<?php 
$isLoggedIn = isset($_SESSION['username'] ) && $_SESSION["loggedin"] === true;
$isAdmin = isset($_SESSION['role'] ) && $_SESSION["role"] === 'admin';
?>

<nav id="navbar" class="flex bg-secondary items-center sticky justify-between flex-wrap select-none p-2 my-auto w-full top-0 duration-500 ease-in-out">

<!-- LEFT SIDE -->
<div class="flex items-center flex-shrink-0 mr-2">
    <?php if($currentPage === 'Home' && (!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] === false)) { ?>
        <form id="login-form" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post" class="flex justify-start flex-col xs:flex-col sm:flex-col md:flex-row">
            <div class="flex flex-row w-64 mr-2 h-10 w-full">
                <select id="courseOption" value="course" class="courseList text-primaryText cursor-pointer block appearance-none w-full bg-secondary border border-gray-400 hover:border-gray-500 px-4 py-2 pr-8 rounded shadow leading-tight focus:outline-none focus:shadow-outline">
                </select>
                <select id="usernameOption" value="username" class="userList text-primaryText cursor-pointer block appearance-none w-full bg-secondary border border-gray-400 hover:border-gray-500 px-4 py-2 pr-8 rounded shadow leading-tight focus:outline-none focus:shadow-outline">
                </select>
                <input id="hiddenUsername" hidden name="login_qwerty_username" type="text"/>
            </div>
            <div class="relative">
                <input id="loginPassword" name="login_qwerty_password" class="w-64 h-10 mr-2 mt-2 xs:mt-2 sm:mt-2 md:mt-0 appearance-none block bg-ternary text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" id="grid-password" type="password" placeholder="Password">
                <i id="togglePasswordVisibility" class="hidden absolute h-full md:flex cursor-pointer text-secondaryText text-2xl items-center top-0 w-10 right-0 fa fa-eye-slash" aria-hidden="true"></i>
            </div>
            <input id="loginSubmit" type="submit" value="Login" class="mt-2 xs:mt-2 sm:mt-2 md:mt-0 button-primary text-primaryText" />
        </form>
        <?php } ?>
        <?php if($currentPage === 'Home' && $isLoggedIn) { ?>
            <div class="my-auto">
            <h3 id="welcomeMsg" class="text-primaryText slideFadeRight font-bold text-lg md:text-xl lg:text-2xl">Welcome, <?php echo htmlentities($_SESSION["full_name"]); ?></h3>
            </div>
        <?php } ?>
        <?php if($currentPage !== 'Home') { ?>
        <a href="index.php" class="ml-2 button-primary text-primaryText">Home</a>
        <?php } ?>
</div>

<div class="block lg:hidden">
			<button id="nav-toggle" class="flex items-center px-3 py-2 border rounded text-gray-500 cursor-pointer text-primaryText hover:text-secondaryText">
                <i class="fa fa-bars" aria-hidden="true"></i>
			</button>
		</div>

        <div class="w-full flex-grow lg:flex lg:items-center lg:w-auto hidden lg:block pt-6 lg:pt-0" id="nav-content">
			<ul class="list-reset lg:flex justify-end flex-1 items-center">
                <?php if($isLoggedIn) { ?>
				<li class="mb-2 lg:mr-2 lg:mb-0 h-10 lg:h-auto mx-auto lg:mx-0 sm:w-1/2 xs:w-full lg:w-auto">
                    <a href="profile.php" class="flex button-primary text-primaryText">Profile</a>
                </li>

                    <?php require_once('php/dbconnect.php');?>
                    <?php if(isProjectOpen($mysqli) != "false") { ?>
                        <li class="mb-2 lg:mr-2 lg:mb-0 h-10 lg:h-auto mx-auto lg:mx-0 sm:w-1/2 xs:w-full lg:w-auto">
                        <a href="vote.php" class="flex button-primary text-primaryText">Vote</a>
                    </li>
                    <?php } ?>
                <?php } ?>

				<li class="mb-2 lg:mr-2 lg:mb-0 h-10 lg:h-auto mx-auto lg:mx-0 sm:w-1/2 xs:w-full lg:w-auto">
					<a href="viewResults.php" class="flex button-primary text-primaryText">View Results</a>
				</li>
				<li class="mb-2 lg:mr-2 lg:mb-0 h-10 lg:h-auto mx-auto lg:mx-0 sm:w-1/2 xs:w-full lg:w-auto">
                    <a href="projectViewer.php" class="flex button-primary text-primaryText">View Projects</a>
                </li>
                <?php if($isAdmin) { ?>
				<li class="mb-2 lg:mr-2 lg:mb-0 h-10 lg:h-auto mx-auto lg:mx-0 sm:w-1/2 xs:w-full lg:w-auto">
                    <a href="admin.php" class="flex button-primary text-primaryText">Admin</a>
                </li>
                <?php } ?>
                <?php if($isLoggedIn) { ?>
				<li class="h-10 lg:ml-2 mb-2 lg:mr-2 lg:mb-0 h-10 lg:h-auto mx-auto lg:mx-0 sm:w-1/2 xs:w-full lg:w-auto">
                    <a id="logoutButton" href="logout.php" class="flex button-primary text-primaryText">Logout</a>
                </li>
                <?php } ?>
			</ul>
		</div>
    </nav>
    
    <script>
		//Javascript to toggle the menu
		document.getElementById('nav-toggle').onclick = function(){
			document.getElementById("nav-content").classList.toggle("hidden");
		}
	</script>