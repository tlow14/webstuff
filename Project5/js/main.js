/**
 * Creates an alert with a common style. The alert has a close button and is automaticallly removed
 * after a period of time.
 * 
 * @param {*} title 
 * @param {*} description 
 * @param {*} $prepend -- JQuery div that it is prepended to
 * @param {*} isNotice -- If it is a notice then it will be green, otherwise red
 */
function createAlert(title, description, $prepend, isNotice) {
    var color = "red-400";
    if (isNotice) {
        color = "green-400";
    }

    var $alert = $("<div></div>").attr("id", "alert");
    $alert.addClass("bg-" + color + " z-10 border border-" + color + " text-white px-4 py-3 rounded relative my-0 flex flex-row justify-between");
    var $div1 = $("<div></div>");
    $div1.append($("<p></p>").addClass("font-bold").text(title));
    $div1.append($("<p></p>").addClass("text-sm").text(description));
    var $div2 = $("<div></div>");

    var $close = $('<i class="fa fa-times-circle text-lg cursor-pointer" aria-hidden="true"></i>').attr("onclick", "this.parentElement.parentElement.style.display='none';");
    $close.addClass("m-4");

    $div2.append($close);

    $alert.append($div1);
    $alert.append($div2);
    $prepend.prepend($alert);

    setTimeout(function() {
        $alert.remove();
    }, 7500);
}

/**
 * All projects are in a local form aka. Project 1-7
 * however the DB stores them with an auto-incrementing ID.
 * This method converts the localID 1-7 to the actual Project ID
 */
function getProjectIDFromLocalId(projectNum, callback) {
    if (projectNum == "null") return callback(projectNum);
    $.getJSON("php/getProjects.php", function(projectIdList) {
        var projId = -1;
        for (var i in projectIdList) {
            var projName = projectIdList[i].name;
            if (projName == "Project " + projectNum) {
                projId = projectIdList[i].id;
                break;
            }
        }
        callback(projId);
    });
}

/**
 * Gets the project data for the give LocalProjectNum
 * 
 * @param {*} projectNum 
 * @param {*} callback 
 */
function getProjectData(projectNum, callback) {
    // Project name is the relative project name, so we need to find the project ID for the given course
    // Watch out, projectNum can be null
    // php should handle this and select the current open project

    getProjectIDFromLocalId(projectNum, function(projId) {
        $.post("php/getProjectResults.php", { "projectID": projId }, function(data) {
            callback(JSON.parse(data));
        }).fail(function() {
            createAlert("Alert!", "There's been an error loading the project data.", $("body"), false);
        });
    });
}

/**
 * Loads a Chart.js chart on the given $(canvasId). The project num is the localProjectNumber
 * 
 * @param {*} canvasId 
 * @param {*} projectNum 
 */
// This has to be a variable outside of the loadChart function. https://github.com/chartjs/Chart.js/issues/2126
var currentChart;

function loadChart(canvasId, projectNum) {

    var $canvas = $(canvasId);

    /**
     * This is when there are no results for the given project, this can happen on the View Results page.
     * Instead of showing an empty canvas, this replaces it with a message
     */
    function noResults() {
        var $notice = $("<h2></h2>").text("Currently, there are no project results.");
        $notice.addClass("text-ternaryText text-4xl text-center font-bold");
        $canvas.hide();
        $canvas.parent().html($notice);
    }

    // These modifiers are what are used in the scoring. There are also modifiers in getAllResults.php, so watch out for that.
    let GOLD_MODIFIER = 4;
    let SILVER_MODIFIER = 2;
    let BRONZE_MODIFIER = 1;

    // Get the project data and then create the Chart
    getProjectData(projectNum, function(projectData) {
        if (projectData == null || projectData.length == 0) {
            noResults();
        } else {
            var userList = [];
            var goldList = [];
            var silverList = [];
            var bronzeList = [];

            for (var i in projectData) {
                var user = projectData[i].full_name;

                // Get the individual vote and scale it by the modifier
                var gold = parseInt(projectData[i].gold) * GOLD_MODIFIER;
                var silver = parseInt(projectData[i].silver) * SILVER_MODIFIER;
                var bronze = parseInt(projectData[i].bronze) * BRONZE_MODIFIER;

                userList.push(user);
                goldList.push(gold);
                silverList.push(silver);
                bronzeList.push(bronze);
            }

            // SORT DATA
            var arrayOfObj = userList.map(function(d, i) {
                return {
                    user: d,
                    gold: goldList[i] || 0,
                    silver: silverList[i] || 0,
                    bronze: bronzeList[i] || 0,
                };
            });
            var sortedArrayOfObj = arrayOfObj.sort(function(a, b) {
                return (b.gold + b.silver + b.bronze) < (a.gold + a.silver + a.bronze) ? -1 : 1;
            });

            var sortedUserList = [];
            var sortedGoldList = [];
            var sortedSilverList = [];
            var sortedBronzeList = [];
            sortedArrayOfObj.forEach(function(d) {
                sortedUserList.push(d.user);
                sortedGoldList.push(d.gold);
                sortedSilverList.push(d.silver);
                sortedBronzeList.push(d.bronze);
            });

            // Remove the last iFrame if one exists
            $(".chartjs-hidden-iframe").remove();

            // Create the new Chart
            Chart.defaults.global.defaultFontColor = 'white';

            // If there is already a chart, then destroy the last one
            if (currentChart) {
                currentChart.destroy();
            }
            currentChart = new Chart($canvas, {
                // Makes the chart horizontal
                type: 'horizontalBar',
                data: {
                    labels: sortedUserList,
                    datasets: [{
                            label: 'Gold',
                            data: sortedGoldList,
                            fill: true,

                            // A single BG color will go for all the users
                            backgroundColor: '#ffd700',
                        },
                        {
                            label: 'Silver',
                            data: sortedSilverList,
                            backgroundColor: '#c0c0c0',
                        },
                        {
                            label: 'Bronze',
                            data: sortedBronzeList,
                            backgroundColor: '#cd7f32',
                        }
                    ]
                },
                options: {
                    scales: {
                        yAxes: [{
                            stacked: true,
                            ticks: {
                                beginAtZero: true
                            },
                            gridLines: {
                                // Hide the Grid lines
                                display: false
                            }
                        }],
                        xAxes: [{
                            gridLines: {
                                // Hide the Grid lines
                                display: false
                            },
                            stacked: true,
                        }]
                    },
                    animation: {
                        // Remove the animation, this is bad when the chart is reloaded because the bars
                        // stretch across the screen over and over
                        duration: 0,
                    },
                    tooltips: {
                        callbacks: {

                            // We need to replace the label with our own label that counts how many gold/silver/bronze
                            // each user got. This is because the data is stored in the Chart as just a value scaled
                            // when showing the label to the user we need to remove the scaling. So they know how many
                            // of each medal they got
                            label: function(tooltipItem, data) {
                                var label = data.datasets[tooltipItem.datasetIndex].label || '';
                                var medalValue = parseInt(tooltipItem.xLabel);

                                if (label == "Gold") {
                                    medalValue /= GOLD_MODIFIER;
                                } else if (label == "Silver") {
                                    medalValue /= SILVER_MODIFIER;
                                } else {
                                    medalValue /= BRONZE_MODIFIER;
                                }
                                label += " " + medalValue;
                                return label;
                            }
                        }
                    }
                }
            });
        }
    });
}

/**
 * Retrieves all the comments for a given project. This works in conjuction with comments.php
 * this is the comments that are used across the site.
 * 
 * @param {*} projectId 
 * @param {*} commentWrapperId 
 */
function populateCommentList(projectId, commentWrapperId) {
    var $commentWrapper = $(commentWrapperId);
    var $templateComment = $("#templateComment");

    getComments(projectId, function(comments) {
        console.log("Refreshing Comments...");
        $commentWrapper.css("min-height", $commentWrapper.outerHeight());
        $commentWrapper.children().each(function(idx, el) {
            if ($(el).attr("id") != "templateComment") {
                $(el).remove();
            }
        });
        var lastTeamId = -1;

        var $template = $templateComment.clone().removeClass("hidden");
        var $commentList = $template.find("#commentList").eq(0);
        var $commentNameList = $template.find("#commentNameList").eq(0);
        var $profileIconList = $template.find("#profileIconList").eq(0);

        var commentsAlreadyAdded = [];
        var usersAlreadyAdded = [];

        // Loop through each comment
        for (var i in comments) {

            // If it is a new team then we need to reset all the variables above.
            // This creates a new comment
            if (lastTeamId != comments[i].team_id) {
                $template = $templateComment.clone().removeClass("hidden").attr("id", "teamId" + team_id);
                $template.attr('data-team_id', parseInt(team_id));
                $commentList = $template.find("#commentList").eq(0);
                $commentNameList = $template.find("#commentNameList").eq(0);
                $profileIconList = $template.find("#profileIconList").eq(0);
                $commentList.empty();
                $commentNameList.text(' ');
                $profileIconList.empty();
                commentsAlreadyAdded = [];
                usersAlreadyAdded = [];
            }

            var comment = comments[i].comment;
            var full_name = comments[i].full_name;
            var iconUrl = comments[i].iconUrl;
            var team_id = comments[i].team_id;

            // Comments can come in at a very strange order because we are getting comments for a team
            // and each user is associated with the comment. This makes sure that a comment is only added one time
            if (commentsAlreadyAdded.indexOf(comment) == -1) {
                var $p = $("<p></p>").text(comment).addClass("break-words");

                commentsAlreadyAdded.push(comment);
                $commentList.append($p);

            }

            // Like the comment above we can get the same comment attached to multiple users,
            // this checks the users and makes sure the users being added to the comments are unique
            if (usersAlreadyAdded.indexOf(full_name) == -1) {

                // This adds the spacer ", " eg. Nathan O'Neel, Tom Lowry
                if (usersAlreadyAdded.length > 0) {
                    $commentNameList.text($commentNameList.text() + ", ");
                }
                $commentNameList.text($commentNameList.text() + full_name);
                usersAlreadyAdded.push(full_name);

                // Add the image of the user, the defualt should be images/noUserIcon.png in the DB now.
                var $image = $("<img/>").addClass("h-16 w-16 mr-2 rounded-full");
                if (iconUrl) {
                    $image.attr("src", iconUrl);
                } else {
                    $image.attr("src", "images/noUserIcon.png");
                }
                $profileIconList.append($image);
            }

            // Keep track of what the current/last teamId are so we can create new comment boxes for each one
            if (lastTeamId != team_id) {
                $commentWrapper.append($template);
            }
            lastTeamId = team_id;

            // If there are more then one user in the comment box, then center it. Just makes it look slightly better
            // if it is a single user it is left justified by default
            if (usersAlreadyAdded.length > 1) {
                $profileIconList.addClass("justify-center");
                $commentNameList.addClass("text-center");
            } else {
                $commentNameList.remove();
                $commentNameList.addClass("ml-2");
                $profileIconList.append($commentNameList);
            }
        }
    });
}

/**
 * This getUsers is only for the current course specified by courseId
 * This is used in the dropdown on the main page index.php for the user selection
 * after they select a course. We only need the specific users for that course.
 * 
 * @param {*} courseId 
 * @param {*} callback 
 */
function getUsersForCourse(courseId, callback) {
    $.post("php/getUsersForCourse.php", { "course_id": courseId }, function(userData) {
        userData = JSON.parse(userData);
        userData.sort(function(a, b) {
            // In a try catch just in case the user has a special case last name, that won't be caught
            try {
                var lastNameA = a.full_name.split(" ").splice(-1)[0].toLowerCase();
                var lastNameB = b.full_name.split(" ").splice(-1)[0].toLowerCase();

                if (!lastNameA) return 1;
                if (!lastNameB) return -1;
                // console.log("1: " + lastNameA + " 2: " + lastNameB);
                return (lastNameA < lastNameB) ? -1 : 1;
            } catch (e) {
                return 1;
            }
        });
        callback(userData);
    });
}

/**
 * Needed to get usernames and sort by user names (ex. used in edit full name on admin page)
 * @param {*} callback 
 */

function getUsernamesForCourse(callback) {
    $.post("php/getUsersForCourse.php", function(userData) {
        userData = JSON.parse(userData);
        userData.sort(function(a, b) {
            return (a.username < b.useName) ? -1 : 1;
        });
        callback(userData);
    });
}

/**
 * Gets the comments associated with a given project
 * 
 * @param {*} project_id 
 * @param {*} callback 
 */
function getComments(project_id, callback) {
    $.post("php/getComments.php", { "project_id": project_id }, function(commentList) {
        callback(JSON.parse(commentList));
    });
}

/**
 * Gets if the user has voted, if the user hasn't voted and can then it should return false
 * However, if the user isn't logged in or isn't in a project etc. Then it will die. If a user
 * has voted, it will return true
 * @param {*} callback 
 */
function getUserHasVoted(callback) {
    $.getJSON("php/hasVoted.php", function(hasVoted) {
        callback(hasVoted);
    });
}

/**
 * Gets the comments for a particular user and project. This is useful to get just that user's
 * comments without having to user getComments() which would return the entire comment list.
 * 
 * @param {*} username 
 * @param {*} projectId 
 * @param {*} callback 
 */
function getUsersComments(username, projectId, callback) {
    $.post("php/getUsersComments.php", { "username": username, "project_id": projectId }, function(commentData) {
        commentData = JSON.parse(commentData);
        // userData.sort(function(a, b) {
        //     return (a.full_name < b.full_name) ? -1 : 1;
        // });
        callback(commentData);
    });
}

/**
 * Gets the users for the current course
 * 
 * @param {*} callback 
 */
function getUsers(callback) {
    $.getJSON("php/getUsers.php", function(userData) {
        userData.sort(function(a, b) {
            return (a.full_name < b.full_name) ? -1 : 1;
        });
        callback(userData);
    });
}

/**
 * Gets the all other users for the current course
 * aka. Everyone but the current user
 * 
 * @param {*} callback 
 */
function getOtherUsers(callback) {
    $.getJSON("php/getOtherUsers.php", function(userData) {
        userData.sort(function(a, b) {
            return (a.full_name < b.full_name) ? -1 : 1;
        });
        callback(userData);
    });
}

/**
 * Gets all non-admin users, this is useful because admin's can be added to teams
 * and they don't show up on the main page.
 * 
 * @param {*} callback 
 */
function getNonAdminUsers(callback) {
    $.getJSON("php/getNonAdminUsers.php", function(userData) {
        userData.sort(function(a, b) {
            return (a.full_name < b.full_name) ? -1 : 1;
        });
        callback(userData);
    });
}

/**
 * Gets a list of all the courses in the DB
 * 
 * @param {*} callback 
 */
function getCourses(callback) {
    $.getJSON("php/getCourses.php", function(courseList) {
        callback(courseList);
    });
}

/**
 * Gets the projects for the current course.
 * This is just a simple call for the project data, and it returns the name and ID
 * 
 * @param {*} callback 
 */
function getProjects(callback) {
    $.getJSON("php/getProjects.php", function(userData) {
        callback(userData);
    });
}

/**
 * Gets a list of all the teams in a specific project. projectName is the full project name
 * eg. "Project 1", "Project 2", etc.
 * 
 * @param {*} projectName 
 * @param {*} callback 
 */
function getTeams(projectName, callback) {
    $.post("php/getTeams.php", { "project_name": projectName }, function(teamList) {
        teamList = JSON.parse(teamList);

        // Sort by Team_ID
        // teamList.sort(function(a, b) {
        //     return (a.team_id < b.team_id) ? -1 : 1;
        // });
        callback(teamList);
    }).fail(function() {
        createAlert("Alert!", "There's been an error loading the team data.", $("body"), false);
    });
}

/**
 * Gets the open project's team list. This is used in voting, which doesn't know what project is open
 * at any given time.
 */
function getProjectTeamList(callback) {
    $.post("php/getCurrentTeamProject.php", function(teamList) {
        teamList = JSON.parse(teamList);
        teamList.sort(function(a, b) {
            return (a.full_name < b.full_name) ? -1 : 1;
        });
        callback(teamList);
    }).fail(function() {
        createAlert("Alert!", "There's been an error loading the team data.", $("body"), false);
    });
}

/**
 * Returns true or false if a project is open for voting
 * 
 * @param {*} callback 
 */
function isProjectOpen(callback) {
    $.getJSON("php/isProjectOpen.php", function(isOpen) {
        callback(isOpen);
    });
}

/**
 * Used for the admin only, and is ensured by that in the PHP. Sets the project open
 * 
 * @param {*} projectId 
 * @param {*} callback 
 */
function setProjectOpen(projectId, callback) {
    $.post("php/setProjectOpen.php", { "project_id": projectId }, function(error) {
        callback(error);
    });
}

/**
 * Helper function for populating the .userList <select> dropdown. 
 * This adds the user's Full Name as the dropdown parameter
 * @param {*} userList 
 */
function populateUserListHelper(userList) {
    var $userList = $(".userList");
    $userList.empty();
    for (var i in userList) {
        var $option = $("<option></option>").addClass("username");
        $option.text(userList[i].full_name);
        $option.attr("username", userList[i].username);
        $userList.append($option);
    }
    $userList.val($(".userList option:first").val());
}

/**
 * Generic function to automatically populate a .userList on any given page.
 */
function populateUserList() {
    getUsers(function(userList) {
        populateUserListHelper(userList);
    });
}

/**
 * Generic function to automatically populate a .usernameList on any given page.
 * This adds the user's username as the dropdown parameter
 */
function populateUsernameList() {
    getUsernamesForCourse(function(userList) {
        var $userList = $(".usernameList");
        $userList.empty();
        for (var i in userList) {
            var $option = $("<option></option>").addClass("username");
            $option.text(userList[i].username);
            $option.attr("username", userList[i].username);
            $userList.append($option);
        }
        $userList.val($(".usernameList option:first").val());
    });
}

/**
 * Generic helper to populate the .otherUserList on any given page.
 */
function populateOtherUserList() {
    getOtherUsers(function(userList) {
        var $userList = $(".otherUserList");
        $userList.empty();
        for (var i in userList) {
            var $option = $("<option></option>").addClass("username");
            $option.text(userList[i].full_name);
            $option.attr("username", userList[i].username);
            $userList.append($option);
        }
        $userList.val($(".otherUserList option:first").val());
    });
}

/**
 * Generic helper to populate the .nonAdminUserList list on any given page.
 */
function populateNonAdminUserList() {
    getNonAdminUsers(function(userList) {
        var $userList = $(".nonAdminUserList");
        $userList.empty();
        for (var i in userList) {
            var $option = $("<option></option>").addClass("username");
            $option.text(userList[i].full_name);
            $option.attr("username", userList[i].username);
            $userList.append($option);
        }
        $userList.val($(".nonAdminUserList option:first").val());
    });
}

/**
 * Helper method to populate the .courseList on pages that have a course dropdown
 * @param {*} courseList 
 */
function populateCourseListHelper(courseList) {
    var $courseList = $(".courseList");
    $courseList.empty();
    for (var i in courseList) {
        var $option = $("<option></option>").addClass("class");
        $option.text(courseList[i].name);
        $option.attr("course_ID", courseList[i].ID);
        $courseList.append($option);
    }
    $courseList.val($(".courseList option:first").val());
    $("#courseID").val(courseList[0].ID);
}

/**
 * Simple method that automatically scrolls the user to the top of the page.
 * Useful when showing a notification from createAlert()
 */
function scrollToTop() {
    $("html, body").animate({ scrollTop: 0 }, 600);
}

/**
 * This is called on a parent element and it automatically runs the animation on each of it's chilren elements
 * 
 * @param {*} $element 
 */
function animate($element) {
    $element.find(".slideUp").each(function(idx, val) {
        $(val).addClass("slideUpComplete");
        setTimeout(function() {
            $(val).removeClass("slideUp slideUpComplete")
        }, 750);
    });
    $element.find(".slideFadeRight").each(function(idx, val) {
        $(val).addClass("slideFadeRightComplete");
        setTimeout(function() {
            $(val).removeClass("slideFadeRight slideFadeRightComplete")
        }, 750);
    });
    $element.find(".slideFadeRight2").each(function(idx, val) {
        $(val).addClass("slideFadeRightComplete");
        setTimeout(function() {
            $(val).removeClass("slideFadeRight2 slideFadeRightComplete")
        }, 1500);
    });
}

/**
 * Sets all of the circles to be actually a circle. 
 * This is used on the main page to give all the circles the same size.
 * Also used on pages with a "back" button to make sure that icon is a perfect circle
 * 
 * @param {$circles} $circled 
 */
function fixCircle($circled) {

    // Forces the circles to all be the same size!
    var size = $circled.eq(0).height() + 10;
    $.each($circled, function(index, circle) {
        // var size = $(circle).height() + 10;
        $(circle).width(size).height(size);
    });
};

/**
 * Enables dark mode by removing the theme-light from the body and adding in the dark-theme
 * 
 * Note, this uses JQuery Cookies which tend to be very slow. Because of this, saving cookies may take
 * multiple tries...
 */
function enableDarkmode() {
    var $darkmodeIcon = $("#darkmodeIcon");
    console.log("Enabling dark mode");

    $("body").removeClass("theme-light").addClass("theme-dark");
    $darkmodeIcon.removeClass("fa-moon-o").addClass("fa-sun-o");

    setTimeout(function() {
        $.removeCookie('pc_darkmode');
        $.cookie('pc_darkmode', true, { expires: 180, path: '/' });
        console.log("Set Dark Mode");
    }, 1);
}

/**
 * Disables dark mode by removing the dark-light from the body and adding in the light-theme
 * 
 * Note, this uses JQuery Cookies which tend to be very slow. Because of this, saving cookies may take
 * multiple tries...
 */
function disableDarkmode() {
    var $darkmodeIcon = $("#darkmodeIcon");
    console.log("Disabling dark mode");

    $("body").removeClass("theme-dark").addClass("theme-light");
    $darkmodeIcon.removeClass("fa-sun-o").addClass("fa-moon-o");

    setTimeout(function() {
        $.removeCookie('pc_darkmode');
        $.cookie('pc_darkmode', false, { expires: 180, path: '/' });
        console.log("Set Dark Mode");
    }, 1)
}

/**
 * Enables legacy mode, which changes the main page to be a table
 * 
 * Note, this uses JQuery Cookies which tend to be very slow. Because of this, saving cookies may take
 * multiple tries...
 */
function enableLegacyView() {
    var $legacyViewIcon = $("#legacyViewIcon");
    console.log("Enabling Legacy View");

    $legacyViewIcon.removeClass("fa-table").addClass("fa-id-card-o");

    setTimeout(function() {
        $.removeCookie('pc_table_view');
        $.cookie('pc_table_view', true, { expires: 180, path: '/' });
        console.log("Set Legacy View");
    }, 1);
}

/**
 * Disables legacy mode, which changes the main page to be a card view for each user
 * 
 * Note, this uses JQuery Cookies which tend to be very slow. Because of this, saving cookies may take
 * multiple tries...
 */
function disableLegacyView() {
    var $legacyViewIcon = $("#legacyViewIcon");
    console.log("Disabling Legacy View");

    $legacyViewIcon.removeClass("fa-id-card-o").addClass("fa-table");

    setTimeout(function() {
        $.removeCookie('pc_table_view');
        $.cookie('pc_table_view', false, { expires: 180, path: '/' });
        console.log("Set Legacy View");
    }, 1);
}

// This will run on EVERY single page on the site!
$(function() {
    // console.log($.cookie('pc_darkmode'));
    if ($.cookie('pc_darkmode') === 'true') {
        enableDarkmode();
    }

    populateUserList();
    populateOtherUserList();
    populateNonAdminUserList();
    populateUsernameList();

    fixCircle($(".circled"));

    // When the user scrolls down, hide the nav bar
    // When they scroll back up show it
    var prevScrollpos = window.pageYOffset;
    window.onscroll = function() {
        var currentScrollPos = window.pageYOffset;
        if (prevScrollpos > currentScrollPos) {
            $("#navbar").removeClass("nav-up");
        } else {
            $("#navbar").addClass("nav-up");
        }
        prevScrollpos = currentScrollPos;
    }
});



/**
 * Creates an "explosion" of gold particles on the specific x/y location
 * 
 * @param {*} x 
 * @param {*} y 
 * @param {*} particles 
 * @param {*} isBig 
 */
function explode(x, y, particles, isBig) {
    var explosion = $('<div></div>').addClass("goldExplosion");

    // put the explosion container into the body to be able to get it's size
    $('body').append(explosion);

    // position the container to be centered on click
    explosion.css('left', x - explosion.width() / 2);
    explosion.css('top', y - explosion.height() / 2);

    var low = 20;
    var high = 40;
    if (isBig) {
        low = 100000;
        high = 1000000;
    }

    for (var i = 0; i < particles; i++) {
        // positioning x,y of the particle on the circle (little randomized radius)
        var x = (explosion.width() / 2) + rand(20, 40) * Math.cos(2 * Math.PI * i / rand(particles - 10, particles + 10)),
            y = (explosion.height() / 2) + rand(20, 40) * Math.sin(2 * Math.PI * i / rand(particles - 10, particles + 10)),
            color = '#ffd700', // Gold color
            // particle element creation (could be anything other than div)
            elm = $('<div class="particle" style="' +
                'background-color: ' + color + ';' +
                'top: ' + y + 'px; ' +
                'left: ' + x + 'px"></div>');

        if (i == 0) { // no need to add the listener on all generated elements
            // css3 animation end detection
            elm.one('webkitAnimationEnd oanimationend msAnimationEnd animationend', function(e) {
                explosion.remove(); // remove this explosion container when animation ended
            });
        }
        explosion.append(elm);
    }
}

/**
 * Gets a random number between the min/max values
 * @param {*} min 
 * @param {*} max 
 */
function rand(min, max) {
    return Math.floor(Math.random() * (max + 1)) + min;
}