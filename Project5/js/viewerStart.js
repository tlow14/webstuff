$(function() {
    // Get the projects that can be viewed
    getProjects(function(projectList) {
        var $projectList = $(".projectList");
        $projectList.empty()
        for (var i in projectList) {
            var $option = $("<option></option>").addClass("project");
            $option.text(projectList[i].name);
            $option.attr("projectID", projectList[i].id);
            $projectList.append($option);
        }
        $projectList.val($(".projectList option:first").val());
    });

    var $viewProject = $("#viewProject");
    var $viewUser = $("#viewUser");
    var $viewUserProject = $("#viewUserProject");

    // These are the buttons that allow the user to change the view
    // Watch out, because this is going to be in a iFrame, we have to change
    // Our parent's window location with window.parent.location. window.location will not work!
    // $viewProject.click(function() {
    //     var projName = $(".projectList option:selected").text();
    //     var localProjectId = parseInt(projName.substr(7));

    //     // Remove any remaining ?params from the user using the back button
    //     window.parent.location.href = window.parent.location.href.split("?")[0] + "?project=" + localProjectId;
    // });
    // $viewUser.click(function() {
    //     var username = $(".nonAdminUserList option:selected").attr("username");

    //     // Remove any remaining ?params from the user using the back button
    //     window.parent.location.href = window.parent.location.href.split("?")[0] + "?user=" + username;
    // });
    $viewUserProject.click(function() {
        var username = $(".nonAdminUserList option:selected").attr("username");
        var projName = $(".projectList option:selected").text();
        var localProjectId = parseInt(projName.substr(7));

        // Remove any remaining ?params from the user using the back button
        window.parent.location.href = window.parent.location.href.split("?")[0] + "?project=" + localProjectId + "&username=" + username;
    });
});