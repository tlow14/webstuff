$(function() {

    // On going count of how many teams currently exist, used for client side
    var amountOfTeams = 0;

    /**
     * Adds a team to the team-container and increments the amountOfTeams
     */
    function addTeam() {
        amountOfTeams++;
        var $div = $("<div></div>").addClass("team-container");
        $div.attr("id", "team" + amountOfTeams);
        var $wrapperDiv = $("<div></div>").addClass("teamSection bg-secondary p-6 rounded-lg");
        var $teamTitle = $("<h2></h2>").addClass("team-title").text("Team " + amountOfTeams);
        $teamTitle.attr("value", amountOfTeams);
        $wrapperDiv.attr("id", "teamNumber" + amountOfTeams);
        $wrapperDiv.append($teamTitle);
        $wrapperDiv.append($div);
        $(".team-grid").append($wrapperDiv);
    }

    /**
     * Removes a the last team from the team-container and decrements amountOfTeams
     */
    function removeTeam() {
        var $team_container = $("body").find("#team" + amountOfTeams).eq(0);
        if ($team_container.children().length == 0 && amountOfTeams != 1) {

            // Get the wrapperDiv
            $team_container.parent().remove();
            amountOfTeams--;
        } else {
            // createAlert("Alert!", "Unable to remove team with users in it!", $("body"), false);
            // scrollToTop();
        }
    }

    // When the auto assign button is clicked,
    // while there are users left in the user grid, remove them and add
    // a new team for each user. Put that user in the new team.
    $("#autoAssign").click(function() {
        var curTeam = 1;
        while ($(".username-grid").children().length > 0) {
            var $team = $("body").find("#team" + curTeam).eq(0);
            var $user = $(".username-grid").children().eq(0);
            $team.append($user.clone());
            $user.remove();

            curTeam++;
            if (curTeam > amountOfTeams) addTeam();
        }
        removeTeam();
    });

    // Populate Users into the userlist
    getNonAdminUsers(function(userList) {
        var $usernameGrid = $(".username-grid");
        for (var i in userList) {
            var $user = $("<p></p>").addClass("draggable-user flex items-center p-4");
            $user.text(userList[i].full_name);
            $user.attr("name", userList[i].username);
            $user.attr("value", userList[i].username);
            $usernameGrid.append($user);
        }

        // Set the username grid to have the same height as it originally did, this makes it so adding and moving
        // teams doesn't look as glitchy
        // $usernameGrid.css("min-height", $usernameGrid.height());

        // Creates the dragula container so that users can be dragged around and dropped into teams
        var containers = $('.team-container').toArray();
        containers.push($usernameGrid);
        dragula(containers, {
            isContainer: function(el) {
                // Define the containers
                return el.classList.contains('team-container') || el.classList.contains('username-grid');
            },
            revertOnSpill: true,
            // Define what can be dragged and moved
            accepts: function(el, target) {
                return $(el).hasClass("draggable-user");
            },
            moves: function(el, target) {
                return $(el).hasClass("draggable-user");
            }
        }).on('drag', function() {

            // When someone starts dragging a user,
            // Add a new team at the end if one doesn't exist
            var $team_container = $("body").find("#team" + amountOfTeams).eq(0);
            if ($team_container.children().length != 0) {
                addTeam();
            }
        }).on('drop', function() {
            removeTrailingTeams();
        });
    });

    /**
     * Removes all the extra teams that can happen after a clear etc. 
     * Should always leave at least one team left
     */
    function removeTrailingTeams() {
        while (true) {
            var $team_container = $("body").find("#team" + amountOfTeams).eq(0);
            if ($team_container.children().length == 0) {
                removeTeam();
                if (amountOfTeams < 2) break;
            } else {
                break;
            }
        }
    }

    // $("#addTeam").click(function() {
    //     addTeam();
    // });
    // $("#removeTeam").click(function() {
    //     removeTeam();
    // });

    // Clears all the users from the teams and adds them back to the user list
    function removeAllUsersFromTeams() {
        for (var i = 1; i <= amountOfTeams; i++) {
            var $team = $("body").find("#team" + i).eq(0);
            $.each($team.children(), function(idx, el) {
                $(".username-grid").append($(el));
            });
        }
    }

    // Clears the teams
    $("#clearTeams").click(function() {
        removeAllUsersFromTeams();
        removeTrailingTeams();
    });

    /**
     * When a new project is selected, this will automatically add all the teams for that project
     * it will take all the users that exist in the project data and move them from the user list to
     * the correct team number.
     */
    function updateTeamsFromProjectData() {
        removeAllUsersFromTeams();

        // This is the localProject name
        var projName = $(".projectList option:selected").text();

        getTeams(projName, function(teamList) {
            for (var i in teamList) {
                var team_name = teamList[i].team_name;
                var username = teamList[i].username;

                //Gets number at the end. This works since we are the only one giving input at the input will always be Team 1, Team 2, etc. 
                //If not we would just change the name of the boxes to the actual team name. This is a design decision
                var teamNum = team_name.substr(5, team_name.length);
                // console.log($user.text());
                while (teamNum >= amountOfTeams) {
                    addTeam();
                }

                var $team = $("body").find("#team" + teamNum).eq(0);
                var $user = $("body").find(`p[value="${username}"]`).eq(0);
                $team.append($user);

            }
            // The teamList can return [] if no team has been set,
            // this fixes the error that no initial Team 1 will be added
            if (teamList.length == 0) {
                addTeam();
            }
        });
    }

    // When the project is changed, then refresh the teams
    $(".projectList").on('change', function() {
        updateTeamsFromProjectData();
    });

    // Get the list of projects and retrieve the intial data
    getProjects(function(projectList) {
        var $projectList = $(".projectList");
        $projectList.empty()
        for (var i in projectList) {
            var $option = $("<option></option>").addClass("project");
            $option.text(projectList[i].name);
            $option.attr("projectID", projectList[i].id);
            $projectList.append($option);
        }
        $projectList.val($(".projectList option:first").val());

        // Get the initial project's data
        updateTeamsFromProjectData();
    });

});

/**
 * Submits the team to PHP.
 */
function submitTeam() {

    var phpArray = [];
    var count = 1;
    var projectName = $(".projectList option:selected").text();

    $('#TeamSelectionGrid > .teamSection').each(
        function() {
            if ($(this).find('.draggable-user').length) {
                var teamNumber = $(this).find('.team-title').text();
                var teamName = ("#team" + count + " > .draggable-user").toString();
                $(teamName).each(
                    function() {
                        var user = $(this).attr("name");
                        phpArray.push({ name: user, team: teamNumber });
                    }
                )
            }
            count++;
        }
    );

    $.ajax({
        type: 'POST',
        url: './php/setTeams.php',
        data: { jsArray: phpArray, projectName: projectName },
        success: function(data) {
            createAlert("Notice", "The teams have been set!", $("body"), true);
            scrollToTop();
            console.log(data);
        },
        error: function() {
            console.log("Could not connect to the database");
            createAlert("Alert!", "The teams failed to get set!", $("body"), false);
            scrollToTop();
        }

    })
};