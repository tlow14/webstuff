$(function() {



    // The toggle button for Dark Mode
    // Note for both the dark mode and the legacy mode the toggle button
    // is bigger than the actual icon because you can click on the entire
    // toggle for it to turn on and off, so we need both the path 
    // and the icon. That is why there are two triggers.
    var $darkmodeToggle = $("#darkmodeToggle");
    var $darkmodePath = $("#darkmodePath");

    /**
     * Toggle the darkmode if the input has changed
     */
    function darkmodeCheckboxChanged() {
        if ($("#darkmodeInput").is(':checked')) {
            disableDarkmode();
        } else {
            enableDarkmode();
        }
    }
    $darkmodeToggle.click(function() {
        darkmodeCheckboxChanged();
    });
    $darkmodePath.click(function() {
        darkmodeCheckboxChanged();
    });
    // If the dark mode is already set, then toggle on the dark mode
    if ($.cookie('pc_darkmode') == 'true') {
        $darkmodeToggle.click();
    }

    // The toggle mode for legacy mode
    var $legacyViewToggle = $("#legacyViewToggle");
    var $legacyViewPath = $("#legacyViewPath");

    /**
     * Toggle the legacy mode if the input has changed
     */
    function legacyCheckboxChanged() {
        if ($("#legacyViewInput").is(':checked')) {
            disableLegacyView();
        } else {
            enableLegacyView();
        }
    }
    $legacyViewToggle.click(function() {
        legacyCheckboxChanged();
    });
    $legacyViewPath.click(function() {
        legacyCheckboxChanged();
    });
    // If the legacy mode is already set, then toggle on the legacy mode
    if ($.cookie('pc_table_view') == 'true') {
        $legacyViewToggle.click();
    }

    fixCircle($('.circled'));
    animate($("body"));

    var $projectList = $(".projectList");
    var $projectUrl = $("#projectUrl");

    function populateProjectUrl() {
        var projectID = $(".projectList option:selected").attr("projectID");
        // Get list of teams/projects
        var username = $("#username").val();
        $.post("php/getUrl.php", { "username": username }, function(urlList) {
            urlList = JSON.parse(urlList);
            console.log(urlList);
            // Loop through and find the data for the selected project
            for (var i in urlList) {
                if (urlList[i].project_id == projectID) {
                    $projectUrl.val(urlList[i].url);
                    break;
                }
                // if data matches projectID...
                //     $projectUrl.text(data.path???);
            }
        });
    }

    $('#urlSaveButton').click(function() {
        postProjectURL();
    });

    function postProjectURL() {
        var projectID = $(".projectList option:selected").attr("projectID");
        var projectName = $(".projectList option:selected").text();
        var url = $('#projectUrl').val();
        var urlOneWord = url.split(" ");
        var isValidUrl = true;

        if (url.substring(0, 28) == "http://judah.cedarville.edu/" || url.substring(0, 28) == "http://csweb.cedarville.edu/") {
            url = url.substring(27, url.length);
        } else if (url.substring(0, 29) == "https://judah.cedarville.edu/" || url.substring(0, 29) == "https://csweb.cedarville.edu/") {
            url = url.substring(28, url.length);
        } else if (url.substring(0, 2) == '/~') {
            url = url;
        } else if (url.substring(0, 1) == '~') {
            url = '/' + url;
        } else if (url.substring(0, 21) == 'judah.cedarville.edu/' || url.substring(0, 21) == 'cs.cedarville.edu/') {
            url = url.substring(21, url.length);
        } else {
            isValidUrl = false;
        }

        if (url != "" || urlOneWord.length == 1 || isValid) {
            $.post("php/updateUrl.php", { "url": url, "project_id": projectID }, function(error) {
                createAlert("Notice", "URL for project " + projectName + " changed to " + url, $("body"), true);
                $('#projectUrl').val('');
                console.log(error);
                populateProjectUrl();
            }).fail(function() {
                createAlert("Alert!", "URL failed to be added.", $("body"), false);
                scrollToTop();
                populateProjectUrl();
            })
        } else {
            createAlert("Alert!", "Need a valid link either http://judah.cedarville.edu/~name/project/example, http://csweb.cedarville.edu/~name/project/example, or /~name/project/example ", $("body"), false);
        }
    }

    // Get the projects that the user has been assigned a team to
    // using the normal getProjects would give every single project.
    // but we don't want do that since the user wouldn't be able to 
    // change the url of a project they haven't been added to
    var username = $("#username").val();
    $.post("php/getUrl.php", { "username": username }, function(projectList) {
        var projectList = JSON.parse(projectList);
        var $projectList = $(".projectList");
        $projectList.empty();
        console.log(projectList);
        for (var i in projectList) {
            var $option = $("<option></option>").addClass("project");
            $option.text("Project " + (parseInt(i) + 1));
            $option.attr("projectID", projectList[i].project_id);
            $projectList.append($option);
        }
        populateProjectUrl();
    });

    // If there is a change on the project list, update the buttons
    $projectList.on('change', function() {
        populateProjectUrl();
    });




    // Check Password matching
    var $password = $("#password");
    var $confirmPassword = $("#confirmPassword");

    // Client side validation of the user's password change
    // Also checked on the server side, this is just so it looks good
    function matchingPassword() {
        if ($password.val() != $confirmPassword.val() || $password.val() == "") {
            $password.removeClass("formSuccess");
            $confirmPassword.removeClass("formSuccess");
            $password.addClass("formError");
            $confirmPassword.addClass("formError");
            return false;
        } else {
            $password.addClass("formSuccess");
            $confirmPassword.addClass("formSuccess");
            $password.removeClass("formError");
            $confirmPassword.removeClass("formError");
            return true;
        }
    }
    $password.change(function() {
        matchingPassword();
    });
    $confirmPassword.change(function() {
        matchingPassword();
    });

    // Submit the password only if they match
    var $passwordForm = $("#passwordForm");
    $("#submitPassword").click(function() {
        if (matchingPassword()) {
            // Send new password to DB
            $passwordForm.submit();
        } else {
            createAlert("Alert!", "The passwords you have entered do not match.", $("body"));
            scrollToTop();
        }
    });
});