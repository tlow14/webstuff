/**
 * Shows the overlay either notifying the user of a vote, or showing a card
 */
function showOverlay() {
    var $overlay = $("#overlay");
    $overlay.css("z-index", 100);
    $overlay.animate({
        opacity: 1,
    }, 125, function() {});
}

/**
 * Hides the overlay
 */
function hideOverlay() {
    var $overlay = $("#overlay");
    $overlay.animate({
        opacity: 0,
    }, 125, function() {
        $overlay.css("z-index", -1);
    });
}

/**
 * This is the common location to set the user's home page relative address
 * @param {*} username 
 */
function getUserHome(username) {
    return "/~" + username + "/cs3220.html";
}

/**
 * Populates all of the cards with cards from the database. 
 * This copies the #card-template and makes replications of it 
 * for each user.
 */
function populateCards() {

    var $template = $("#card-template");
    var $cardContainer = $(".mgrid");

    $.getJSON("php/getAllResults.php", function(projectData) {
        // console.log(projectData);

        // Changes the data to be in a new array with [username, {data}]
        projectData = Object.entries(projectData);

        // Sorts the data by last name so that they will be an alphabetical order
        projectData.sort(function(a, b) {
            // In a try catch just in case the user has a special case last name, that won't be caught
            try {
                var lastNameA = a[1].full_name.split(" ").splice(-1)[0].toLowerCase();
                var lastNameB = b[1].full_name.split(" ").splice(-1)[0].toLowerCase();

                if (!lastNameA) return 1;
                if (!lastNameB) return -1;
                // console.log("1: " + lastNameA + " 2: " + lastNameB);
                return (lastNameA < lastNameB) ? -1 : 1;
            } catch (e) {
                return 1;
            }
        });



        // Loop through all of the sorted projectData
        for (var key in projectData) {

            var username = projectData[key][0];
            var full_name = projectData[key][1].full_name;
            var icon_url = projectData[key][1].icon_url;
            var subtitle = projectData[key][1].subtitle;
            var projectAwards = projectData[key][1].projects;

            // Create a copy of the template
            var $newCard = $template.clone().attr("id", username);

            // Because the username exists more than one place
            $newCard.find(".card_username").each(function(inx, val) {
                $(val).text(full_name);
            });

            // If the subtitle is valid, then change all of the .card_subtitles to the actual subtitle
            if (subtitle) {
                $newCard.find(".card_subtitle").each(function(inx, val) {
                    $(val).text(subtitle);
                });
            }

            // If the iconUrl exists, then add it to each .card_icon
            if (icon_url) {
                $newCard.find(".card_icon").each(function(inx, val) {
                    $(val).attr("src", icon_url);
                });
            }

            // Set the home button url to the user's home location
            $newCard.find(".courseHomePage").attr("href", getUserHome(username));

            // Loop through each of the projects individually and get the info on them
            // this info includes gold/silver/bronze, write-ins/comments, etc.
            for (var localProjectId in projectAwards) {

                // This is "Project 1" ... "Project n"
                //          012345678
                var projectName = projectAwards[localProjectId].name;
                var projectNum = parseInt(projectName.substr(7));

                var isGold = projectAwards[localProjectId].gold;
                var isSilver = projectAwards[localProjectId].silver;
                var isBronze = projectAwards[localProjectId].bronze;

                // Find the project link for the frontside of the card
                var $cardLink = $newCard.find(".card-link").eq(projectNum - 1);

                // Add the link to the flipside and the small links on the frontside
                var $flipsideLink = $newCard.find(".flipside").find(".card-link").eq(projectNum - 1);
                $flipsideLink.attr("href", "projectViewer.php?project=" + projectNum + "&username=" + username);

                $cardLink.attr("href", "projectViewer.php?project=" + projectNum + "&username=" + username);

                // Add each of the comments to the associated project on the flipside
                for (var i in projectAwards[localProjectId].comments) {
                    var comment = projectAwards[localProjectId].comments[i];
                    if (comment) {
                        var $li = $("<li></li>").addClass("my-1 font-light list-none break-words").text(comment);
                        $newCard.find(".flipside").find(".cardLinkComments").eq(projectNum - 1).append($li);
                    }
                }

                // Create a new image and add it if the user got a gold/silver/bronze
                var $img = $("<img/>").text("trophy").addClass("h-8 mx-2");
                var $medalLocation = $flipsideLink.find(".medalLocation");
                if (isBronze) {
                    $cardLink.addClass("bronzeAward");

                    $img.attr("src", "images/bronze-medal.png");
                    $img.attr("alt", "Bronze Medal");
                    $medalLocation.append($img.clone());
                    $medalLocation.prepend($img.clone());
                } else if (isSilver) {
                    $cardLink.addClass("silverAward");

                    $img.attr("src", "images/silver-medal.png");
                    $img.attr("alt", "Silver Medal");
                    $medalLocation.append($img.clone());
                    $medalLocation.prepend($img.clone());
                } else if (isGold) {
                    $cardLink.addClass("goldAward");

                    $img.attr("src", "images/gold-medal.png");
                    $img.attr("alt", "Gold Medal");
                    $medalLocation.append($img.clone());
                    $medalLocation.prepend($img.clone());
                }
                $cardLink.addClass("circled");
            }

            // Add the new card to the card container
            $cardContainer.append($newCard);
        }

        // Remove the template
        $template.remove();

        // Clicking on a Card
        var $cards = $(".card");
        var $overlay = $("#overlay");
        // If the user clicks on a card we need to show the flipside of the card
        // To do this, the flipside's html is inserted into the overlay and the overlay is displayed
        $cards.click(function(e) {
            var flipsideHtml = $(this).find('.flipside').html();
            $overlay.html(flipsideHtml);

            var $flipsideWrapper = $("#overlay .flipside-wrapper");

            // If clicking inside the element, do nothing
            // Only allow the click to the parent overlay
            $flipsideWrapper.on("click", function(event) {
                event.stopPropagation();
            });
            showOverlay();
            animate($flipsideWrapper);
        });
        // If a card-link is clicked, we don't want to try and open the card. It gives a bad effect.
        $(".card-link").click(function(e) {
            e.stopPropagation();
        });

        // Now that all the cards are loaded into the DOM, animate all of them
        animateCards();

        // If a goldAward is hovered, give it a cool effect. They earned it!
        $('.goldAward').one("mouseenter", function(e) {
            var offset = $(this).offset();
            explode(offset.left + ($(this).width() / 2), offset.top + ($(this).height() / 2), 30);
        });

    });
}

/**
 * Populates all of the legacy table view, follows the exact same method of populateCards, but is just in a different format.
 */
function populateTable() {
    var $template = $("#table-row-template");
    var $tableContainer = $("#table-body");

    $.getJSON("php/getAllResults.php", function(projectData) {
        projectData = Object.entries(projectData);
        projectData.sort(function(a, b) {
            try {
                var lastNameA = a[1].full_name.split(" ").splice(-1)[0].toLowerCase();
                var lastNameB = b[1].full_name.split(" ").splice(-1)[0].toLowerCase();

                if (!lastNameA) return 1;
                if (!lastNameB) return -1;
                // console.log("1: " + lastNameA + " 2: " + lastNameB);
                return (lastNameA < lastNameB) ? -1 : 1;
            } catch (e) {
                return 1;
            }
        });
        // console.log(projectData);
        for (var key in projectData) {

            var username = projectData[key][0];
            var full_name = projectData[key][1].full_name;
            var icon_url = projectData[key][1].icon_url;
            var subtitle = projectData[key][1].subtitle;
            var projectAwards = projectData[key][1].projects;

            var $newRow = $template.clone().attr("id", username);
            $newRow.find('.userCol').attr("data-href", getUserHome(username));

            // Because the username exists more than one place
            $newRow.find(".row_username").each(function(inx, val) {
                $(val).text(full_name);
            });

            if (subtitle) {
                $newRow.find(".row_subtitle").each(function(inx, val) {
                    $(val).text(subtitle);
                });
            }
            if (icon_url) {
                $newRow.find(".row_icon").each(function(inx, val) {
                    $(val).attr("src", icon_url);
                });
            }
            for (var localProjectId in projectAwards) {

                // This is "Project 1" ... "Project n"
                //          012345678
                var projectName = projectAwards[localProjectId].name;
                var projectNum = parseInt(projectName.substr(7));

                var isGold = projectAwards[localProjectId].gold;
                var isSilver = projectAwards[localProjectId].silver;
                var isBronze = projectAwards[localProjectId].bronze;

                var $td = $("<td></td>").addClass("border-grey-light border hover:bg-ternary truncate cursor-pointer");
                var $img = $("<img/>").text("trophy").addClass("h-12 mx-auto");

                $td.attr("data-href", "projectViewer.php?project=" + projectNum + "&username=" + username);

                if (isBronze) {
                    $img.attr("src", "images/bronze-medal.png");
                    $img.attr("alt", "Bronze Medal");
                    $td.append($img.clone());
                } else if (isSilver) {
                    $img.attr("src", "images/silver-medal.png");
                    $img.attr("alt", "Silver Medal");
                    $td.append($img.clone());
                } else if (isGold) {
                    $img.attr("src", "images/gold-medal.png");
                    $img.attr("alt", "Gold Medal");
                    $td.append($img.clone());
                }

                $newRow.append($td);
            }
            $tableContainer.append($newRow);
        }

        $template.remove();

        // If the table row is clicked then send the user to the associated href.
        // We need this because as of right now you can't just add a <a><td></td></a>
        // or vice versa. So each TD has a data-href that functions the same way as an href.
        // The below code actually makes something happen when it is clicked.
        $(document.body).on("click", "td[data-href]", function() {
            window.location.href = this.dataset.href;
        });

    });
}

/**
 * Decides whether or not to load the legacy table view, or the card view based on a cookie
 */
function populateHomePage() {
    if ($.cookie('pc_table_view') === 'true') {
        populateTable();
        $("#table-view").removeClass("hidden");
        $("#card-view").empty();
    } else {
        populateCards();
        $("#table-view").empty();
    }
}

/**
 * Animates the cards one at a time based on a small interval
 * remember that there could be around 30 users, so the interval
 * can't be too long.
 */
function animateCards() {
    var $cards = $(".card");
    var size = $cards.length;
    $cards.each(function(idx, val) {
        setTimeout(function() {
            $(val).addClass("slideUpComplete");

            // This is the last element, remove all the slideUp and Completes
            // from the cards
            if (idx == size - 1) {
                setTimeout(function() {
                    $cards.each(function(idx, val) {
                        $(val).removeClass("slideUp slideUpComplete");
                    });

                    // .75s is the time of the animation
                }, 750);
            }
        }, (75 * idx));
        totalIndex = idx;
    });
}

$(function() {

    // Populates the course dropdown and loads the last saved course dropdown from a cookie if the user is logged out.
    getCourses(function(courseList) {
        populateCourseListHelper(courseList);

        // Gets the current course. If there is no saved session variable, then it will fail.
        // If it fails it will look for a cookie, if the cookie doesn't exist then it will reload the page.
        // On the page reload, it will then default to the newest course based on desc ID's
        // Remember to watch out for what the user sees when they logout. This is a result of that case.
        $.getJSON("php/getCurrentCourse.php", function(courseId) {
            $(".courseList").val($("body").find(`option[course_ID="${courseId}"]`).eq(0).val());
            getUsersForCourse(courseId, function(userList) {
                populateUserListHelper(userList);
            });
        }).fail(function() {
            console.log("User doesn't have a session, so will use cookie");
            var courseId = $.cookie('last_courseId');
            if (courseId) {
                $.post("php/setCurrentCourse.php", { "course_id": courseId }, function() {
                    location.reload();
                });
            }
        });
    });

    // When the user clicks on the dropdown, change the SESSION variable and reload the page
    $(".courseList").on("change", function() {
        var courseId = $(".courseList option:selected").attr("course_ID");
        $.post("php/setCurrentCourse.php", { "course_id": courseId }, function() {
            location.reload();
        });

        // Set the new cookie to the new courseId
        $.removeCookie('last_courseId');
        $.cookie('last_courseId', courseId, { expires: 180, path: '/' });
    });

    // Decide which view to load and load it.
    populateHomePage();

    // Animate the hello message and "People's Choice"
    animate($("#mainTitle").parent());
    animate($("#welcomeMsg").parent());

    // Converts the user's full_name to username for the php
    $("#loginSubmit").click(function() {
        $("#hiddenUsername").val($("#usernameOption option:selected").attr("username"));
    });

    // Relayout the Grid when the window size is moved
    // $(window).resize(function () {
    //     // Close all the cards
    //     $cards.each(function(inx, val) {
    //         $(val).removeClass("active");
    //     });
    // });

    // If the overlay is clicked, then hide it
    var $overlay = $("#overlay");
    $overlay.click(function(e) {
        hideOverlay();
    });

    // If the user hasn't voted, then display the overlay
    // By default, the overlay has a message that urges the user
    // to vote. However, that overlay message is overwritten by the
    // cards if they are clicked
    getUserHasVoted(function(hasVoted) {
        if (hasVoted == false) {
            showOverlay();
        }
    });

    // Simple password visibility toggle button
    $("#togglePasswordVisibility").click(function() {
        if ($(this).hasClass("fa-eye")) {
            $(this).removeClass("fa-eye");
            $(this).addClass("fa-eye-slash");
            $("#loginPassword").attr("type", "password");
        } else {
            $(this).removeClass("fa-eye-slash");
            $(this).addClass("fa-eye");
            $("#loginPassword").attr("type", "text");
        }
    });
});