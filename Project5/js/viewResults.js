$(function() {
    // Load the initial chart which will always be 1 because that is the tab/view that is automatically selected
    loadChart("#result1", 1);
    getProjectIDFromLocalId(1, function(projNum) {
        populateCommentList(projNum, "#view" + 1 + " #commentWrapper");
    });

    // When a new tab is clicked, get the tab number and load the new chart
    // Loading the projects this way is best because if not then we would have to 
    // load all of the projects at the same time when they are loaded. This only 
    // loads the projects that we want to see
    $(".tab").click(function() {
        var tabNumber = $(this).attr("tab");

        // Remove active tab from all the tabs
        $(".tab").each(function(idx, el) {
            $(el).removeClass("activeTab");
        });

        // Add active tab, to the one that just got clicked
        $(this).addClass("activeTab");

        // Close all views
        $(".view").each(function(idx, el) {
            $(el).removeClass("activeView");
        });

        // Open the view that is associated with this tab
        $("#view" + tabNumber).addClass("activeView");

        // Load the new chart with the new projectNumber
        loadChart("#result" + tabNumber, tabNumber);
        getProjectIDFromLocalId(tabNumber, function(projNum) {
            console.log("Project Num: " + projNum + " Tab Number: " + tabNumber);
            populateCommentList(projNum, "#view" + tabNumber + " #commentWrapper");
        });
    });
});