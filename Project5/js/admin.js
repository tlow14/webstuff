$(function() {
    var $projectList = $(".projectList");
    var $closeCheckbox = $("#closeCheckbox");
    var $openCheckbox = $("#openCheckbox");

    // Updates the buttons based on if the selected project is open/closed
    function updatePollButtons() {
        isProjectOpen(function(projectId) {
            var projectID = $(".projectList option:selected").attr("projectID");
            // console.log(projectID + " == " + projectId);
            if (projectID == projectId) {
                $closeCheckbox.prop("checked", false);
                $openCheckbox.prop("checked", true);
            } else {
                $closeCheckbox.prop("checked", true);
                $openCheckbox.prop("checked", false);
            }
        });
    }
    // If there is a change on the project list, update the buttons
    $projectList.on('change', function() {
        updatePollButtons();
    });

    // Gets the projects and updates the dropdown
    getProjects(function(projectList) {
        var $projectList = $(".projectList");
        $projectList.empty()
        for (var i in projectList) {
            var $option = $("<option></option>").addClass("project");
            $option.text(projectList[i].name);
            $option.attr("projectID", projectList[i].id);
            $projectList.append($option);
        }

        // Finds the open project and sets the current project selction dropdown to that course
        // If there is no project, then the dropdown defaults to the first item in the list
        isProjectOpen(function(isOpen) {
            if (isOpen != false) {
                $projectList.val($("body").find(`option[projectID="${isOpen}"]`).eq(0).val());
            } else {
                $projectList.val($(".projectList option:first").val());
            }
            updatePollButtons();
        });
    });

    // If the close checkbox is clicked, and it was currently on, then close the project on the server
    $closeCheckbox.on("change", function() {
        if (this.checked) {
            setProjectOpen("null", function(error) {
                console.log(error);
            }).fail(function() {
                createAlert("Alert!", "Failed to set the project to closed!", $("body"), false);
                scrollToTop();
            });
        }
    });
    $openCheckbox.on("change", function() {
        if (this.checked) {
            var projectID = $(".projectList option:selected").attr("projectID");
            // console.log("Opened Project: " + projectID);

            setProjectOpen(projectID, function(error) {
                console.log(error);
            }).fail(function() {
                createAlert("Alert!", "Failed to set the project to open!", $("body"), false);
                scrollToTop();
            });
        }
    });

    // var $userManagement = $("#usernameManagement");
    var $deleteUser = $("#deleteUser");
    var $resetPassword = $("#resetPassword");
    var $setRoleAdmin = $("#setRoleAdmin");
    var $setRoleUser = $("#setRoleUser");

    // Deletes a user from the DB when the button is pressed
    $deleteUser.click(function() {
        var username = $("#usernameManagement  option:selected").attr("username");

        $.post("php/removeUser.php", { "username": username }, function(error) {
            console.log(error);

            // Update the dropdowns with users in them
            populateOtherUserList();
            populateUsernameList();

            createAlert("Notice", username + " has been removed!", $("body"), true);
        }).fail(function() {
            createAlert("Alert!", username + " failed to be removed!", $("body"), false);
        });
        scrollToTop();
    });

    // Reset the password of the selected user
    $resetPassword.click(function() {
        var username = $("#usernameManagement  option:selected").attr("username");
        $.post("php/resetUserPassword.php", { "username": username }, function(error) {
            createAlert("Notice", username + "'s password has been reset!", $("body"), true);
        }).fail(function() {
            createAlert("Alert!", username + " failed to have password reset!", $("body"), false);
        });
        scrollToTop();
    });

    // Sets the selected user's role to admin
    $setRoleAdmin.click(function() {
        var username = $("#usernameManagement  option:selected").attr("username");
        $.post("php/setUserRole.php", { "username": username, "role": "admin" }, function(error) {
            createAlert("Notice", username + " is now a " + "admin" + "!", $("body"), true);
        }).fail(function() {
            createAlert("Alert!", username + "'s role failed to be set!", $("body"), false);
        });
        scrollToTop();
    });

    // Sets the selected user's role to user
    $setRoleUser.click(function() {
        var username = $("#usernameManagement  option:selected").attr("username");
        $.post("php/setUserRole.php", { "username": username, "role": "user" }, function(error) {
            createAlert("Notice", username + " is now a " + "user" + "!", $("body"), true);
        }).fail(function() {
            createAlert("Alert!", username + "'s role failed to be set!", $("body"), false);
        });
        scrollToTop();
    });

    // Edits the user's name
    var $editUserNameSubmit = $("#editUserSubmit");
    $editUserNameSubmit.click(function() {
        var $editFullName = $("#editFullName");
        var $editUsername = $("#editusername");

        var username = $editUsername.val();
        var full_name = $editFullName.val();

        var isUsername = true;
        var isFullname = true;
        if (username == null) {
            isUsername = false;
        }
        if (full_name == "") {
            isFullname = false;
            if (!isUsername) {
                createAlert("Alert!", "Enter a linux username and new full name to edit auser", $("body"), false);
            } else {
                createAlert("Alert!", "Enter a new full name to edit a user", $("body"), false);
            }
            scrollToTop();
        } else {
            if (!isUsername) {
                createAlert("Alert!", "Enter a linux username to edit a user", $("body"), false);
                scrollToTop();
            }
        }

        if (isFullname && isUsername) {
            $.post("php/editUser.php", { "username": username, "full_name": full_name }, function(type) {

                $editFullName.val("");
                populateOtherUserList();
                populateUsernameList();

            }).fail(function() {
                createAlert("Alert!", username + " failed to edit!", $("body"), false);
                scrollToTop();
            });
        }
    });

    // CREATE USER
    var $addLinuxUsername = $("#addLinuxUsername");
    var $addFullName = $("#addFullName");
    var $createUserSubmit = $("#createUserSubmit");
    $createUserSubmit.click(function() {

        var username = $addLinuxUsername.val();
        var full_name = $addFullName.val();
        var lengthCheck = username.split(' ');

        var isUsername = true;
        var isFullname = true;
        if (username == "" || lengthCheck.length != 1) {
            isUsername = false;
        }
        if (full_name == "") {
            isFullname = false;
            if (!isUsername) {
                createAlert("Alert!", "Enter a linux username (only one word) and new full name to create a user", $("body"), false);
            } else {
                createAlert("Alert!", "Enter a new full name to create a user", $("body"), false);
            }
            scrollToTop();
        } else {
            if (!isUsername) {
                createAlert("Alert!", "Enter one word as linux username to create a user", $("body"), false);
                scrollToTop();
            }
        }
        console.log("Creating new user: Username: " + username + " Full_Name: " + full_name);

        if (isUsername && isFullname) {
            $.post("php/createUser.php", { "username": username, "full_name": full_name }, function(type) {
                if (type == "duplicate") {
                    createAlert("Notice", username + " already exists in this course", $("body"), true);
                    scrollToTop();
                } else if (type == "update") {
                    createAlert("Notice", username + " user from another course has been changed to this course.", $("body"), true);
                    scrollToTop();
                } else if (type == "new") {
                    createAlert("Notice", username + " has been created", $("body"), true);
                }

                $addLinuxUsername.val("");
                $addFullName.val("");
                populateOtherUserList();
                populateUsernameList();

            }).fail(function() {
                createAlert("Alert!", username + " failed to create!", $("body"), false);
                scrollToTop();
            });
        }
    });

    // COURSE MANAGEMENT
    // CREATE COURSE
    var $projectDecrement = $("#projectDecrement");
    var $projectIncrement = $("#projectIncrement");
    var $projectCount = $("#projectCount");
    // This is the number counter for the course creation
    $projectDecrement.click(function() {
        if (parseInt($projectCount.val()) > 0) {
            $projectCount.val(parseInt($projectCount.val()) - 1);
        }
    });
    $projectIncrement.click(function() {
        $projectCount.val(parseInt($projectCount.val()) + 1);
    });

    // Create a new course with the specified number of projects.
    var $addCourseName = $("#addCourseName");
    var $addCourse = $("#addCourse");
    $addCourse.click(function() {
        var courseName = $addCourseName.val();
        var projectCount = parseInt($projectCount.val());
        // console.log("Adding Course: " + courseName + ", with " + projectCount + " projects.");
        $.post("php/createCourse.php", { "courseName": courseName, "projectCount": projectCount }, function(error) {
            createAlert("Notice", "Created course " + courseName + " that has " + projectCount + " projects.", $("body"), true);
            scrollToTop();
        }).fail(function() {
            createAlert("Alert!", "Failed to create course: " + courseName + "!", $("body"), false);
            scrollToTop();
        });
    });

    // function populateCourseList() {
    //     getCourses(function(courseList) {
    //         var $courseList = $(".courseList");
    //         $courseList.empty();
    //         for (var i in courseList) {
    //             var $option = $("<option></option>").addClass("username");
    //             $option.text(courseList[i].name);
    //             $option.attr("course_ID", courseList[i].ID);
    //             $courseList.append($option);
    //         }
    //         $courseList.val($(".courseList option:first").val());
    //     });
    // }
    function populateCourseList() {
        getCourses(function(courseList) {
            populateCourseListHelper(courseList);
        });
    }
    populateCourseList();

    // DELETE COURSE
    // var $removeCourseList = $("#removeCourseList");
    var $removeCourse = $("#removeCourse");
    $removeCourse.click(function() {
        var courseName = $("#removeCourseList option:selected").val();
        var course = $("#removeCourseList option:selected").attr("course_ID");
        $.post("php/removeCourse.php", { "course_id": course }, function(error) {
            createAlert("Notice", courseName + " has successfully beed deleted!", $("body"), true);
            scrollToTop();

            populateCourseList();

        }).fail(function() {
            createAlert("Alert!", courseName + " failed to be deleted", $("body"), false);
            scrollToTop();
        });
    });


});