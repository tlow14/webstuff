$(function() {

    // RELOAD THE CHART AND COMMENTS EVERY 5 SECONDS
    loadChart("#vote-results", "null");
    populateCommentList('null', "#commentWrapper");
    setInterval(function() {
        loadChart("#vote-results", "null");
        populateCommentList('null', "#commentWrapper");
    }, 2500);

    var $teamSelectList = $("#teamSelectList");
    // Populate the team list, if a team has multiple users
    // then those users are comma separated
    getProjectTeamList(function(teamList) {
        var $teamGrid = $(".teamlist-grid");
        // var name = $('#titleMessage').text().split(',')[0].trim();
        var name = $("#usernameInput").text();
        var ownTeam = "";

        // Sort by Team_ID
        teamList.sort(function(a, b) {
            return (a.team_id < b.team_id) ? -1 : 1;
        });

        //Grabs the team of the current user
        for (var i in teamList) {
            if (name == teamList[i].full_name) {
                ownTeam = teamList[i].team_id;
            }
        }

        for (var i in teamList) {
            var teamId = teamList[i].team_id.toString();
            //Makes it so the user's team does not show up
            if (ownTeam != teamList[i].team_id) {
                if (document.getElementById(teamId)) {
                    var teamIdJQ = "#" + teamId;
                    var box = $(teamIdJQ).append(",\n" + teamList[i].full_name);
                    teamIdJQ += "dropdown";
                    var dropdownName = $(teamIdJQ).append(",\n" + teamList[i].full_name);
                } else {
                    var $user = $("<p></p>").addClass("draggable-user text-primaryText");
                    $user.text(teamList[i].full_name);
                    $user.attr("id", teamList[i].team_id);
                    $user.attr("team_name", teamList[i].team_name);
                    $user.attr("team_id", teamList[i].team_id);

                    $teamGrid.append($user);

                    var $option = $("<option></option>").addClass("team");
                    $option.text(teamList[i].full_name);
                    var teamIdPrime = teamId + "dropdown";
                    $option.attr("id", teamIdPrime);
                    $option.attr("team_name", teamList[i].team_name);
                    $option.attr("team_id", teamList[i].team_id);

                    $teamSelectList.append($option);
                }
            }
        }

        // Allow the Drag and Drop for Mobile devices!
        const moveList = document.querySelectorAll('p.draggable-user');
        if (moveList) {
            moveList.forEach(move => {
                move.addEventListener('touchmove', event => event.preventDefault());
            });
        }

        // Create the Dragula so users can drag votes into different containers
        var containers = $('.vote-container').toArray();
        containers.push($teamGrid);
        dragula(containers, {
            isContainer: function(el) {
                // Define the containers
                return el.classList.contains('vote-container') || el.classList.contains('teamlist-grid');
            },
            revertOnSpill: true,
            accepts: function(el, target) {
                // Define what the container accepts
                // This is some client-side handling if the user tries to add multiple users to a vote
                if ($(target).children().length == 2 && $(target).hasClass('vote-container')) return false;
                return $(el).hasClass("draggable-user");
            },
            moves: function(el, target) {
                return $(el).hasClass("draggable-user");
            }
        }).on('drop', function(el, target) {
            // Add an effect to the user that just got voted for!
            if ($(target).attr("id") == "goldVote") {
                var offset = $(target).offset();
                // Top Right
                explode(offset.left + ($(target).width()), offset.top, 50, true);

                // Top Left
                explode(offset.left + (0), offset.top, 50, true);

                // Bottom Right
                explode(offset.left + ($(target).width()), offset.top + ($(target).height()), 50, true);

                // Bottom Left
                explode(offset.left + (0), offset.top + ($(target).height()), 50, true);
            }
        });
    });

    var $goldVote = $("#goldVote");
    var $silverVote = $("#silverVote");
    var $bronzeVote = $("#bronzeVote");

    /**
     * Client side validation of the vote
     */
    function isValidVote() {
        if ($goldVote.children().length != 1) {
            return false;
        }
        if ($silverVote.children().length != 1) {
            return false;
        }
        if ($bronzeVote.children().length != 1) {
            return false;
        }
        return true;
    }
    var $submitVote = $("#submitVote");
    // Submit the vote to get validated and handled by the PHP
    $submitVote.click(function() {
        if (isValidVote()) {
            var goldTeamID = $goldVote.children().eq(0).attr("team_id");
            var silverTeamID = $silverVote.children().eq(0).attr("team_id");
            var bronzeTeamID = $bronzeVote.children().eq(0).attr("team_id");

            console.log("User voting for Gold: " + goldTeamID + " Silver: " + silverTeamID + " Bronze: " + bronzeTeamID);

            $.post("php/sendVote.php", { "goldID": goldTeamID, "silverID": silverTeamID, "bronzeID": bronzeTeamID }, function(error) {
                createAlert("Notice", "Vote Successful!", $("body"), true);
                $(".voteContainer").empty();
                $(".voteContainer").detach();
                loadChart("#vote-results", "null");
                scrollToTop();
            }).fail(function() {
                createAlert("Alert!", "The vote has failed to send!", $("body"), false);
                scrollToTop();
            });
        } else {
            createAlert("Invalid Vote!", "The vote was invalid, make sure gold, silver, and bronze have a user in it.", $("body"), false);
            scrollToTop();
        }
    });

    //Checks if the user is on a given team
    function checkTeams(username, teamArray) {
        var hasOwnName = false;
        for (var i in teamArray) {
            if (username == teamArray[i].trim()) {
                hasOwnName = true;
            }
        }
        return hasOwnName
    }
    // Adds a comment
    $("#addComment").click(function() {
        var team_id = $("#teamSelectList option:selected").attr("team_id");
        var commentText = $("#commentText").val();

        if (commentText != "") {
            $.post("php/writeIn.php", { "comment_team": team_id, "comment": commentText }, function(error) {
                createAlert("Notice", "Comment Added!", $("body"), true);
                populateCommentList('null');
                $("#commentText").val('');
            }).fail(function() {
                createAlert("Alert!", "Comment failed to be added.", $("body"), false);
                scrollToTop();
            });
        } else {
            createAlert("Alert!", "Cannot give a blank comment", $("body"), false);
        }
    });


    // Comment text counter
    // Updates when the user types or backspaces on it
    var $commentNumberator = $("#commentNumberator");
    var $commentText = $("#commentText");
    $commentText.keyup(function(event) {
        if (event.which == 13) {
            event.preventDefault();
        }
        $commentNumberator.text($commentText.val().length);
    });
});