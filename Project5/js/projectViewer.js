// Gets the URL parameter from the current URL, this is how the project/user is determined
var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
        }
    }
};

/**
 * If the URL parmeter is a project return true
 */
function isProject() {
    return !!getUrlParameter("project");
}

/**
 * If the URL parmeter is a user return true
 */
function isUser() {
    return !!getUrlParameter("user");
}


$(function() {
    var $siteIframe = $("#siteIframe");
    var $rightTooltip = $("#rightTooltip");
    var $leftTooltip = $("#leftTooltip");
    var $rightUserTooltip = $("#rightUserTooltip");
    var $leftUserTooltip = $("#leftUserTooltip");
    var $siteTitle = $("#siteTitle");
    var $navLeft = $("#navLeft");
    var $navRight = $("#navRight");
    var $navUserLeft = $("#navUserLeft");
    var $navUserRight = $("#navUserRight");
    var projectNumber;
    var projectMax = 1000000000;

    // Open in a new tab if the site title is clicked
    $siteTitle.click(function() {
        window.open($siteIframe.attr('src'), '_blank');
    });

    // Open in a new tab button
    $("#openInNewTab").click(function() {
        window.open($siteIframe.attr('src'), '_blank');
    });

    /**
     * Loads the page based on the siteIndex and userData
     * this is what handles the tooltips, loading the page
     * and setting the titles
     */
    function loadPage() {
        var username = userData[siteIndex].username;
        console.log("Loading " + username + "'s Page...");

        var url = urlList[siteIndex];
        if (!url || url == "") {
            url = "viewerErrorPage.php";
            $siteIframe.attr('src', url);
        } else {

            // Attempt to get the page. If that fails then open in a new tab
            // If it succeeds then open it!
            $.get(url, function() {
                $siteIframe.attr('src', url);
            }).fail(function() {
                console.log("Failed to load, opening in new tab");

                // THIS IS THE LINE THAT OPENS IN A NEW TAB AUTOMATICALLY
                window.open(url, '_blank');
                $siteIframe.attr('src', "viewerErrorPage.php");
            });
        }

        $siteTitle.text("Project " + projectNumber + ": " + userData[siteIndex].full_name);

        // if (isUser()) {
        //     $siteTitle.text($siteTitle.text() + " Project " + (siteIndex + 1));
        // }

        // Update the nav icons
        if (siteIndex == 0) {
            $navUserLeft.addClass("disabled-icon");
        } else if (siteIndex == userData.length - 1) {
            $navUserRight.addClass("disabled-icon");
        } else if (siteIndex > 0) {
            $navUserLeft.removeClass("disabled-icon");
            if (siteIndex < userData.length - 1) {
                $navUserRight.removeClass("disabled-icon");
            }
        }

        if (projectNumber == 1) {
            $navLeft.addClass("disabled-icon");
        } else if (projectNumber == projectMax - 1) {
            $navRight.addClass("disabled-icon");
        } else if (projectNumber >= 1) {
            $navLeft.removeClass("disabled-icon");
            if (projectNumber < projectMax - 1) {
                $navRight.removeClass("disabled-icon");
            }
        }

        // Update the tooltips
        if (siteIndex == 0) {
            $leftUserTooltip.attr("aria-label", "Disabled");
        }
        if (siteIndex == userData.length - 1) {
            $rightUserTooltip.attr("aria-label", "Disabled");
        }
        if (siteIndex <= userData.length - 1 && siteIndex > 0) {
            $leftUserTooltip.attr("aria-label", userData[siteIndex - 1].tooltip);
        }
        if (siteIndex + 1 < userData.length) {
            $rightUserTooltip.attr("aria-label", userData[siteIndex + 1].tooltip);
        }

        // Update the tooltips
        if (projectNumber == 1) {
            $leftTooltip.attr("aria-label", "Disabled");
        }
        if (projectNumber == projectMax - 1) {
            $rightTooltip.attr("aria-label", "Disabled");
        }
        if (projectNumber <= projectMax - 1 && projectNumber > 1) {
            $leftTooltip.attr("aria-label", "Project " + (projectNumber - 1));
        }
        if (projectNumber + 1 < projectMax) {
            $rightTooltip.attr("aria-label", "Project " + (projectNumber + 1));
        }
    }

    // console.log(getUrlParameter("position"));

    var siteIndex = 0;
    var userData = [];
    var urlList = [];

    // Gets a list of all the Non-Admin users (because admin users won't have projects)
    // getNonAdminUsers(function(userList) {

    // If it is a project, then we sort by each user
    function getProjectData(newProjectNumber, firstTimeLoad) {
        projectNumber = newProjectNumber;
        getProjectIDFromLocalId(projectNumber, function(projId) {
            $.post("php/getUrl.php", { "project_id": projId }, function(dbUrlList) {
                var isEmpty = true;
                dbUrlList = JSON.parse(dbUrlList);
                for (var i in dbUrlList) {
                    if (dbUrlList[i].url && dbUrlList[i].url != "") {
                        isEmpty = false;
                    }
                }
                if (!dbUrlList || dbUrlList == "null" || isEmpty) {
                    if (isEmpty) {
                        projectMax = projectNumber;
                        projectNumber -= 1;
                    }
                    $rightTooltip.attr("aria-label", "Disabled");
                    $navRight.addClass("disabled-icon");
                    createAlert("Alert!", "Teams haven't been set for that project " + projectNumber + " yet!", $("body"), false);
                    scrollToTop();
                } else {
                    urlList = [];
                    userData = [];
                    console.log("Loading Projects...");

                    for (var i in dbUrlList) {
                        dbUrlList[i].tooltip = dbUrlList[i].full_name;
                        urlList.push(dbUrlList[i].url);
                    }
                    userData = dbUrlList;

                    if (firstTimeLoad) {
                        var userPosition = getUrlParameter("username");
                        if (userPosition) {

                            var newPos = -1;
                            // find user position in userData
                            for (var i in userData) {
                                var tempUsername = userData[i].username;
                                if (tempUsername == userPosition) {
                                    newPos = parseInt(i);
                                    break;
                                }
                            }
                            if (newPos == -1) {
                                createAlert("Alert!", "That user doesn't exist!", $("body"), false);
                                scrollToTop();
                            } else {
                                siteIndex = newPos;
                                // loadPage(urlList[siteIndex]);
                                try {
                                    loadPage();
                                } catch (e) {
                                    createAlert("Alert!", "That project doesn't exist yet...", $("body"), false);
                                }
                            }
                        } else {
                            loadPage();
                        }
                    } else {
                        try {
                            loadPage();
                        } catch (e) {
                            createAlert("Alert!", "That project doesn't exist yet...", $("body"), false);
                        }
                    }
                    // loadPage(urlList[siteIndex]);
                }
            });
        });
    }
    if (isProject()) {
        getProjectData(parseInt(getUrlParameter("project")), true);
    }
    // else if (isUser()) { // If it a user then we sort by that user's project
    //     var username = getUrlParameter("user");
    //     $.post("php/getUrl.php", { "username": username }, function(dbUrlList) {
    //         dbUrlList = JSON.parse(dbUrlList);
    //         if (!dbUrlList || dbUrlList == "null") {
    //             createAlert("Alert!", "The requested user: " + getUrlParameter("user") + " was not found.", $("body"), false);
    //             scrollToTop();
    //         } else {
    //             console.log("Loading User...");

    //             for (var i in dbUrlList) {
    //                 urlList.push(dbUrlList[i].url);
    //                 // The JSON parsing ensures a deep copy
    //                 var userInfo = JSON.parse(JSON.stringify({}));
    //                 userInfo.tooltip = "Project " + (parseInt(i) + 1);
    //                 userInfo.username = username;
    //                 userInfo.full_name = dbUrlList[i].full_name;

    //                 userData.push(userInfo);
    //             }

    //             // Use the position that the user passed in if it is valid
    //             // and if it is in the current range of projects
    //             var position = getUrlParameter("position");
    //             if (position) {
    //                 // -1 because it is zero index
    //                 if (parseInt(position) > urlList.length && parseInt(position) >= 0) {
    //                     createAlert("Alert!", "Teams haven't been set for that project " + parseInt(position) + " yet!", $("body"), false);
    //                     scrollToTop();
    //                 } else {
    //                     siteIndex = getUrlParameter("position") - 1;
    //                     loadPage(urlList[siteIndex]);
    //                 }
    //             } else {
    //                 loadPage(urlList[siteIndex]);
    //             }
    //         }
    //     });
    // }

    // Handle clicking on the < and > arrows in the nav bar
    // Ensure to not let the the nav bar to go < 0 or more than userData's length
    $navLeft.click(function() {
        if (!$navLeft.hasClass("disabled-icon")) {
            if (projectNumber > 1) {
                projectNumber -= 1;
            }
            getProjectData(projectNumber, false);
        }
    });
    $navRight.click(function() {
        if (!$navRight.hasClass("disabled-icon")) {
            if (projectNumber < projectMax) {
                projectNumber++;
            }
            getProjectData(projectNumber, false);
        }
    });


    $navUserLeft.click(function() {
        if (!$navUserLeft.hasClass("disabled-icon")) {
            siteIndex = siteIndex > 0 ? siteIndex - 1 : 0;
            loadPage();
        }
    });
    $navUserRight.click(function() {
        if (!$navUserRight.hasClass("disabled-icon")) {
            siteIndex = siteIndex < userData.length - 1 ? 1 + siteIndex : userData.length - 1;
            loadPage();
        }
    });

});