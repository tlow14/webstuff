$(function() {
    // Populate the course dropdown
    var $courseList = $(".courseList");
    getCourses(function(courseList) {
        populateCourseListHelper(courseList);

        // Select the current course at #currentCourseId, which is the session variable from PHP $_SESSION["course_id"]
        var courseId = $("#currentCourseId").val();
        $courseList.val($("body").find(`option[course_ID="${courseId}"]`).eq(0).val());
    });

    // When the course is changed, switch the #courseID field, which will be sent to PHP 
    // because the admin's session variable will get changed.
    $courseList.on("change", function() {
        var courseID = $(".courseList option:selected").attr("course_ID");
        $("#courseID").val(courseID);
    });
});