<?php
// Initialize the session
session_start();

require_once('php/helpers.php');
// require_once('php/dbconnect.php');
 
// Include config file
$mysqli = mysqli_connect('james.cedarville.edu', 'cs3220', '', 'cs3220_Sp20') or die('Database connect error.');

// For Navbar.php
$currentPage = 'Home';

function isOpenProj() {
    return isProjectOpen($mysqli);
}

// Define variables and initialize with empty values
$username = $password = "";
$username_err = $password_err = "";
// Processing form data when form is submitted

//If in login mode
if($_SERVER["REQUEST_METHOD"] == "POST") {

    if(isset($_POST["login_qwerty_password"])) {

        
        // Check if username is empty
        if(empty(trim($_POST["login_qwerty_username"]))){
            $username_err = "Please enter username.";
        } else{
            $username = trim($_POST["login_qwerty_username"]);
        }
        // Check if password is empty
        if(empty(trim($_POST["login_qwerty_password"]))){
            $password_err = "Please enter your password.";
        } else{
            $password = trim($_POST["login_qwerty_password"]);
        }
        // Validate credentials
        if(empty($username_err) && empty($password_err)){

            // Prepare a select statement
            $sql = "SELECT full_name, password, course_id, role, iconUrl, subtitle FROM qwerty_pc_user WHERE username = ?";
            if($stmt = mysqli_prepare($mysqli, $sql)){
                // Bind variables to the prepared statement as parameters
                mysqli_stmt_bind_param($stmt, "s", $param_username);
                
                // Set parameters
                $param_username = $username;
                
                // Attempt to execute the prepared statement
                if(mysqli_stmt_execute($stmt)){

                    // Store result
                    mysqli_stmt_store_result($stmt);
                    
                    // Check if username exists, if yes then verify password
                    if(mysqli_stmt_num_rows($stmt) == 1){                    
                        // Bind result variables
                        mysqli_stmt_bind_result($stmt, $full_name, $hashed_password, $course_id, $role, $iconUrl, $subtitle);
                        if(mysqli_stmt_fetch($stmt)){
                            if(password_verify($password, $hashed_password)){
                                    //  is correct, so start a new session
                                    session_start();
                                    // Store data in session variables
                                $_SESSION["loggedin"] = true;
                                // $_SESSION["id"] = $id;
                                $_SESSION["full_name"] = $full_name;
                                $_SESSION["username"] = $param_username;
                                $_SESSION["role"] = $role;
                                $_SESSION["iconUrl"] = $iconUrl;
                                $_SESSION["subtitle"] = $subtitle;

                                // If it is an admin, then we don't want to force the DB course for them. 
                                // We want to enforce whatever course they chose on the dropdown
                                // They can change it later in the admin page if they decide to
                                if($role !== 'admin') {
                                    $_SESSION["course_id"] = $course_id;
                                }

                                // echo "CourseID: ";
                                // echo $_SESSION["course_id"];
                                
                            } else{
                                // Display an error message if password is not valid
                                $password_err = "The password you entered was not valid.";
                            }
                        }
                    } else{
                        // Display an error message if username doesn't exist
                        $username_err = "No account found with that username.";
                    }
                } else{
                    echo "Oops! Something went wrong. Please try again later.";
                }

                // Close statement
                mysqli_stmt_close($stmt);
            }
        }

        //If in create account mode
    }

    // Close connection
    mysqli_close($mysqli);
}
?>
<html lang="en">

<head>
    <title>People's Choice</title>
    <?php require('header.php'); ?>
    <script src="js/index.js"></script>
</head>

<body class="theme-light page-background font-sans" >
    <!-- Login Alerts -->
    <div class="<?php echo (!empty($password_err)) ? '' : 'hidden'; ?> bg-red-400 z-10 border border-red-400 text-white px-4 py-3 rounded relative my-0 flex flex-row justify-between"><div><p class="font-bold">Alert!</p><p class="text-sm"><?php echo htmlentities($password_err); ?></p></div><div><i class="fa fa-times-circle text-lg cursor-pointer m-4" aria-hidden="true" onclick="this.parentElement.parentElement.style.display='none';"></i></div></div>
    <div class="<?php echo (!empty($username_err)) ? '' : 'hidden'; ?> bg-red-400 z-10 border border-red-400 text-white px-4 py-3 rounded relative my-0 flex flex-row justify-between"><div><p class="font-bold">Alert!</p><p class="text-sm"><?php echo htmlentities($username_err); ?></p></div><div><i class="fa fa-times-circle text-lg cursor-pointer m-4" aria-hidden="true" onclick="this.parentElement.parentElement.style.display='none';"></i></div></div>
    <div id="overlay" class="hideOverlay fixed justify-center flex w-screen h-screen bg-gray-700 bg-darken-2 z-10">
            <div class="flipside-wrapper bg-secondary rounded-lg shadow-2xl p-2 flex flex-col">
                <h2 class="text-primaryText text-2xl mx-2"><span class="font-bold">Notice</span> the voting poll is open!</h2>
                <p class="text-secondaryText text-center text-sm font-light mx-2">Click below to go there now!</p>
                <a href="vote.php" class="mt-4 button-secondary text-center text-primaryText">Vote</a>
            </div>
        </div>
    <?php require('navbar.php'); ?>

    <!-- PROJECT CONTAINER -->
    <div class="w-screen-1 mb-20 mx-auto">
        <!-- TITLE -->
        <div class="w-screen-1">
            <div class="flex flex-row w-full mx-auto p-2 justify-center items-center">
                <img class="h-8 w-8 md:w-12 md:h-12 lg:h-16 lg:w-16 my-auto mr-4" src="images/logo.png" />
                <h2 id="mainTitle" class="text-3xl md:text-4xl lg:text-5xl xl:text-6xl text-ternaryText slideFadeRight2 select-none" style="text-shadow: 0 2px 5px rgba(0, 0, 0, 0.25);">People's Cho<span data-balloon-length="xlarge" data-balloon-pos="down" aria-label="We the People of the United States, in Order to form a more perfect Union, establish Justice, insure domestic Tranquility, provide for the common defence, promote the general Welfare, and secure the Blessings of Liberty to ourselves and our Posterity, do ordain and establish this Constitution for the United States of America.">i</span>ce</h2>
            </div>
        </div>
        <div id="table-view" class="hidden flex items-center justify-center w-screen">
            <div class="container">
                <table class="w-full flex flex-row flex-no-wrap sm:bg-white rounded-lg overflow-hidden sm:shadow-lg my-5">
                    <thead class="text-white">
                        <tr class="bg-ternary text-primaryBgText flex flex-col flex-no wrap sm:table-row rounded-l-lg sm:rounded-none mb-2 sm:mb-0">
                            <th class="p-3 text-left">Name</th>
                            <?php 
                                $projectList = getProjects($mysqli);
                                    $i = 0;
                                    foreach ($projectList as $project) {
                                        $i++;
                                    ?>
                                    <th class="p-3 text-left text-center">Project <?php echo $i; ?></th>
                                <?php } ?>
                        </tr>
                    </thead>
                    <tbody id="table-body" class="flex-1 sm:flex-none">
                        <tr id="table-row-template" class="flex flex-col flex-no wrap sm:table-row mb-2 sm:mb-0 bg-secondary">
                            <td data-href="#" class="userCol border-grey-light border hover:bg-ternary flex flex-row cursor-pointer items-center">
                                <img class="row_icon h-12 w-12" src="images/noUserIcon.png"/>
                                <div class="flex flex-col mx-2">
                                    <span class="row_username text-primaryText"></span>
                                    <span class="row_subtitle text-secondaryText"></span>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>



        <!-- CARD VIEW -->
        <div class="flex justify-center w-screen-1">
        <div id="card-view" class="mgrid w-screen-1 content-center grid xl:grid-cols-4 lg:grid-cols-3 md:grid-cols-2 sm:grid-cols-1 grid-flow-row sm:grid-flow-row md:grid-flow-row-dense">
            <!-- <div class="mgrid flex flex-wrap"> -->
            <!-- <div id="gridSizer" class="w-third--1 mx-2"></div> -->

            <!-- sm:w-full md:w-third--1 lg:w-third--1 -->

            <div id="card-template" class="card slideUp mx-2 w-full-1 overflow-hidden">
                <div class="frontside p-2">
                    <!-- This is where we would put the acheievements -->
                    <!-- <div class="card-header border-b flex">
                    <img src="https://c7.uihere.com/files/637/816/57/gold-medal-olympic-medal-clip-art-medal.jpg" class="h-6 w-6" />
                </div> -->
                    <div class="card-body px-6 flex w-full">
                        <div class="flex-shrink-0">
                            <img src="images/noUserIcon.png" alt="User Icon" class="card_icon rounded-full h-12 w-12" />
                        </div>
                        <div class="ml-6 flex flex-col items-center justify-start">
                            <h4 class="card_username text-primaryText text-xl w-full">Nathan O'Neel</h4>
                            <p class="card_subtitle text-secondaryText text-base w-full"></p>
                        </div>
                        <div class="flex flex-row flex-1 justify-end">
                            <a href="#" class="courseHomePage mt-2">
                                <i class="fa fa-home fa-2x text-secondaryText" aria-hidden="true"></i>
                            </a>
                        </div>
                    </div>
                    <span class="text-secondaryText ml-4 mr-1 text-sm">Projects:</span>
                    <div class="flex flex-row items-center">
                        <div class="card-footer text-center grid gap-2 grid-cols-8 mx-auto">
                        <?php 
                            $projectList = getProjects($mysqli);
                            $i = 0;
                            foreach ($projectList as $project) {
                                $i++;
                            ?>
                            <a href="#" class="card-link<?php echo $i; ?> card-link circled hover:bg-ternary"><span class="text-primaryText"><?php echo $i; ?></span></a>
                        <?php } ?>
                        </div>
                    </div>
                </div>
                <!-- Frontside -->
                <div class="flipside">
                    <div class="flipside-wrapper mx-4 w-full md:mx-0 md:w-2/3 lg:w-1/2 bg-secondary rounded-lg shadow-2xl">
                        <div class="card-header p-5 border-b flex flex-row items-center">
                            <div class="flex flex-row w-1/2">
                            <img src="images/noUserIcon.png" alt="" class="card_icon rounded-full h-16 w-16" />
                            <div class="flex flex-col items-center">
                                <h2 class="card_username slideFadeRight text-primaryText ml-5 text-xl font-medium tracking-wide w-full">Nathan O'Neel</h2>
                                <p class="card_subtitle slideFadeRight2 text-secondaryText ml-5 text-base w-full"></p>
                            </div>
                            </div>
                            <a href="#" class="courseHomePage w-1/2 bg-ternary flex flex-row justify-center rounded items-center font-bold p-2 m-1 text-primaryText text-center">
                                <i class="fa fa-home fa-2x mr-2" aria-hidden="true"></i>
                                <span>Course Page</span>
                            </a>
                        </div>
                        <div class="card-content p-5">
                            <!-- <div class="grid grid-cols-3 gap-4 text-center"> -->
                            <div class="flex flex-wrap">
                            <?php 
                                $projectList = getProjects($mysqli);
                                $i = 0;
                                foreach ($projectList as $project) {
                                    $i++;
                            ?>
                                <a href="#" class="card-link<?php echo $i; ?> card-link <?php if($i == count($projectList) && $i % 2 == 1) {echo 'w-2/3 mx-auto';} else { echo 'w-half--1';} ?> bg-ternary flex flex-col rounded justify-center items-center font-bold p-2 m-1 text-primaryText text-center">
                                    <span class="medalLocation flex flex-row justify-center items-center">Project <?php echo $i; ?></span>
                                    <div class="cardLinkComments"></div>
                                </a>
                            <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Flipside -->
            </div>
            <!-- Card Template -->
        </div>
        <!-- Grid -->
        </div>
    </div>
    <!-- Card Wrapper -->
    <script src="js/dependencies/jquery.ui.min.js"></script>
</body>

</html>