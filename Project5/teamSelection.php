<?php
session_start();


// If the user isn't an admin redirect them!
if(!isset($_SESSION['role'] ) || $_SESSION["role"] !== 'admin') {
    header("location: index.php");
}
?>
<html lang="en">

<head>
    <title>PC | Team Select</title>
    <?php require('header.php'); ?>
    <script src="js/teamSelection.js"></script>
</head>

<body class="theme-light page-background font-sans"></body>
<div class="card-header flex justify-center text-ternaryText text-4xl bg-secondary">
    <a href="admin.php" class="circled back-button m-2 text-primary border-primary">
        <i class="h-10 w-10" aria-label="Home Page" data-balloon-pos="right">
            <i class="fa fa-arrow-left cursor-pointer" aria-hidden="true"></i>
        </i>
    </a>
    <h3 class="text-center text-primary p-4">Team Selection</h3>
</div>
<div class="main-wrapper content-center flex flex-col mx-auto w-screen">
    <div class="w-2/3 p-2 mx-auto bg-secondary mt-2 rounded-lg">
        <h2 class="text-center text-primaryText text-2xl mb-1">Select Project</h2>
        <select value="project" class="projectList text-primaryText cursor-pointer block appearance-none w-full bg-secondary border border-gray-400 hover:border-gray-500 px-4 py-2 pr-8 rounded shadow leading-tight focus:outline-none focus:shadow-outline">
        </select>
    </div>
    <div class="content-center mt-10 mx-auto flex flex-row w-full justify-center">
        <p id="autoAssign" class="button-ternary tinted m-4">Auto Assign One Player/Team</p>
        <!-- <p id="addTeam" class="button-ternary m-4">Add Team</p>
        <p id="removeTeam" class="button-ternary m-4">Remove Team</p> -->
        <p id="clearTeams" class="button-ternary tinted m-4">Clear Teams</p>
        <p id="submitTeams" class="button-ternary tinted m-4" onclick="submitTeam()">Submit Teams</p>
    </div>
    <div class="mx-auto rounded p-6 m-2 w-full mb-64" id="TeamSelection">
        <div class="username-grid bg-secondary rounded-lg grid grid-cols-8 p-6 gap-2"></div>
        <div class="team-grid grid grid-cols-6 gap-2 p-6" id="TeamSelectionGrid">
            <!-- <div class="bg-secondary p-6 rounded-lg" id="teamNumber1">
                <h3 class="team-title" value="1" >Team 1</h3>
                <div id="team1" class="team-container username-list">
                </div>
            </div>
            <div class="bg-secondary p-6 rounded-lg" id="teamNumber2">
                <h3 class="team-title" value="2">Team 2</h3>
                <div id="team2" class="team-container">
                </div>
            </div> -->
        </div>
    </div>
</div>
</body>

</html>