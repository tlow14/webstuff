<?php 

require_once('php/dbconnect.php');
require_once('php/helpers.php');

// For Navbar.php
$currentPage = 'viewResults';
?>
<html lang="en">

<head>
    <title>PC | Results</title>

    <?php require('header.php'); ?>
    <script src="js/viewResults.js"></script>

    <style>
        .activeTab {
            background-color: var(--bg-background-secondary);
            color: var(--text-copy-primary);
        }
        
        .activeView {
            display: block !important;
        }
    </style>
</head>

<body class="font-sans theme-light bg-primary">
    <?php require('navbar.php'); ?>

    <div class="flex justify-center items-center h-full w-full">
        <div class="w-full h-full mt-2">
        <div class="flex flex-wrap mx-auto justify-center overflow-hidden">
            <!-- <div class="flex -mx-px h-10 bg-primary ml-2 text-ternaryText"> -->
                
            <?php 
            $projectList = getProjects($mysqli);
            $i = 0;
            foreach ($projectList as $project) {
                $i++;
            ?>

            <button tab="<?php echo $i; ?>" class="tab <?php if($i == 1) echo "activeTab";?> my-1 text-ternaryText border-b-2 border-secondary p-2 bg-transparent hover:bg-gray-200 text-sm md:text-base font-semibold rounded-t focus:outline-none mx-px py-px md:py-2 px-3 md:px-4" type="button">
            <!-- <div class="my-1 px-1 w-1/6 overflow-hidden"> -->
            <?php echo htmlentities($project['name']); ?>
            <!-- </div> -->
            </button>
            <?php } ?>

            </div>
            <ul class="text-sm rounded-b p-4 bg-primary">
            <?php 
            $projectList = getProjects($mysqli);
            $i = 0;
            foreach ($projectList as $project) {
                $i++;
            ?>
                <li id="view<?php echo $i; ?>" class="view <?php if($i == 1) echo "activeView";?> hidden w-full">
                    <div id="project<?php echo $i; ?>">
                        <div class="sm:w-full lg:w-2/3 xl:w-2/3 mx-auto flex flex-col justify-center">
                            <div class="tinted rounded-lg p-3">
                                <canvas id="result<?php echo $i; ?>" class="projectResults w-full rounded-lg"></canvas>
                            </div>
                            
                            <h2 class="w-full text-center text-4xl font-bold text-ternaryText m-2">Write In Awards</h2>
                            <?php 
                                // $commentList = getComments($mysqli, $project['id']); 
                                // foreach ($commentList as $comment) {
                            ?>
                            <?php require('comment.php'); ?>
                            <?php // } ?>
                        </div>
                    </div>
                </li>
                <?php } ?>
            </ul>
        </div>
    </div>
</body>

</html>