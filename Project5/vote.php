<?php

require_once('php/dbconnect.php');
require_once('php/helpers.php');
// require_once('php/writeIn.php');

$projectID = isProjectOpen($mysqli);
if($projectID == "false") {
    header("location: index.php");
}


?>
<html lang="en">
<head>
    <title>PC | Vote</title>

    <?php require('header.php'); ?>

    <link rel="stylesheet" href="css/vote.css">
    <script src="js/vote.js"></script>
</head>

<body class="theme-light page-background font-sans"></body>
<div class="card-header flex justify-center text-ternaryText text-lg lg:text-2xl xl:text-4xl bg-secondary">
    <a href="index.php" class="circled back-button m-2 text-primary border-primary">
        <i class="h-2 w-2 md:h-4 md:w-4 lg:h-10 lg:w-10" aria-label="Home Page" data-balloon-pos="right">
            <i class="fa fa-arrow-left cursor-pointer" aria-hidden="true"></i>
        </i>
    </a>
    <p id="usernameInput" class="hidden"><?php echo htmlentities($_SESSION["full_name"])?></p>
    <h3 id="titleMessage" class="text-center text-primary p-4"><?php // echo htmlentities($_SESSION["full_name"])?><!--, Welcome to -->Voting for 

        <?php $projects = getProjects($mysqli);
            $openProject = isProjectOpen($mysqli);
            foreach ($projects as $project) {
                if($project['id'] === $openProject) {
                    echo $project['name']; 
                }
            }
        ?>
    </h3>
</div>
<div class="main-wrapper content-center flex flex-col mx-auto w-screen text-primary">
    <div class="">
        <div class="my-4 sm:w-full lg:w-2/3 xl:w-2/3 mx-auto tinted rounded-lg p-3">
            <canvas id="vote-results"></canvas>
        </div>
    </div>

    <?php // check that user hasn't not voted
        if (!userHasVoted($mysqli, $_SESSION["username"], $projectID)) { ?>
    <!-- Hide if the user has already voted -->
    <div class="voteContainer mx-auto rounded p-6 m-2 w-full">
        <div class="teamlist-grid grid grid-cols-2 md:grid-cols-4 lg:grid-cols-6 p-6 gap-2 bg-secondary w-full rounded-lg">

        </div>
        <div class="team-grid grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-1 lg:gap-10 p-0 my-2 lg:my-0 lg:p-6">
            <div class="bg-secondary p-6 rounded-lg">
                <h3 class="team-title">Gold</h3>
                <div id="goldVote" class="vote-container p-2 min-w-full border-2 border-gold"></div>
            </div>
            <div class="bg-secondary p-6 rounded-lg">
                <h3 class="team-title">Silver</h3>
                <div id="silverVote" class="vote-container p-2 min-w-full border-2 border-silver"></div>
            </div>
            <div class="bg-secondary p-6 rounded-lg">
                <h3 class="team-title">Bronze</h3>
                <div id="bronzeVote" class="vote-container p-2 min-w-full border-2 border-bronze"></div>
            </div>
        </div>
        <div class="content-center mx-auto flex flex-row w-full justify-center">
            <p id="submitVote" class="button-ternary tinted">Submit Vote</p>
        </div>
    </div>
        <?php } ?>

    <div class="mx-auto mb-20 rounded pb-4 p-6 w-full lg:w-2/3 bg-ternary mt-4">
    <?php require('comment.php'); ?>
    <!-- <div id="commentWrapper" class="text-primaryText">
        <div id="templateComment" class="hidden sortTeam flex flex-col items-start card-footer m-2 max-w-4xl px-10 py-6 bg-white rounded-lg shadow-md">
            <div class="w-full">
                <p class="flex items-baseline">
                    <div id="profileIconList" class="flex flex-row items-center">
                    </div> 
                    <div id="commentNameList" class="w-full font-bold text-2xl">
                    </div>
                </p>
                <div id="commentList" class="mt-3">
                </div>
            </div>
        </div> 
    </div> -->
    <div class="mt-8 sm:mt-0 sm:w-full text-primaryText">
        <form>
                <div class="box border rounded flex flex-col h-40 shadow bg-secondary">
                    <div class="box__title px-3 py-2 border-b">
                        <div class="flex flex-row">
                            <h3 class="text-sm flex-1 text-primaryText font-medium">New Write-In to</h3>
                            <p class="text-secondaryText"><span id="commentNumberator">0</span> / 160</p>
                        </div>
                            <select id="teamSelectList" class="text-primaryText cursor-pointer block appearance-none w-full bg-secondary border border-gray-400 hover:border-gray-500 px-4 py-2 pr-8 rounded shadow leading-tight focus:outline-none focus:shadow-outline">
                            </select>
                    </div>
                    <input id="commentTeamId" hidden type="text" value="team_id">
                    <textarea id="commentText" maxlength="160" class="text-grey-darkest flex-1 p-2 m-1 bg-transparent" name="comment"></textarea>
                </div>
                <div class="w-full mt-4">
                    <p id="addComment" class="button-secondary text-center">Add Write-In</p>
                </div>
            </form>
        </div>
    </div>
</div>
</body>

</html>

<?php 
$mysqli->close(); 
?>