<?php 
session_start();

if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] === false) {
    header("location: index.php");
}
require_once('php/dbconnect.php');
// require_once('php/update.php');
// $mysqli = mysqli_connect('james.cedarville.edu', 'cs3220', '', 'cs3220_Sp20') or die('Database connect error.');
// Define variables and initialize with empty values
$username = $password = "";
$username_err = $password_err = "";

if($_SERVER["REQUEST_METHOD"] == "POST") {
    // if(empty(trim($_POST["create_qwerty_username"]))) {
    //         $username_err = "Please create username";
    // }else{
    //         $sql = "SELECT id FROM qwerty_user WHERE name = ?";
            
    //         if($stmt = mysqli_prepare($mysqli, $sql)){
    //             mysqli_stmt_bind_param($stmt, "s" , $param_username);

    //             $param_username = trim($_POST["create_qwerty_username"]);
    //             if(mysqli_stmt_execute($stmt)){
    //                 mysqli_stmt_store_result($stmt);
                    
    //                 if(mysqli_stmt_num_rows($stmt) == 1){
    //                     $username_err = 'This username is already taken.';
    //                 }else{
    //                     $username = trim($_POST["create_qwerty_username"]);
    //                 }

    //             }else{
    //                 echo "Something went wrong try agian another time";
    //             }
    //         }
    //     }

    if(!empty(trim($_POST["iconUrl"])) || !empty(trim($_POST["subtitle"]))) {


        if(empty(trim($_POST["iconUrl"]))){
            $icon_err = "Please enter a Icon.";     
        }
        
        if(empty(trim($_POST["subtitle"]))){
            $subtitle_err = "Please enter a subtitle.";     
        }


        //Updates Icon URL
        if(empty($icon_err)) {
            // Prepare an insert statement
            $sql = "UPDATE qwerty_pc_user set iconUrl=? where username=?";
            $iconUpdated = false;
            if($stmt = mysqli_prepare($mysqli, $sql)){

                $username = $_SESSION["username"];
                // Bind variables to the prepared statement as parameters
                mysqli_stmt_bind_param($stmt, "ss", $_POST["iconUrl"], $username);
                
                $_SESSION["iconUrl"] = $_POST["iconUrl"];

                // Attempt to execute the prepared statement
                if(mysqli_stmt_execute($stmt)){
                    // Redirect to login page
                    $iconUpdated = true;
                } else{
                    echo "Something went wrong saving icon Url. Please try again later.";
                }

                
    
                // Close statement
                mysqli_stmt_close($stmt);
            }
        }


        //Updates subtitle
        if(empty($subtitle_err)) {
            // Prepare an insert statement
            $sql = "UPDATE qwerty_pc_user set subtitle=? where username=?";
            if($stmt = mysqli_prepare($mysqli, $sql)){

                $username = $_SESSION["username"];
                // Bind variables to the prepared statement as parameters
                mysqli_stmt_bind_param($stmt, "ss", $_POST["subtitle"], $username);
            

                // Attempt to execute the prepared statement
                if(mysqli_stmt_execute($stmt)){
                    if($iconUpdated){
                        echo '<div class="bg-green-400 z-10 border border-green-400 text-white px-4 py-3 rounded relative my-0 flex flex-row justify-between"><div><p class=font-"bold">Alert!</p><p class="text-sm">Icon and Subtitle Updated!</p></div><div><i class="fa fa-times-circle text-lg cursor-pointer m-4" aria-hidden="true" onclick="this.parentElement.parentElement.style.display=\'none\';"></i></div></div>';
                    }else{
                        echo '<div class="bg-green-400 z-10 border border-green-400 text-white px-4 py-3 rounded relative my-0 flex flex-row justify-between"><div><p class="font-bold">Notice!</p><p class="text-sm">Profile Subtitle Updated!</p></div><div><i class="fa fa-times-circle text-lg cursor-pointer m-4" aria-hidden="true" onclick="this.parentElement.parentElement.style.display=\'none\';"></i></div></div>';
                    }    
                } else{
                    echo "Something went wrong saving subtitle. Please try again later.";
                }

                
    
                // Close statement
                mysqli_stmt_close($stmt);
            }
        }else{
            if($iconUpdated){
                echo '<div class="bg-green-400 z-10 border border-green-400 text-white px-4 py-3 rounded relative my-0 flex flex-row justify-between"><div><p class="font-bold">Notice!</p><p class="text-sm">Profile Icon Updated!</p></div><div><i class="fa fa-times-circle text-lg cursor-pointer m-4" aria-hidden="true" onclick="this.parentElement.parentElement.style.display=\'none\';"></i></div></div>';
            }
        }

    }else{


            // Validate password
        if(empty(trim($_POST["qwerty_password"]))){
            $password_err = "Please enter a password.";     
        } elseif(strlen(trim($_POST["qwerty_password"])) < 8){
            $password_err = "Password must have at least 8 characters.";
        } else{
            $password = trim($_POST["qwerty_password"]);
        }
        
        // Validate confirm password
        if(empty(trim($_POST["qwerty_confirm_password"]))){
            if(!$password_err){
                $confirm_password_err = "Please confirm password.";
            }     
        } else{
            $confirm_password = trim($_POST["qwerty_confirm_password"]);
            if(empty($password_err) && ($password != $confirm_password)){
                $confirm_password_err = "Password did not match.";
            }
        }

        if(empty($password_err) && empty($confirm_password_err)){
            // Prepare an insert statement
            $sql = "UPDATE qwerty_pc_user set password=? where username=?";
            if($stmt = mysqli_prepare($mysqli, $sql)){

                $username = $_SESSION["username"];
                // Bind variables to the prepared statement as parameters
                mysqli_stmt_bind_param($stmt, "ss", $param_password, $username);
                
                // Set parameters
                // $param_username = $username;
                $options = [
                    'cost' => 12,
                ];
                $param_password = password_hash($password, PASSWORD_BCRYPT, $options); // Creates a password hash
                
                // Attempt to execute the prepared statement
                if(mysqli_stmt_execute($stmt)){
                    // Redirect to login page
                    echo '<div class="bg-green-400 z-10 border border-green-400 text-white px-4 py-3 rounded relative my-0 flex flex-row justify-between"><div><p class="font-bold">Notice!</p><p class="text-sm">Password Updated!</p></div><div><i class="fa fa-times-circle text-lg cursor-pointer m-4" aria-hidden="true" onclick="this.parentElement.parentElement.style.display=\'none\';"></i></div></div>';
                } else{
                    echo "Something went wrong. Please try again later.";
                }
    
                // Close statement
                mysqli_stmt_close($stmt);
            }
        }
    }
}

?>
<html lang="en">

<head>
    <title>PC | Profile</title>
    
    <?php require('header.php'); ?>

    <script src="js/profile.js"></script>

    <style>
    .toggle__dot {
        top: -.25rem;
        left: -.25rem;
        transition: all 0.3s ease-in-out;
    }

    input:checked ~ .toggle__dot {
        transform: translateX(100%);
        /* background-color: var(--bg-background-secondary); */
        background-color: var(--bg-background-ternary);
    }
    input:checked ~ .toggle__dot i {
        color: var(--text-copy-primary);
        /* background-color: var(--bg-background-secondary); */
        border-radius: 50%;
    }
    </style>
</head>

<body class="theme-light page-background font-sans">

<!-- Login Alerts -->
<div class="<?php echo (!empty($password_err)) ? '' : 'hidden'; ?> bg-red-400 z-10 border border-red-400 text-white px-4 py-3 rounded relative my-0 flex flex-row justify-between"><div><p class="font-bold">Alert!</p><p class="text-sm"><?php echo htmlentities($password_err); ?></p></div><div><i class="fa fa-times-circle text-lg cursor-pointer m-4" aria-hidden="true" onclick="this.parentElement.parentElement.style.display='none';"></i></div></div>
<div class="<?php echo (!empty($confirm_password_err)) ? '' : 'hidden'; ?> bg-red-400 z-10 border border-red-400 text-white px-4 py-3 rounded relative my-0 flex flex-row justify-between"><div><p class="font-bold">Alert!</p><p class="text-sm"><?php echo htmlentities($confirm_password_err); ?></p></div><div><i class="fa fa-times-circle text-lg cursor-pointer m-4" aria-hidden="true" onclick="this.parentElement.parentElement.style.display='none';"></i></div></div>


<div class="card-header flex fixed justify-start text-ternaryText text-4xl">
    <a href="index.php" class="circled m-2">
        <i class="h-10 w-10" aria-label="Home Page" data-balloon-pos="right">
            <i class="fa fa-arrow-left cursor-pointer" aria-hidden="true"></i>
        </i>
    </a>
</div>
<div class="main-wrapper content-center flex flex-col mx-auto w-screen">
    <img class="rounded h-48 w-48 mx-auto" src="<?php echo (!empty($_SESSION["iconUrl"])) ? htmlentities($_SESSION["iconUrl"]) : 'images/noUserIcon.png'; ?>" />
    <div class="mx-auto rounded-lg m-2">

        <h2 class="slideFadeRight text-ternaryText text-center text-2xl mb-1">Hello, <?php echo htmlentities($_SESSION["full_name"]); ?></h2>
        <div>
        <div class="form-wrapper mx-auto text-center">
            <div class="shadow-lg p-2 rounded-lg bg-secondary">
                <h3 class="slideFadeRight2 text-seconaryText">Edit Profile</h3>
                <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" class="" data-balloon-length="xlarge" aria-label="Submitting will change both the Icon URL and the Subtitle. An Icon URL is any image URL on the internet. If you don't have any ideas yet, try this example! images/example.jpg . Note, for images not on this site you will need to include the full URL, including the http/https. Data URIs are not supported." data-balloon-pos="up">
                    <input name="iconUrl" value="<?php if($_SESSION["iconUrl"] != "images/noUserIcon.png") echo htmlentities($_SESSION["iconUrl"]);?>" maxlength="300" class="m-1 w-64 h-10 mr-2 appearance-none block bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" type="text" placeholder="Icon URL">
                    <input name="subtitle" value="<?php echo htmlentities($_SESSION["subtitle"]);?>" maxlength="100" class="m-1 w-64 h-10 mr-2 appearance-none block bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" type="text" placeholder="Subtitle">
                    <input type="submit" value="Save" class="m-1 button-secondary mx-auto text-center w-full" />
                </form>

                <div class="flex flex-row items-center justify-center w-full m-2 h-10">
                    <label for="darkmodeInput" class="m-2 mx-4" aria-label="Toggles Dark Mode" data-balloon-pos="up" class="flex items-center cursor-pointer">
                        <div class="relative">
                            <input id="darkmodeInput" type="checkbox" class="hidden" />
                            <div id="darkmodePath" class="toggle__line w-10 h-4 bg-gray-400 rounded-full shadow-inner"></div>
                            <div id="darkmodeToggle" class="toggle__dot absolute w-6 h-6 bg-primary rounded-full shadow inset-y-0 my-auto left-0 text-center">
                                <i id="darkmodeIcon" class="fa fa-moon-o w-6 h-6 table-cell align-middle text-ternaryText" aria-hidden="true"></i>
                            </div>
                        </div>
                    </label>
                    <label for="legacyViewInput" class="m-2 mx-4" aria-label="Toggles Legacy View on Home Page" data-balloon-pos="up" class="flex items-center cursor-pointer">
                        <div class="relative">
                            <input id="legacyViewInput" type="checkbox" class="hidden" />
                            <div id="legacyViewPath" class="toggle__line w-10 h-4 bg-gray-400 rounded-full shadow-inner"></div>
                            <div id="legacyViewToggle" class="toggle__dot absolute w-6 h-6 bg-primary rounded-full shadow inset-y-0 my-auto left-0 text-center">
                                <i id="legacyViewIcon" class="fa fa-table w-6 h-6 table-cell align-middle text-ternaryText" aria-hidden="true"></i>
                            </div>
                        </div>
                    </label>
                </div>
            </div>
        </div>
        <div class="shadow-lg p-2 rounded-lg bg-secondary mt-4 text-center" data-balloon-length="xlarge" aria-label="The links to all of your projects need to have a path, this allows you to assign them! Either input a relative method /~Username/Project#/index.php or a hard link like http://judah.cedarville.edu/~Username/Project#/index.php" data-balloon-pos="up">
            <h3 class="slideFadeRight2 text-seconaryText">Change Project Path</h3>
            <form method="post" class="">
                <select value="project" class="projectList text-primaryText py-3 px-4 m-1 mr-2 cursor-pointer block appearance-none w-64 bg-secondary border border-gray-400 hover:border-gray-500 rounded shadow leading-tight focus:outline-none focus:shadow-outline">
                </select>
                <input hidden id="username" value="<?php echo htmlentities($_SESSION["username"]);?>" type="text">
                <input id="projectUrl" class="m-1 w-64 h-10 mr-2 appearance-none block bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" type="text" placeholder="Project URL">
                <p id="urlSaveButton" class="m-1 button-secondary mx-auto text-center">Save</p>
            </form>
        </div>
        <div class="shadow-lg p-2 rounded-lg bg-secondary mt-4 text-center">
            <h3 class="slideFadeRight2 text-seconaryText">Change Password</h3>
            <form id="passwordForm" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post" class="">
                <input id="password" name="qwerty_password" class="m-1 w-64 h-10 mr-2 appearance-none block bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" type="password" placeholder="Password">
                <input id="confirmPassword" name="qwerty_confirm_password" class="m-1 w-64 h-10 mr-2 appearance-none block bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" type="password" placeholder="Confirm Password">
                <p id="submitPassword" class="m-1 button-secondary mx-auto text-center">Save</p>
            </form>
        </div>
            
    </div>
</div>
</body>

</html>