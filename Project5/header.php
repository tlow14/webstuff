<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">

<!-- <link rel="icon" type="image/x-icon" href="images/logo.png" /> -->
<link rel="icon" type="image/x-icon" href="images/favicon.png" />

<link href="https://fonts.googleapis.com/css?family=Montserrat:200,300,400,500&display=swap" rel="stylesheet">
<link rel="stylesheet" href="css/dragula.min.css">
<link rel="stylesheet" href="css/font-awesome.min.css">
<link rel="stylesheet" href="css/ballon.min.css">
<link rel="stylesheet" href="css/tailwind.css">
<link rel="stylesheet" href="css/style.css">

<script src="js/dependencies/jquery.js"></script>
<script src="js/dependencies/chart.js"></script>
<script src="js/dependencies/dragula.js"></script>
<script src="js/dependencies/cookie.js"></script>
<script src="js/main.js"></script>