<?php
    // header('X-Frame-Options: DENY');  
?>
<html lang="en">

<head>
    <title>PC | Viewer Error Page</title>
    <?php require('header.php'); ?>
</head>

<body class="theme-light page-background font-sans">

<div class="bg-secondary pb-4 pt-4 m-6 flex flex-col rounded-lg">
    <div class="m-2">
        <h2 class="text-2xl pb-2 text-primaryText font-bold text-center">Project Viewer Error Page</h2>
        <h2 class="text-sm pb-2 text-secondaryText font-bold text-center">Make sure you are allowing pop-ups from this website, because the project will open in a new tab.</h2>
        <h2 class="text-sm text-secondaryText font-bold text-center">The error could have happened for a few reasons. Please check the following:</h2>
        <div class="flex flex-col w-full lg:w-2/3 mx-auto p-4">
            <p>1. The path to the webpage is correctly set in your profile.</p>
            <p>2. The permissions on all files and directories are correct.</p>
            <p>3. You don't have "header('X-Frame-Options: DENY');" or any other X-Frame-Option in the PHP file.<p>
        </div>
    </div>
</div>
</body>

</html>