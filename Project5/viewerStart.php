<?php
    // header('X-Frame-Options: DENY');  
?>
<html lang="en">

<head>
    <title>PC | Viewer Start Page</title>
    <?php require('header.php'); ?>
    <script src="js/viewerStart.js"></script>
</head>

<body class="theme-light page-background font-sans">

<div class="bg-secondary pb-4 pt-4 m-6 flex flex-col rounded-lg">
    <div class="m-2">
        <h2 class="text-2xl pb-2 text-primaryText font-bold text-center">Project Viewer Starting Page</h2>
        <h2 class="text-sm pb-2 text-secondaryText font-bold text-center">Select an option and press the "View ..." button</h2>
    </div>
    <div class="sm:w-full md:w-3/4 lg:w-2/3 xl:w-1/2 mx-auto flex flex-col">
        <div class="flex flex-row m-2 justify-center">
            <div class="flex flex-col lg:flex-row">
                <p class="text-secondaryText mr-2 flex items-center justify-center w-32">Select Project</p>
                <select value="project" class="projectList text-primaryText cursor-pointer w-full lg:w-64 block appearance-none bg-secondary border border-gray-400 hover:border-gray-500 px-4 py-2 pr-8 rounded shadow leading-tight focus:outline-none focus:shadow-outline">
                </select>
            </div>
            <!-- <p id="viewProject" class="button-secondary ml-4 w-48 flex items-center justify-center" aria-label="Opens all User's Project at Project #" data-balloon-pos="up">View By Project</p> -->
        </div>
        <div class="flex flex-row m-2 justify-center">
            <div class="flex flex-col lg:flex-row">
                <p class="text-secondaryText mr-2 flex items-center justify-center w-32">Select User</p>
                <select value="username" class="nonAdminUserList text-primaryText cursor-pointer block appearance-none w-full lg:w-64 bg-secondary border border-gray-400 hover:border-gray-500 px-4 py-2 pr-8 rounded shadow leading-tight focus:outline-none focus:shadow-outline">
                </select>
            </div>
            <!-- <p id="viewUser" class="button-secondary ml-4 w-48 flex items-center justify-center" aria-label="Opens the User's Projects" data-balloon-pos="up">View By User</p> -->
        </div>
        <div class="flex items-center justify-center m-1">
            <p id="viewUserProject" class="button-secondary w-48 flex items-center justify-center" aria-label="Opens the User's Projects to the specified Project #" data-balloon-pos="up">View</p>
        </div>
    </div>
</div>
</body>

</html>