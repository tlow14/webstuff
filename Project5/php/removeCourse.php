<?php

session_start();
if(!isset($_SESSION['role'] ) || $_SESSION["role"] !== 'admin') {
    die("Not authorized");
}

require_once('dbconnect.php');
require_once('helpers.php');

removeCourse($mysqli, $_POST["course_id"]);

$mysqli->close();

function removeCourse($mysqli, $courseId) {



    setProjectOpen($mysqli, "null");

    //Gets all the project IDs
    $getAllProjects = "SELECT p.ID
                       FROM qwerty_pc_project AS p
                       INNER JOIN qwerty_pc_course AS c ON p.course_id = c.ID
                       WHERE c.ID=?";
    $projectList = array();
    if($projectIdStmt = mysqli_prepare($mysqli, $getAllProjects)){
        mysqli_stmt_bind_param($projectIdStmt, 'i', $courseId);
        mysqli_stmt_execute($projectIdStmt);
        mysqli_stmt_bind_result($projectIdStmt, $projectID);
        while(mysqli_stmt_fetch($projectIdStmt)){
            $projectList []= array("project_id" => $projectID); 
        }
        mysqli_stmt_close($projectIdStmt);
        
        //Goes through the project and deletes all the other things associated
        //With that project so it can delete all the projects
        foreach($projectList as $project){
            removeAllFromProject($mysqli, $project["project_id"]);           
        }
    }      

    //Deletes the project
    $deleteProjectSQL = "DELETE FROM qwerty_pc_project
                         WHERE course_id=?";
    if($deletePStmt = mysqli_prepare($mysqli, $deleteProjectSQL)){
        mysqli_stmt_bind_param($deletePStmt, 'i', $project_id);
        mysqli_stmt_execute($deletePStmt);
        mysqli_stmt_close($deletePStmt);
    }

    $preparedSQL ="delete from qwerty_pc_course where ID=?";

    if($stmt = mysqli_prepare($mysqli, $preparedSQL)){
        mysqli_stmt_bind_param($stmt, "i", $courseId);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_close($stmt);
    }
}

//This removes teams and it also removes the the projects
function removeAllFromProject($mysqli, $project_id){
    
    $getTeamIdsSQL = "SELECT t.ID
                      FROM qwerty_pc_team AS t
                      JOIN qwerty_pc_project_team AS pt ON t.ID = pt.team_id
                      WHERE pt.project_id=?";
    $teamList = array();
    if($teamIdStmt = mysqli_prepare($mysqli, $getTeamIdsSQL)){
        mysqli_stmt_bind_param($teamIdStmt, 'i', $project_id);
        mysqli_stmt_execute($teamIdStmt);
        mysqli_stmt_bind_result($teamIdStmt, $teamID);
        
        while(mysqli_stmt_fetch($teamIdStmt)){
            $teamList []= array("team_id" => $teamID); 
        }
        mysqli_stmt_close($teamIdStmt);      
    }            
    
    //Loops through all the teams and deletes those
    foreach($teamList as $team){
        $prepareDeletePT = "DELETE FROM qwerty_pc_project_team
                            WHERE project_id=?";
        if($deletePTStmt = mysqli_prepare($mysqli, $prepareDeletePT)){
            mysqli_stmt_bind_param($deletePTStmt, 'i', $project_id);
            mysqli_stmt_execute($deletePTStmt);
            mysqli_stmt_close($deletePTStmt);
        }
        echo mysqli_stmt_error();
        $prepareDeleteUT = "DELETE FROM qwerty_pc_user_team
                            WHERE team_id=?";
        if($deleteUTStmt = mysqli_prepare($mysqli, $prepareDeleteUT)){
            mysqli_stmt_bind_param($deleteUTStmt, 'i', $team["team_id"]);
            mysqli_stmt_execute($deleteUTStmt);
            mysqli_stmt_close($deleteUTStmt);
            
        }
        $prepareDeleteT = "DELETE FROM qwerty_pc_team
                        WHERE ID=?";
        if($deleteTStmt = mysqli_prepare($mysqli, $prepareDeleteT)){
            mysqli_stmt_bind_param($deleteTStmt, 'i', $team["team_id"]);
            mysqli_stmt_execute($deleteTStmt);
            mysqli_stmt_close($deleteTStmt);
        }
    }

    //Deletes writein for this project
    $deleteWriteinSQL = "DELETE FROM qwerty_pc_writein
                       WHERE project_id=?";
    if($deleteWStmt = mysqli_prepare($mysqli, $deleteWriteinSQL)){
        mysqli_stmt_bind_param($deleteWStmt, 'i', $project_id);
        mysqli_stmt_execute($deleteWStmt);
        mysqli_stmt_close($deleteWStmt);
    }                   
    


    //Deletes Vote for this project
    $deleteVotedSQL = "DELETE FROM qwerty_pc_voted
                       WHERE project_id=?";
    if($deleteVStmt = mysqli_prepare($mysqli, $deleteVotedSQL)){
        mysqli_stmt_bind_param($deleteVStmt, 'i', $project_id);
        mysqli_stmt_execute($deleteVStmt);
        mysqli_stmt_close($deleteVStmt);
    }                   
    

}

?>