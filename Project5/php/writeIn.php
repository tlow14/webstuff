<?php
    require_once('dbconnect.php');
    require_once('helpers.php');

    //Checks is 
    $currProject = isProjectOpen($mysqli);
    if ($currProject == "false") {
        echo "ERROR: no open project";
        return;
    }

    sendComment($mysqli,$_POST["comment_team"], $_POST["comment"], $currProject);

    function sendComment($mysqli, $toTeam, $text, $projectId){
        $preparedsql = "INSERT INTO qwerty_pc_writein (team_id, project_id, comment)
                        VALUES (?,?,?)";

        if($stmt = mysqli_prepare($mysqli, $preparedsql)){
            mysqli_stmt_bind_param($stmt, 'iis', $toTeam,  $projectId, $text);
            mysqli_stmt_execute($stmt);
            mysqli_stmt_close($stmt);
        }
    }
?>