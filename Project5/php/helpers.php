<?php
session_start();

function currentCourse($mysqli) {
    if(isset($_SESSION["course_id"])) {
       return $_SESSION["course_id"];
    } else {
        // This will happen for the anonymous user, it will use the most recent course
        $preparedSQL = "SELECT ID from qwerty_pc_course order by ID desc limit 1";
        if($stmt = mysqli_prepare($mysqli, $preparedSQL)){
            mysqli_stmt_execute($stmt);
            mysqli_stmt_bind_result($stmt, $id);
            while(mysqli_stmt_fetch($stmt)){
                $result = $id;
            }
            mysqli_stmt_close($stmt);
        }
        return $result; 
    }
}

function currentCourseInfo($mysqli) {
    if(isset($_SESSION["course_id"])) {
       $preparedSQL = "SELECT ID, name, open_project_id from qwerty_pc_course where ID=?";
        if($stmt = mysqli_prepare($mysqli, $preparedSQL)){

            $courseId = $_SESSION["course_id"];
            mysqli_stmt_bind_param($stmt, "i", $courseId);
            mysqli_stmt_execute($stmt);
            mysqli_stmt_bind_result($stmt, $id, $name, $openProjId);

            $result = array();
            while(mysqli_stmt_fetch($stmt)){
                $result []= array("ID" => $id, "name" => $name, "open_project_id" => $openProjId); 
            }
            mysqli_stmt_close($stmt);
        }
        return $result; 
    }
}

function getTeamsById($mysqli, $project_id, $course_id){
    $preparedsql = "SELECT u.full_name, t.name, u.username, p.project_id, p.team_id
            FROM qwerty_pc_project_team AS p
            INNER JOIN qwerty_pc_user_team AS ut ON p.team_id = ut.team_id
            INNER JOIN qwerty_pc_user AS u ON u.username = ut.user_id
            INNER JOIn qwerty_pc_team AS t ON t.ID = ut.team_id
            INNER JOIN qwerty_pc_project AS pr ON pr.ID = p.project_id
            WHERE pr.id=? AND pr.course_id=?";
    //print $projectId;
    if($stmt = mysqli_prepare($mysqli, $preparedsql)){
        mysqli_stmt_bind_param($stmt, "ii", $project_id, $course_id);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_bind_result($stmt, $full_name, $team_name, $username, $proID, $teamId);  
        $result = array();
        while(mysqli_stmt_fetch($stmt)){
            $project = array("full_name" => $full_name, "team_name" => $team_name, "username" => $username, "team_id" => $teamId); 
            $result []= $project;
        }
        mysqli_stmt_close($stmt);
    }
    return $result;
}


function getTeamsByName($mysqli, $projectName, $course_id){
    $preparedsql = "SELECT u.full_name, t.name, u.username, p.project_id
            FROM qwerty_pc_project_team AS p
            INNER JOIN qwerty_pc_user_team AS ut ON p.team_id = ut.team_id
            INNER JOIN qwerty_pc_user AS u ON u.username = ut.user_id
            INNER JOIn qwerty_pc_team AS t ON t.ID = ut.team_id
            INNER JOIN qwerty_pc_project AS pr ON pr.ID = p.project_id
            WHERE pr.name=? AND pr.course_id=?";


    //print $projectId;
    if($stmt = mysqli_prepare($mysqli, $preparedsql)){
        mysqli_stmt_bind_param($stmt, "si", $projectName, $course_id);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_bind_result($stmt, $full_name, $team_name, $username, $proID);  
        $result = array();
        while(mysqli_stmt_fetch($stmt)){
            $project = array("full_name" => $full_name, "team_name" => $team_name, "username" => $username, "project_id"=> $proID); 
            $result []= $project;
        }
        mysqli_stmt_close($stmt);
    }
    return $result;
}

// function getTeamIdsForProject($mysqli, $projectId) {
//     $preparedsql = "SELECT DISTINCT ut.team_id
//     FROM qwerty_pc_project_team AS p
//     INNER JOIN qwerty_pc_user_team AS ut ON p.team_id = ut.team_id
//     INNER JOIN qwerty_pc_user AS u ON u.username = ut.user_id
//     INNER JOIN qwerty_pc_team AS t ON t.ID = ut.team_id
//     INNER JOIN qwerty_pc_project AS pr ON pr.ID = p.project_id
//     WHERE pr.ID =?
//     AND pr.course_id =?";

//     if (!$projectId || $projectId == "null") {
//         $projectId = isProjectOpen($mysqli);
//     }

//     $courseId = currentCourse($mysqli);

//     if($stmt = mysqli_prepare($mysqli, $preparedsql)){
//     mysqli_stmt_bind_param($stmt, "ii", $projectId, $courseId);
//     mysqli_stmt_execute($stmt);
//     mysqli_stmt_bind_result($stmt, $teamId);  
//     $result = array();
//     while(mysqli_stmt_fetch($stmt)){
//         $project = array("team_id" => $teamId);
//         $result []= $project;
//     }
//     mysqli_stmt_close($stmt);
//     }
//     return $result; 
// }

function getComments($mysqli, $projectId) {
    $preparedsql = "SELECT u.full_name, ut.team_id, u.username, w.comment, u.iconUrl, w.project_id
        FROM qwerty_pc_project_team AS p
        INNER JOIN qwerty_pc_user_team AS ut ON p.team_id = ut.team_id
        INNER JOIN qwerty_pc_user AS u ON u.username = ut.user_id
        INNER JOIN qwerty_pc_writein AS w ON w.team_id = ut.team_id
        WHERE w.project_id=?
        ORDER BY ut.team_id";

    if (!$projectId || $projectId == "null") {
        $projectId = isProjectOpen($mysqli);
    }

    if($stmt = mysqli_prepare($mysqli, $preparedsql)){
        mysqli_stmt_bind_param($stmt, "i", $projectId);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_bind_result($stmt, $full_name, $team_id, $username, $comment, $iconUrl, $projectId);  
        $result = array();
        while(mysqli_stmt_fetch($stmt)){
            $result []= array("full_name" => $full_name, "team_id" => $team_id, "username" => $username, "comment" => $comment, "iconUrl" => $iconUrl, "project_id" => $projectId); 
        }
        mysqli_stmt_close($stmt);
    }
    return $result;
}

// function getCommentsForTeam($mysqli, $projectId, $teamId) {
//     $preparedsql = "SELECT u.full_name, ut.team_id, u.username, w.comment, u.iconUrl
//     FROM qwerty_pc_project_team AS p
//     INNER JOIN qwerty_pc_user_team AS ut ON p.team_id = ut.team_id
//     INNER JOIN qwerty_pc_user AS u ON u.username = ut.user_id
//     INNER JOIN qwerty_pc_writein AS w ON w.team_id = ut.team_id
//     WHERE w.project_id=? AND ut.team_id=?;";

//     if (!$projectId || $projectId == "null") {
//         $projectId = isProjectOpen($mysqli);
//     }

//     if($stmt = mysqli_prepare($mysqli, $preparedsql)){
//         mysqli_stmt_bind_param($stmt, "ii", $projectId, $teamId);
//         mysqli_stmt_execute($stmt);
//         mysqli_stmt_bind_result($stmt, $full_name, $team_id, $username, $comment, $iconUrl);  
//         $result = array();
//         while(mysqli_stmt_fetch($stmt)){
//             $result []= array("full_name" => $full_name, "team_id" => $team_id, "username" => $username, "comment" => $comment, "iconUrl" => $iconUrl); 
//         }
//         mysqli_stmt_close($stmt);
//     }
//     return $result;
// }

function getUsersComments($mysqli, $projectId, $user) {
    $preparedsql = "SELECT u.full_name, ut.team_id, u.username, w.comment, u.iconUrl
        FROM qwerty_pc_project_team AS p
        INNER JOIN qwerty_pc_user_team AS ut ON p.team_id = ut.team_id
        INNER JOIN qwerty_pc_user AS u ON u.username = ut.user_id
        INNER JOIN qwerty_pc_writein AS w ON w.team_id = ut.team_id
        WHERE w.project_id=? AND u.username=?";

    if (!$projectId || $projectId == "null") {
        $projectId = isProjectOpen($mysqli);
    }

    if($stmt = mysqli_prepare($mysqli, $preparedsql)){
        mysqli_stmt_bind_param($stmt, "is", $projectId, $user);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_bind_result($stmt, $full_name, $team_id, $username, $comment, $iconUrl);  
        $result = array();
        while(mysqli_stmt_fetch($stmt)){
            $result []= array("full_name" => $full_name, "team_id" => $team_id, "username" => $username, "comment" => $comment, "iconUrl" => $iconUrl); 
        }
        mysqli_stmt_close($stmt);
    }
    return $result;
}



/* 
    Gets all users
*/
function getUsers($mysqli) {
    $preparedsql = "select username, full_name, iconUrl from qwerty_pc_user where course_id=?";

    if($stmt = mysqli_prepare($mysqli, $preparedsql)){

        $courseid = currentCourse($mysqli);
        mysqli_stmt_bind_param($stmt, "i", $courseid);

        mysqli_stmt_execute($stmt);
        mysqli_stmt_bind_result($stmt, $username, $full_name, $iconUrl);  

        $result = array();
        while(mysqli_stmt_fetch($stmt)){
            $result []= array("username" => $username, "full_name" => $full_name, "iconUrl" => $iconUrl); 
        }
        mysqli_stmt_close($stmt);
    }
    return $result;
}

/*
    Gets all users that are not the current user
*/
function getOtherUsers($mysqli) {
    $preparedsql = "select username, full_name, iconUrl from qwerty_pc_user where course_id=? and username != ?";

    if($stmt = mysqli_prepare($mysqli, $preparedsql)){

        $courseid = currentCourse($mysqli);
        $username = $_SESSION['username'];
        mysqli_stmt_bind_param($stmt, "is", $courseid, $username);

        mysqli_stmt_execute($stmt);
        mysqli_stmt_bind_result($stmt, $username, $full_name, $iconUrl);  

        $result = array();
        while(mysqli_stmt_fetch($stmt)){
            $result []= array("username" => $username, "full_name" => $full_name, "iconUrl" => $iconUrl); 
        }
        mysqli_stmt_close($stmt);
    }
    return $result;
}

/*
    Gets all users that have the role 'user' aka. non admins
*/
function getNonAdminUsers($mysqli) {
    $preparedsql = "SELECT username, full_name, iconUrl, subtitle from qwerty_pc_user where course_id=? AND role='user'";

    if($stmt = mysqli_prepare($mysqli, $preparedsql)){

        $courseid = currentCourse($mysqli);
        mysqli_stmt_bind_param($stmt, "i", $courseid);

        mysqli_stmt_execute($stmt);
        mysqli_stmt_bind_result($stmt, $linux_username, $full_name, $iconUrl, $subtitle);  
        while(mysqli_stmt_fetch($stmt)){
            $result []= ["username" => $linux_username, 
                        "full_name" => $full_name, 
                        "iconUrl" => $iconUrl, 
                        "subtitle" => $subtitle]; 
        }
        mysqli_stmt_close($stmt);
    }
    return $result;
}

// function getUserInfo($mysqli) {
//     $preparedSQL ="select full_name, role, course_id, subtitle, iconUrl from qwerty_pc_user where username=?";

//     if($stmt = mysqli_prepare($mysqli, $preparedSQL)){
//         // echo $_SESSION["username"];
//         mysqli_stmt_bind_param($stmt, "s", $_SESSION["username"]);
//         mysqli_stmt_execute($stmt);
//         mysqli_stmt_bind_result($stmt, $full_name, $role, $course_id, $subtitle, $iconUrl);
//         $result = array();
//         if(mysqli_stmt_fetch($stmt)) {
//             $result []= array("full_name" => $full_name, "role" => $role, "course_id" => $course_id, "subtitle" => $subtitle, "iconUrl" => $iconUrl);
//         }
//         mysqli_stmt_close($stmt);
//     }

//     // return $result;
//     return $_SESSION["username"];
// }




function isProjectOpen($mysqli) {
    $preparedSQL ="SELECT open_project_id 
                    FROM qwerty_pc_course 
                    WHERE ID=?";
    if($stmt = mysqli_prepare($mysqli, $preparedSQL)){
        mysqli_stmt_bind_param($stmt, "i", currentCourse($mysqli));
        mysqli_stmt_execute($stmt);
        mysqli_stmt_bind_result($stmt, $projectId);
        if(mysqli_stmt_fetch($stmt)) {
            $result = $projectId;
        }
        mysqli_stmt_close($stmt);
    }

    if($result != "") {
        return $result;
    } else {
        return "false";
    } 
}

function setProjectOpen($mysqli, $project_id) {
    $preparedSQL ="update qwerty_pc_course set open_project_id=? where ID=?";

    if($stmt = mysqli_prepare($mysqli, $preparedSQL)){

        if($project_id == "null") {
            $project_id = null;
        }
        mysqli_stmt_bind_param($stmt, "ii", $project_id, currentCourse($mysqli));
        mysqli_stmt_execute($stmt);
        mysqli_stmt_close($stmt);
    }
}

function getProjects($mysqli) {
    $preparedSQL ="select ID, name from qwerty_pc_project where course_id = ? order by ID";
    if($stmt = mysqli_prepare($mysqli, $preparedSQL)){
        mysqli_stmt_bind_param($stmt, "i", currentCourse($mysqli));
        mysqli_stmt_execute($stmt);
        mysqli_stmt_bind_result($stmt, $ID, $name);
        $result = array();
        while(mysqli_stmt_fetch($stmt)){
            $project = array("name" => $name, "id" => $ID);
            $result []= $project;
        }
        mysqli_stmt_close($stmt);
    }
    return $result; 
}

function numberOfProjects($mysqli)
{
    return count(getProjects($mysqli));
}

function userHasVoted ($mysqli, $userID, $projectID)
{
    $preparedSQL = "select user_id
                    from qwerty_pc_voted
                    where user_id = ? and project_id = ?";
    if ($stmt = mysqli_prepare($mysqli, $preparedSQL)) {
        mysqli_stmt_bind_param($stmt, "si", $userID, $projectID);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_bind_result($stmt, $ID);
        $result = array();
        while(mysqli_stmt_fetch($stmt)){
            $result []= $ID;
        }
        mysqli_stmt_close($stmt);
    }
    return (count($result) > 0);
}

?>