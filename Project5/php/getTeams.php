<?php
    require_once('dbconnect.php');
    require_once('helpers.php');

    $json = json_encode(getTeamsByName($mysqli, $_POST["project_name"], currentCourse($mysqli)));
    echo $json;
?>