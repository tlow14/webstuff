<?php

require_once('dbconnect.php');
require_once('helpers.php');

//Print array in JSON format
echo json_encode(getUsersFromCourse($mysqli, currentCourse($mysqli)));
$mysqli->close();

function getUsersFromCourse($mysqli, $courseId) {
    $preparedsql = "select username, full_name, iconUrl from qwerty_pc_user where course_id = ? OR role='admin'";

    if($stmt = mysqli_prepare($mysqli, $preparedsql)){

        mysqli_stmt_bind_param($stmt, "i", $courseId);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_bind_result($stmt, $username, $full_name, $iconUrl);  

        $result = array();
        while(mysqli_stmt_fetch($stmt)){
            $result []= array("username" => $username, "full_name" => $full_name, "iconUrl" => $iconUrl); 
        }
        mysqli_stmt_close($stmt);
    }
    return $result;
}

?>