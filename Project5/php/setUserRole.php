<?php

session_start();
if(!isset($_SESSION['role'] ) || $_SESSION["role"] !== 'admin') {
    die("Not authorized");
}

require_once('dbconnect.php');
require_once('helpers.php');

setUserRole($mysqli, $_POST["role"], $_POST["username"]);

$mysqli->close();

function setUserRole($mysqli, $role, $username) {
    $sql = "UPDATE qwerty_pc_user set role=? where username=?";
            if($stmt = mysqli_prepare($mysqli, $sql)){

                // Bind variables to the prepared statement as parameters
                mysqli_stmt_bind_param($stmt, "ss", $role, $username);
                
                mysqli_stmt_execute($stmt);
                // // // Attempt to execute the prepared statement
                // if(mysqli_stmt_execute($stmt)){
                //     // Redirect to login page
                //     echo '<div class="bg-red-400 z-10 border border-red-400 text-white px-4 py-3 rounded relative my-0 flex flex-row justify-between"><div><p class="font-bold">Notice!</p><p class="text-sm">User ' . $username . ' is now a ' . $role . '</p></div><div><i class="fa fa-times-circle text-lg cursor-pointer m-4" aria-hidden="true" onclick="this.parentElement.parentElement.style.display="none";"></i></div></div>';
                // // } else{
                // //     echo "Something went wrong. Please try again later.";
                // }
    
                // Close statement
                mysqli_stmt_close($stmt);
            }
}

?>
