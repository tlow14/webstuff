<?php

session_start();
if(!isset($_SESSION['role'] ) || $_SESSION["role"] != 'admin') {
    die("Not authorized");
}

require_once('dbconnect.php');
require_once('helpers.php');

resetPassword($mysqli, $_POST["username"]);

$mysqli->close();

function resetPassword($mysqli, $username) {
    $preparedSQL = "UPDATE qwerty_pc_user set password=? where username=?";

    // This is "password" in hash form.
    // $newPassword = '$2y$12$UQqT1FKBbh0juutCCQhat.vmQhBCQBJhtX6gzl/X.Tk8N7GVhDmzC';

    if($stmt = mysqli_prepare($mysqli, $preparedSQL)){
        // Bind variables to the prepared statement as parameters
        mysqli_stmt_bind_param($stmt, "ss", $param_password, $username);
                
        // Set parameters
        // $param_username = $username;
        $options = [
            'cost' => 12,
        ];
        $param_password = password_hash("password", PASSWORD_BCRYPT, $options); // Creates a password hash
        mysqli_stmt_execute($stmt);
        mysqli_stmt_close($stmt);
    }
}

?>