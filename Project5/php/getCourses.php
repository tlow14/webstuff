<?php
require_once('dbconnect.php');
require_once('helpers.php');

//Print array in JSON format
echo json_encode(getCourses($mysqli));
$mysqli->close();

function getCourses($mysqli) {
    $preparedSQL = "select ID, name, open_project_id from qwerty_pc_course order by ID desc"; 
    $stmt = mysqli_prepare($mysqli, $preparedSQL);
    mysqli_stmt_execute($stmt);
    mysqli_stmt_bind_result($stmt, $ID, $name, $openProject);
    $result = array();
    while(mysqli_stmt_fetch($stmt)){
        $result []= array("ID" => $ID, "name" => $name, "open_project_id" => $openProject); 
    }
    return $result;
}

?>
