<?php

require_once('dbconnect.php');
require_once('helpers.php');

if(!isset($_SESSION['username'] ) || $_SESSION["loggedin"] !== true) {
    // echo "Not logged in!";
    die("User is not logged in!");
}


$currProject = isProjectOpen($mysqli);
if ($currProject == "false") {
    // echo "Project Not Open!";
    // return;
    die("Project not open!");
}

if(userHasVoted($mysqli, $_SESSION["username"], $currProject)) {
    echo "true";
} else {
    echo "false";
}

?>