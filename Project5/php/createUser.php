<?php

session_start();
if(!isset($_SESSION['role'] ) || $_SESSION["role"] != 'admin') {
    die("Not authorized");
}

require_once('dbconnect.php');
require_once('helpers.php');

createUser($mysqli, $_POST["username"], $_POST["full_name"]);

$mysqli->close();

function createUser($mysqli, $username, $full_name) {
    $userExistSQL = "SELECT COUNT(1), course_id 
                     FROM qwerty_pc_user 
                     WHERE username=?";
    
    if($userExistStmt = mysqli_prepare($mysqli, $userExistSQL)){
        mysqli_stmt_bind_param($userExistStmt, 's', $username);
        mysqli_stmt_execute($userExistStmt);
        mysqli_stmt_bind_result($userExistStmt, $exists, $course_id);
        mysqli_stmt_fetch($userExistStmt);
        mysqli_stmt_close($userExistStmt);
        if($exists > 0){
            if($course_id == currentCourse($mysqli)){
                echo "duplicate";
            }else{
                echo "update";
                $prepareUpdateSql = "UPDATE qwerty_pc_user
                                    SET course_id=?, full_name=?
                                    WHERE username=?";
                if($updateStmt = mysqli_prepare($mysqli, $prepareUpdateSql)){
                    mysqli_stmt_bind_param($updateStmt, 'iss', currentCourse($mysqli), $full_name, $username);
                    mysqli_stmt_execute($updateStmt);
                    mysqli_stmt_close($updateStmt);
                }
            }

        }else{
            echo "new";
            $preparedSQL = "INSERT into qwerty_pc_user(username, password, full_name, role, course_id, iconUrl) values(?, ?, ?, ?, ?, ?)";
        
            if($stmt = mysqli_prepare($mysqli, $preparedSQL)){
        
                $iconUrl = 'images/noUserIcon.png';
                $role = 'user';
                // Bind variables to the prepared statement as parameters
                mysqli_stmt_bind_param($stmt, "ssssis", $username, $param_password, $full_name, $role, currentCourse($mysqli), $iconUrl);
        
                $options = [
                    'cost' => 12,
                ];
                $param_password = password_hash("password", PASSWORD_BCRYPT, $options); // Creates a password hash
                mysqli_stmt_execute($stmt);
                mysqli_stmt_close($stmt);
            }
        }
    }


    
}

?>