<?php
session_start();
$link = mysqli_connect('james.cedarville.edu', 'cs3220', '', 'cs3220_Sp20') or die('Database connect error.');

updateURL($link, $_POST["url"], $_POST['project_id'], $_SESSION["username"]);
$link->close();

//Allows the updating of the url in qwerty_pc_projec_team
function updateURL($link, $url, $project_id, $username){
    $prepareUrlSql = "UPDATE qwerty_pc_project_team AS pt
                      INNER JOIN qwerty_pc_user_team AS ut ON ut.team_id = pt.team_id
                      SET pt.url=?
                      WHERE pt.project_id=? AND ut.user_id=?";
    if($stmt = mysqli_prepare($link, $prepareUrlSql)){
        var_dump($url, $project_id, $username);
        mysqli_stmt_bind_param($stmt, 'sis', $url, $project_id, $username);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_close($stmt);
    }
}
?>