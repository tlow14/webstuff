<?php

require_once('dbconnect.php');
require_once('helpers.php');
$project= $_POST['projectName'];

submitTeams($mysqli,$_POST['jsArray'], $project, currentCourse($mysqli));


function submitTeams($mysqli, $jsArray, $project_name, $course_id){
    $project_id = -1;
    foreach($jsArray as $item){
        
        
        $user_id = $item['name'];
        $team_name = $item['team'];
        $team_id =-1;
        //This part checks if there is a team already created for this project and courseID
        $preparedTeamsql = "SELECT COUNT(1), t.ID
                            FROM qwerty_pc_user_team AS ut
                            JOIN qwerty_pc_team AS t ON t.ID = ut.team_id
                            JOIN qwerty_pc_project_team AS pt ON pt.team_id = ut.team_id
                            JOIN qwerty_pc_project AS p  ON p.ID = pt.project_id
                            WHERE t.name=? AND p.name=? AND p.course_id=?;";

        if($stmtTeam = mysqli_prepare($mysqli, $preparedTeamsql)){
            mysqli_stmt_bind_param($stmtTeam, 'ssi',  $team_name, $project_name, $course_id);
            mysqli_stmt_execute($stmtTeam);
            mysqli_stmt_bind_result($stmtTeam, $teamCheck, $tempId);
            mysqli_stmt_fetch($stmtTeam);
                
        
        //Creates a new team if none exist
            if($teamCheck == 0){
                mysqli_stmt_close($stmtTeam);
                $teamInsert = "INSERT INTO qwerty_pc_team (name) VALUES(?);";
                        if($insertStatementUser1 = mysqli_prepare($mysqli, $teamInsert)){
                            mysqli_stmt_bind_param($insertStatementUser1, 's', $team_name);
                            mysqli_stmt_execute($insertStatementUser1);
                            mysqli_stmt_close($insertStatementUser1);
                        }
                        $teamIdSql = "SELECT MAX(t.ID)
                                    FROM qwerty_pc_team AS t
                                    WHERE t.name=?";
                        if($teamIdStmt = mysqli_prepare($mysqli, $teamIdSql)){
                            mysqli_stmt_bind_param($teamIdStmt, 's', $team_name);
                            mysqli_stmt_execute($teamIdStmt);
                            mysqli_stmt_bind_result($teamIdStmt, $tID);
                            mysqli_stmt_fetch($teamIdStmt);
                            $team_id = $tID;
                            mysqli_stmt_close($teamIdStmt);
                        }
            }else{
                $team_id = $tempId;
                mysqli_stmt_close($stmtTeam);
            }
        }
        
        
        $preparedsql = "SELECT COUNT(1)
                        FROM qwerty_pc_user_team AS ut
                        JOIN qwerty_pc_team AS t ON ut.team_id = t.ID
                        JOIN qwerty_pc_project_team AS pt ON pt.team_id = ut.team_id
                        JOIN qwerty_pc_project AS p  ON p.ID = pt.project_id
                        WHERE ut.user_id=? AND p.name=? AND p.course_id=?" ;
        if($stmt = mysqli_prepare($mysqli, $preparedsql)){
            mysqli_stmt_bind_param($stmt, 'ssi', $user_id, $project_name,$course_id);
            mysqli_stmt_execute($stmt);
            mysqli_stmt_bind_result($stmt, $userCheck);
            mysqli_stmt_fetch($stmt);
            
            
            //If the user DOES NOT already in the team for this project and course
            if($userCheck == 0){
                mysqli_stmt_close($stmt);
                $blah = "INSERT INTO qwerty_pc_user_team (user_id, team_id) VALUES (?,?)";
                $insertStatementUser = mysqli_prepare($mysqli, $blah);
                if($insertStatementUser){
                    mysqli_stmt_bind_param($insertStatementUser, 'si', $user_id, $team_id);
                    mysqli_stmt_execute($insertStatementUser);
                    mysqli_stmt_close($insertStatementUser);
                }
            }else{
                mysqli_stmt_close($stmt);
                var_dump($team_id, $user_id);
                
                
                //Updates the team for a user
                $updateUserTeamSql = "UPDATE qwerty_pc_user_team AS ut
                                      INNER JOIN qwerty_pc_project_team AS pt ON ut.team_id = pt.team_id
                                      INNER JOIN qwerty_pc_project AS p ON pt.project_id = p.ID
                                      SET ut.team_id=?
                                      WHERE ut.user_id=? AND p.name=? AND p.course_id=?";
                if($updateUserTeamStatement = mysqli_prepare($mysqli, $updateUserTeamSql)){
                    mysqli_stmt_bind_param($updateUserTeamStatement, 'issi', $team_id,  $user_id, $project_name, $course_id);
                    mysqli_stmt_execute($updateUserTeamStatement);
                    mysqli_stmt_close($updateUserTeamStatement);
                }          
                
            }
            
            
            
            $preparedsql2 = "SELECT COUNT(1), p.ID
                    FROM qwerty_pc_user_team AS ut
                    JOIN qwerty_pc_project_team AS pt ON ut.team_id = pt.team_id
                    JOIN qwerty_pc_project AS p ON pt.project_id = p.ID
                    WHERE ut.user_id=? AND p.name=? AND p.course_id=?";
            if($stmt2 = mysqli_prepare($mysqli, $preparedsql2)){
                mysqli_stmt_bind_param($stmt2, 'ssi', $user_id, $project_name, $course_id);
                mysqli_stmt_execute($stmt2);
                mysqli_stmt_bind_result($stmt2, $exists, $projId);
                mysqli_stmt_fetch($stmt2);
                
                $project_id = $projId;
                //Checks if the user is already in another team
                if($exists > 0){
                    
                    mysqli_stmt_close($stmt2);
                    $preparedUpdate = "UPDATE qwerty_pc_project_team AS pt
                                        INNER JOIN qwerty_pc_user_team AS ut on ut.team_id = pt.team_id
                                        JOIN qwerty_pc_project AS p ON pt.project_id = p.ID
                                        SET pt.team_id=?
                                        WHERE p.name=? AND ut.user_id=? AND p.course_id=?";
                    if($updateStatement = mysqli_prepare($mysqli, $preparedUpdate)){
                        mysqli_stmt_bind_param($updateStatement, 'issi', $team_id, $project_name, $user_id, $course_id);
                        mysqli_stmt_execute($updateStatement);
                        mysqli_stmt_close($updateStatement);
                    }
                    
                    
                }else{
                    mysqli_stmt_close($stmt2);
                    //Get Project_id
                    $projectSQL = "SELECT p.id
                                   FROM qwerty_pc_project AS p
                                   WHERE p.name=? AND p.course_id=?";
                    if($stmtProject = mysqli_prepare($mysqli, $projectSQL)){
                        mysqli_stmt_bind_param($stmtProject, 'si', $project_name, $course_id);
                        mysqli_stmt_execute($stmtProject);
                        mysqli_stmt_bind_result($stmtProject, $pId);
                        mysqli_stmt_fetch($stmtProject);
                        $project_id = $pId;
                        mysqli_stmt_close($stmtProject);
                    }
                    $projectNum = explode(' ', trim($project_name));

                    $url = "/~" . $user_id . "/TermProject/Project" . $projectNum[1] . "/";
                    $preparedInsert = "INSERT INTO qwerty_pc_project_team (project_id, team_id, gold, silver, bronze, url)
                                        VALUES (?,?,?,?,?,?)";
                    if($insertStatement = mysqli_prepare($mysqli, $preparedInsert)){
                        $zero = 0;
                        mysqli_stmt_bind_param($insertStatement, 'iiiiis', $project_id, $team_id,$zero, $zero, $zero, $url);
                        mysqli_stmt_execute($insertStatement);
                        mysqli_stmt_close($insertStatement);
                    }
                }
                
            }
            
            
        }
    
    }
    

    //DELETES ALL EMPTY TEAMS
    //LOOK  AT THAT SQL !!!!
    $sqlDelete1 = "DELETE FROM qwerty_pc_project_team 
                            WHERE team_id 
                            IN(
                                SELECT tid FROM(
                                    SELECT t.id as tid
                                    FROM qwerty_pc_team as t
                                    WHERE t.id NOT IN (
                                        SELECT ut.team_id 
                                        FROM  qwerty_pc_user_team AS ut
                                        INNER JOIN qwerty_pc_project_team AS pt ON ut.team_id = pt.team_id
                                        INNER JOIN qwerty_pc_team AS t ON t.id = ut.team_id
                                        ) 
                                ) as c
                            )";
    if(mysqli_query($mysqli, $sqlDelete1)){}
    
    $sqlDeleteWritin = "DELETE FROM qwerty_pc_writein
                        WHERE team_id
                        IN(
                            SELECT tid FROM(
                                SELECT t.id as tid
                                FROM qwerty_pc_team as t
                                WHERE t.id NOT IN (
                                    SELECT ut.team_id 
                                    FROM  qwerty_pc_user_team AS ut
                                    INNER JOIN qwerty_pc_team AS t ON t.id = ut.team_id
                                    ) 
                            ) as c
                        )";
    if(mysqli_query($mysqli, $sqlDeleteWritin)){}                    
    
    $sqlDelete2 = "DELETE FROM qwerty_pc_team 
                        WHERE id
                        IN(
                            SELECT tid FROM(
                                SELECT t.id as tid
                                FROM qwerty_pc_team as t
                                WHERE t.id NOT IN (
                                    SELECT ut.team_id 
                                    FROM  qwerty_pc_user_team AS ut
                                    INNER JOIN qwerty_pc_team AS t ON t.id = ut.team_id
                                    ) 
                            ) as c
                        )";
    if(mysqli_query($mysqli, $sqlDelete2)){}
    
    //Fixes empty teams
    $preparedTeams = "SELECT t.name, t.ID
                      FROM qwerty_pc_team AS t
                      INNER JOIN qwerty_pc_project_team AS pt ON pt.team_id = t.id
                      INNER JOIN qwerty_pc_project AS p ON pt.project_id = p.ID
                      WHERE p.name=? AND p.course_id=?";
    if($teamsStmt = mysqli_prepare($mysqli, $preparedTeams)){
        mysqli_stmt_bind_param($teamsStmt, 'si', $project_name, $course_id);
        mysqli_stmt_execute($teamsStmt);
        mysqli_stmt_bind_result($teamsStmt, $teams, $teamsID);
        
        $result = array();
        while(mysqli_stmt_fetch($teamsStmt)){
            $result []= array("teamName" => $teams, "team_id" => $teamsID); 
        }
        mysqli_stmt_close($teamsStmt);

        foreach($result as $teamList){
            $team_id = $teamList["team_id"];
            $isInJS = false;
            foreach($jsArray as $item){
                if($item["team"] == $teamList["teamName"]){
                    $isInJS = true;
                }
            }
            //Deletes each team that is empty in the JS
            if(!$isInJS){
                $prepareDeletePT = "DELETE FROM qwerty_pc_project_team
                                    WHERE team_id=?";
                if($deletePTStmt = mysqli_prepare($mysqli, $prepareDeletePT)){
                    mysqli_stmt_bind_param($deletePTStmt, 'i', $team_id);
                    mysqli_stmt_execute($deletePTStmt);
                    mysqli_stmt_close($deletePTStmt);
                }

                $prepareDeleteUT = "DELETE FROM qwerty_pc_user_team
                                    WHERE team_id=?";
                if($deleteUTStmt = mysqli_prepare($mysqli, $prepareDeleteUT)){
                    mysqli_stmt_bind_param($deleteUTStmt, 'i', $team_id);
                    mysqli_stmt_execute($deleteUTStmt);
                    mysqli_stmt_close($deleteUTStmt);

                }

                $prepareDeleteW = "DELETE FROM qwerty_pc_writein
                                    WHERE team_id=?";
                if($deleteWStmt = mysqli_prepare($mysqli, $prepareDeleteW)){
                    mysqli_stmt_bind_param($deleteWStmt, 'i', $team_id);
                    mysqli_stmt_execute($deleteWStmt);
                    mysqli_stmt_close($deleteWStmt);
                }

                $prepareDeleteT = "DELETE FROM qwerty_pc_team
                                    WHERE ID=?";
                if($deleteTStmt = mysqli_prepare($mysqli, $prepareDeleteT)){
                    mysqli_stmt_bind_param($deleteTStmt, 'i', $team_id);
                    mysqli_stmt_execute($deleteTStmt);
                    mysqli_stmt_close($deleteTStmt);

                }
            }

        }
    }

    //Clears out useless people who are no longer on team in voted
    $preparedVotedDelete = "DELETE FROM qwerty_pc_voted WHERE user_id IN(
                                SELECT uid from (
                                    SELECT user_id as uid
                                    FROM qwerty_pc_voted
                                    WHERE user_id NOT IN(
                                        SELECT ut.user_id
                                        FROM qwerty_pc_user_team AS ut
                                        INNER JOIN qwerty_pc_project_team AS pt ON ut.team_id = pt.team_id
                                        WHERE pt.project_id=?
                                    )
                                ) as c
                            )AND project_id=?";
    if($votedDeleteStmt = mysqli_prepare($mysqli, $preparedVotedDelete)){
        mysqli_stmt_bind_param($votedDeleteStmt, 'ii', $project_id, $project_id);
        mysqli_stmt_execute($votedDeleteStmt);
        mysqli_stmt_close($votedDeleteStmt);

    }                        

}
?>