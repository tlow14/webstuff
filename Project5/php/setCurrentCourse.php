<?php

require_once('dbconnect.php');
require_once('helpers.php');

// If the user is not logged in!
if(isset($_SESSION['username'] ) && $_SESSION["loggedin"] === true) {
    die("Can't change course when logged in...");
}

$_SESSION["course_id"] = $_POST["course_id"];

?>
