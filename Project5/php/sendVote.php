<?php

require_once('dbconnect.php');
require_once('helpers.php');

// check that project is open
$currProject = isProjectOpen($mysqli);
if ($currProject == "false") {
    echo "ERROR: no open project";
    return;
}

// check that user has not voted
if (userHasVoted($mysqli, $_SESSION["username"], $currProject)) {
    echo "ERROR: user already voted";
    return;
}

// check that user isn't voting for him/herself
if (selfVote($mysqli, $_SESSION["username"], $_POST["goldID"], $_POST["silverID"], $_POST["bronzeID"])) {
    echo "ERROR: team can't vote for itself";
    return;
}

// check that a team exists for all 3 voted
if (!teamExists($mysqli, $_POST["goldID"])) {
    echo "ERROR: team with ID " . $_POST["goldID"] . " does not exist in database";
    return;
}
if (!teamExists($mysqli, $_POST["silverID"])) {
    echo "ERROR: team with ID " . $_POST["silverID"] . " does not exist in database";
    return;
}
if (!teamExists($mysqli, $_POST["bronzeID"])) {
    echo "ERROR: team with ID " . $_POST["bronzeID"] . " does not exist in database";
    return;
}

// send gold, silver, and bronze votes
sendGoldVote($mysqli, $currProject, $_POST["goldID"]);
sendSilverVote($mysqli, $currProject, $_POST["silverID"]);
sendBronzeVote($mysqli, $currProject, $_POST["bronzeID"]);

// add vote so user can't vote again
updateVoted($mysqli, $_SESSION["username"], $currProject);
//updateVoted($mysqli, $_POST["voterID"], $currProject);

function teamExists($mysqli, $team)
{
    $preparedSQL = "select ID
                    from qwerty_pc_team
                    where ID = ?";
    if ($stmt = mysqli_prepare($mysqli, $preparedSQL)) {
        mysqli_stmt_bind_param($stmt, "i", $team);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_bind_result($stmt, $ID);
        $result = array();
        while(mysqli_stmt_fetch($stmt)){
            $result []= $ID;
        }
        mysqli_stmt_close($stmt);
    }
    return (count($result) == 1);
}

function selfVote($mysqli, $username, $gold, $silver, $bronze)
{
    $preparedSQL = "select team_id
                    from qwerty_pc_user_team
                    where user_id = ?";
    if ($stmt = mysqli_prepare($mysqli, $preparedSQL)) {
        mysqli_stmt_bind_param($stmt, "s", $username);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_bind_result($stmt, $teamID);
        while(mysqli_stmt_fetch($stmt)){
            if ($gold == $teamID) { return true; }
            if ($silver == $teamID) { return true; }
            if ($bronze == $teamID) { return true; }
        }
        mysqli_stmt_close($stmt);
    }
    return false;
}

function sendGoldVote($mysqli, $project, $team)
{
    $preparedSQL = "update qwerty_pc_project_team
                    set gold = gold + 1
                    where project_id = ? and team_id = ?";
    if ($stmt = mysqli_prepare($mysqli, $preparedSQL)) {
        mysqli_stmt_bind_param($stmt, "ii", $project, $team);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_close($stmt);
    } else {
        echo "gold vote couldn't be sent.";
    }
}

function sendSilverVote($mysqli, $project, $team)
{
    $preparedSQL = "update qwerty_pc_project_team
                    set silver = silver + 1
                    where project_id = ? and team_id = ?";
    if ($stmt = mysqli_prepare($mysqli, $preparedSQL)) {
        mysqli_stmt_bind_param($stmt, "ii", $project, $team);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_close($stmt);
    } else {
        echo "silver vote couldn't be sent.";
    }
}

function sendBronzeVote($mysqli, $project, $team)
{
    $preparedSQL = "update qwerty_pc_project_team
                    set bronze = bronze + 1
                    where project_id = ? and team_id = ?";
    if ($stmt = mysqli_prepare($mysqli, $preparedSQL)) {
        mysqli_stmt_bind_param($stmt, "ii", $project, $team);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_close($stmt);
    } else {
        echo "bronze vote couldn't be sent.";
    }
}

function updateVoted($mysqli, $user, $project) 
{
    $preparedSQL = "insert into qwerty_pc_voted
                    values (?, ?)";
    if ($stmt = mysqli_prepare($mysqli, $preparedSQL)) {
        mysqli_stmt_bind_param($stmt, "si", $user, $project);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_close($stmt);
    }
}

?>