<?php

require_once('dbconnect.php');
require_once('helpers.php');

// get all projects
$projects = getProjects($mysqli);
// create user objects with point attribute
$users = setUsers($mysqli, $projects);

// look at each project individually
foreach ($projects as $project) {
    // get all votes for project
    $projectID = $project["id"];
    $votes = getVotes($mysqli, $projectID);
    //echo json_encode($votes);

    // put users in array to be sorted
    $tempUsers = array();
    foreach ($users as $user) {
        $tempUsers []= [
            "username" => $user["username"],
            "full_name" => $user["full_name"],
            "points" => 0
        ];
    }

    // calculate vote totals for each user
    foreach ($votes as $vote) {
        foreach ($tempUsers as &$user) {
            if ($vote["user_id"] == $user["username"]) {
                $user['points'] = (int)$vote["gold"] * 4 + (int)$vote["silver"] * 2 + (int)$vote["bronze"];
            }
        }
    }

    // sort the users by points in desc order
    $sorted = array();
    foreach ($tempUsers as $temp) {
        $sorted []= $temp;
    }
    usort($sorted, "comp");

    // loop through sorted users and assign awards (needs to be at least 1)
    $gold = 1;
    $silver = 1;
    $bronze = 1;
    foreach($sorted as $user) {
        // check for gold
        if ($user["points"] >= $gold) {
            $users->{$user["username"]}{"projects"}{$projectID}{"gold"} = True;
            $gold = $user["points"];
            //echo json_encode($user);
        } else {
            // check for silver
            if ($user["points"] >= $silver) {
                $users->{$user["username"]}{"projects"}{$projectID}{"silver"} = True;
                $silver = $user["points"];
            } else {
                // check for bronze
                if ($user["points"] >= $bronze) {
                    $users->{$user["username"]}{"projects"}{$projectID}{"bronze"} = True;
                    $bronze = $user["points"];
                }
            }
        }
    }
    //break;
}

// set the results and echo
echo json_encode($users);

function setUsers($mysqli, $projects) {
    $users = (object)[];

    foreach (getNonAdminUsers($mysqli) as $user) {
        //create the user
        $users->$user["username"] = [
            "username" => $user["username"],
            "full_name" => $user["full_name"],
            "icon_url" => $user["iconUrl"],
            "subtitle" => $user["subtitle"],
        ];
        // add all projects by id with awards set to false
        foreach ($projects as $project) {
            $username = $user["username"];
            $id = $project["id"];
            //$team_id = getUsersTeamID();
            $users->{$username}{"projects"}{$id} = [
                "name" => $project["name"],
                "gold" => False,
                "silver" => False,
                "bronze" => False,
                //"comments" => getAllComments($mysqli, $id, 3)
                "comments" => getSimpleComments($mysqli, $id, $username)
            ];
        }
    }
    return $users;
}

function comp($a, $b) {
    if ($a["points"] == $b["points"]) { return 0; }
    else if ($a["points"] < $b["points"]) { return 1; }
    else { return -1; }
}

function getVotes($mysqli, $projectID) {
    // TODO: prepared statement
    // $sql = "select UT.user_id, PT.project_id, PT.gold, PT.silver, PT.bronze
    //         from qwerty_pc_project_team PT, qwerty_pc_user_team UT
    //         where PT.team_id=UT.team_id and PT.project_id=" . $projectID;
    // $returnedResults = $mysqli->query($sql);
    // while ($result = $returnedResults->fetch_assoc()) {
    //     $results []= $result;
    // }

    $preparedsql = "select UT.user_id, PT.project_id, PT.gold, PT.silver, PT.bronze
             from qwerty_pc_project_team PT, qwerty_pc_user_team UT
             where PT.team_id=UT.team_id and PT.project_id=?";
    if($stmt = mysqli_prepare($mysqli, $preparedsql)){
        mysqli_stmt_bind_param($stmt, "i", $projectID);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_bind_result($stmt, $user_id, $project_id, $gold, $silver, $bronze);  
        $results = array();
        while(mysqli_stmt_fetch($stmt)){
            $project = array("user_id" => $user_id, "project_id" => $project_id, "gold" => $gold, "silver"=> $silver, "bronze"=> $bronze); 
            $results []= $project;
        }
        mysqli_stmt_close($stmt);
    }

    return $results;
}

function getSimpleComments($mysqli, $id, $username)
{
    $comments = array();
    foreach (getUsersComments($mysqli, $id, $username) as $comment)
    {
        $comments []= $comment["comment"];
    }
    return $comments;
}

function getAllComments($mysqli, $project_id, $team_id)
{
    $preparedSQL = "select comment
                    from qwerty_pc_writein
                    where project_id = ? and team_id = ?";
    if ($stmt = mysqli_prepare($mysqli, $preparedSQL)) {
        mysqli_stmt_bind_param($stmt, "ii", $project_id, $team_id);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_bind_result($stmt, $comment);
        $comments = array();
        while (mysqli_stmt_fetch($stmt)) {
            $comments []= $comment;
        }
        mysqli_stmt_close($stmt);
    }
    return $comments;
}

?>