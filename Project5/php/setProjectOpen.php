<?php

session_start();
if(!isset($_SESSION['role'] ) || $_SESSION["role"] !== 'admin') {
    die("Not authorized");
}

require_once('dbconnect.php');
require_once('helpers.php');

setProjectOpen($mysqli, $_POST["project_id"]);

$mysqli->close();

?>
