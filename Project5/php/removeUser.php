<?php

session_start();
if(!isset($_SESSION['role'] ) || $_SESSION["role"] !== 'admin') {
    die("Not authorized");
}

require_once('dbconnect.php');
require_once('helpers.php');

removeUser($mysqli, $_POST["username"]);

$mysqli->close();

function removeUser($mysqli, $username) {
    
    //Grabs the teams that the user is on
    $preparedTeamsSQL = "SELECT team_id 
                        FROM qwerty_pc_user_team AS ut
                        WHERE ut.user_id=?";

    $teamList = array();
    if($teamIdStmt = mysqli_prepare($mysqli, $preparedTeamsSQL)){
        mysqli_stmt_bind_param($teamIdStmt, 's', $username);
        mysqli_stmt_execute($teamIdStmt);
        mysqli_stmt_bind_result($teamIdStmt, $teamID);
        
        while(mysqli_stmt_fetch($teamIdStmt)){
            $teamList []= array("team_id" => $teamID); 
        }
        mysqli_stmt_close($teamIdStmt);      
    }   
    
    //Deletes user from user_team
    $preparedDeleteUserTeamSQL = "DELETE FROM qwerty_pc_user_team WHERE user_id=?";
    if($deleteUserTeamStmt = mysqli_prepare($mysqli, $preparedDeleteUserTeamSQL)){
        echo "got to ut";
        mysqli_stmt_bind_param($deleteUserTeamStmt, 's', $username);
        mysqli_stmt_execute($deleteUserTeamStmt);
        mysqli_stmt_close($deleteUserTeamStmt);
    }  

    //Deletes from voted
    $preparedDeleteVotedSQL = "DELETE FROM qwerty_pc_voted WHERE user_id=?";
    if($deleteVotedStmt = mysqli_prepare($mysqli, $preparedDeleteVotedSQL)){
        echo "got to voted";
        mysqli_stmt_bind_param($deleteVotedStmt, 's', $username);
        mysqli_stmt_execute($deleteVotedStmt);
        mysqli_stmt_close($deleteVotedStmt);
    }  
    var_dump($teamList);
    //Checks to see if the there is another user on the team. Deletes the team and project_team
    foreach($teamList as $teams){
        $prepareTeamCheckSQL = "SELECT COUNT(1) 
                                FROM qwerty_pc_user_team
                                WHERE team_id=?";
        if($teamCheckStmt = mysqli_prepare($mysqli, $prepareTeamCheckSQL)){
            echo "got to select";
            mysqli_stmt_bind_param($teamCheckStmt, 'i', $teams);
            mysqli_stmt_execute($teamCheckStmt);
            mysqli_stmt_bind_result($teamCheckStmt, $exists);
            mysqli_stmt_fetch($teamCheckStmt);
            var_dump($exists);
            mysqli_stmt_close($teamCheckStmt);      
            
            //Deletes the team from Project team, writein, and then from team
            if($exists == 0){
                $preparedDeleteTeamsfromProjectSQL = "DELETE FROM qwerty_pc_project_team WHERE team_id=?";
                if($deleteProjectTeamStmt = mysqli_prepare($mysqli, $preparedDeleteTeamsfromProjectSQL)){
                    echo "Got to pt";
                    mysqli_stmt_bind_param($deleteProjectTeamStmt, 'i', $teams);
                    mysqli_stmt_execute($deleteProjectTeamStmt);
                    mysqli_stmt_close($deleteProjectTeamStmt);
                } 

                $preparedDeleteWriteinSQL = "DELETE FROM qwerty_pc_writein WHERE team_id=?";
                if($deleteWriteinStmt = mysqli_prepare($mysqli, $preparedDeleteWriteinSQL)){
                    echo "got to w";
                    mysqli_stmt_bind_param($deleteWriteinStmt, 'i', $teams);
                    mysqli_stmt_execute($deleteWriteinStmt);
                    mysqli_stmt_close($deleteWriteinStmt);
                } 

                $preparedDeleteTeamsSQL = "DELETE FROM qwerty_pc_team WHERE ID=?";
                if($deleteTeamStmt = mysqli_prepare($mysqli, $preparedDeleteTeamsSQL)){
                    echo "got to team";
                    mysqli_stmt_bind_param($deleteTeamStmt, 'i', $teams);
                    mysqli_stmt_execute($deleteTeamStmt);
                    mysqli_stmt_close($deleteTeamStmt);
                } 

            }
        }
    }

    $preparedUserDeleteSQL = "DELETE FROM qwerty_pc_user WHERE username=?";
    
    if($stmt = mysqli_prepare($mysqli, $preparedUserDeleteSQL)){
        echo "got to user";
        mysqli_stmt_bind_param($stmt, "s", $username);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_close($stmt);
    }
}

?>