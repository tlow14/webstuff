<?php
    require_once('dbconnect.php');
    require_once('helpers.php');

    $json = json_encode(getTeamsById($mysqli, isProjectOpen($mysqli), currentCourse($mysqli)));
    echo $json;
?>