<?php

$link = mysqli_connect('james.cedarville.edu', 'cs3220', '', 'cs3220_Sp20') or die('Database connect error.');

//Sees what is posted from project viewer and chooses the correct function
if(!empty($_POST["username"])){
    echo json_encode(getUrlbyUsername($link, $_POST["username"]));
}else if(!empty($_POST["project_id"])){
    echo json_encode(getUrlbyProject($link, $_POST["project_id"]));
}else{
    echo "Something went wrong";
}

$link->close();

//Grabs all the Urls of a given username
function getUrlbyUsername($mysqli, $username){
    $preparedsql= "SELECT pt.project_id, pt.url, u.full_name
                    FROM qwerty_pc_project_team AS pt
                    INNER JOIN qwerty_pc_user_team AS ut ON ut.team_id = pt.team_id
                    INNER JOIN qwerty_pc_user AS u ON u.username= ut.user_id
                    WHERE ut.user_id=?";

    if($stmt = mysqli_prepare($mysqli, $preparedsql)){
        mysqli_stmt_bind_param($stmt, 's', $username);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_bind_result($stmt, $projectId, $url, $full_name);
        while(mysqli_stmt_fetch($stmt)){
            $result []= array("url" => $url, "project_id"=>$projectId, "full_name"=> $full_name );
        }
        
    }
    return $result;
}

//Grabs all the URLs in a given project
function getUrlbyProject($mysqli, $project_id){
    $preparedsql = "SELECT username, full_name, url
                    FROM qwerty_pc_project_team AS pt
                    INNER JOIN qwerty_pc_user_team AS ut ON ut.team_id = pt.team_id
                    INNER JOIN qwerty_pc_user AS u ON u.username = ut.user_id
                    WHERE project_id =?";

    if($stmt = mysqli_prepare($mysqli, $preparedsql)){
        mysqli_stmt_bind_param($stmt, 'i', $project_id);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_bind_result($stmt, $username, $full_name, $url);
        while(mysqli_stmt_fetch($stmt)){
            $result []= array("url" => $url, "username"=>$username, "full_name"=>$full_name);
        }
    }
    return $result;
}   
?>