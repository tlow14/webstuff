<?php

require_once('dbconnect.php');
require_once('helpers.php');

// get tuples from qwerty_pc_project_team
$results = getResults($mysqli, $_POST["projectID"]);
echo json_encode($results);

function getResults($mysqli, $projectID) {
    $project = $projectID;
    if (!$project || $project == "null") {
        $project = isProjectOpen($mysqli);
    }
    // TODO: convert to prepared

    $preparedsql = "SELECT  U.full_name, UT.user_id, PT.project_id, PT.gold, PT.silver, PT.bronze
                    from qwerty_pc_user_team UT
                    inner join qwerty_pc_user U on U.username=UT.user_id
                    inner join qwerty_pc_project_team PT on UT.team_id=PT.team_id
                    where PT.project_id=?";
    if($stmt = mysqli_prepare($mysqli, $preparedsql)){
        mysqli_stmt_bind_param($stmt, 'i', $project);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_bind_result($stmt, $full_name, $user_id, $project_id, $gold, $silver, $bronze);
    
        $results = array();
        while(mysqli_stmt_fetch($stmt)){
            $results []= array("full_name" => $full_name, "user_id" => $user_id, "project_id"=>$project_id, "gold"=>$gold, "silver"=>$silver, "bronze"=>$bronze); 
        }
        mysqli_stmt_close($stmt);
    }
    
    // $sql = "select U.full_name, UT.user_id, PT.project_id, PT.gold, PT.silver, PT.bronze
    //         from qwerty_pc_user_team UT
    //             inner join qwerty_pc_user U on U.username=UT.user_id
    //             inner join qwerty_pc_project_team PT on UT.team_id=PT.team_id
    //         where PT.project_id=" . $project;
    // $returnedResults = $mysqli->query($sql);
    // while ($result = $returnedResults->fetch_assoc()) {
    //     $results []= $result;
    // }
    return $results;
}

?>