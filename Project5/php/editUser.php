<?php
    require_once('dbconnect.php');
    require_once('helpers.php');

    editUser($mysqli,$_POST["username"], $_POST["full_name"], currentCourse($mysqli));
    function editUser($mysqli, $username, $fullname){
        $preparedsql = "UPDATE qwerty_pc_user
                        SET full_name=?
                        WHERE username=?";
        if($stmt = mysqli_prepare($mysqli, $preparedsql)){
            mysqli_stmt_bind_param($stmt, 'ss', $fullname,  $username);
            mysqli_stmt_execute($stmt);
            mysqli_stmt_close($stmt);
        }
    }
?>
