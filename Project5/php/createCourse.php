<?php
session_start();
if(!isset($_SESSION['role'] ) || $_SESSION["role"] != 'admin') {
    die("Not authorized");
}

require_once('dbconnect.php');
require_once('helpers.php');

createCourse($mysqli, $_POST["courseName"], $_POST["projectCount"]);

$mysqli->close();

function createCourse($mysqli, $courseName, $projectCount) {
    if($courseName == null) die("Need a course name.");

    $preparedSQL = "insert into qwerty_pc_course(ID, name) values(DEFAULT, ?)";

    if($stmt = mysqli_prepare($mysqli, $preparedSQL)){
        // Bind variables to the prepared statement as parameters
        mysqli_stmt_bind_param($stmt, "s", $courseName);

        mysqli_stmt_execute($stmt);
        mysqli_stmt_close($stmt);
    }

    // Now I need to get the ID of the just created course, there is probably a better way to do this
    // but since the ID is auto incrementing I don't now how to insert a specific ID safely.
    // I'm going to query the DB again and look for the name that was just created
    $preparedSQL = "SELECT ID from qwerty_pc_course where name=? order by ID limit 1";
    if($stmt = mysqli_prepare($mysqli, $preparedSQL)){
        mysqli_stmt_bind_param($stmt, "s", $courseName);
        mysqli_stmt_execute($stmt);
        mysqli_stmt_bind_result($stmt, $id);
        while(mysqli_stmt_fetch($stmt)){
            $courseId = $id;
        }
        mysqli_stmt_close($stmt);
    }

    // Insert the specified amount of projects into the course
    for ($x = 1; $x <= $projectCount; $x++) {
        $preparedSQL = "insert into qwerty_pc_project(ID, name, course_id) values(DEFAULT, ?, ?)";
        if($stmt = mysqli_prepare($mysqli, $preparedSQL)){
            $projectName = 'Project ' . $x;
            // Bind variables to the prepared statement as parameters
            mysqli_stmt_bind_param($stmt, "si", $projectName, $courseId);

            mysqli_stmt_execute($stmt);
            mysqli_stmt_close($stmt);
        }
    }
}

?>