<?php

session_start();

require_once('php/dbconnect.php');
require_once('php/helpers.php');

$isLoggedIn = isset($_SESSION['username']) && $_SESSION["loggedin"] === true;
$isAdmin    = isset($_SESSION['role']) && $_SESSION["role"] === 'admin';

// echo $isLoggedIn;
// echo $isAdmin;
// // If the user isn't an admin redirect them!
// if(!isset($_SESSION['role'] ) || $_SESSION["role"] !== 'admin') {
//     header("location: index.php");
// }
?>
<html lang="en">

<head>
    <title>PC | Project Viewer</title>

    <?php
    require('header.php');
    ?>
   <script src="js/projectViewer.js"></script>

    <style> 
        .disabled-icon {
            opacity: 0.2;
            cursor: default
        }
    </style>
</head>

<body class="theme-light">
    <div class="flex flex-col w-screen h-screen">
        <div class="w-screen lg:h-12 flex flex-row justify-space bg-secondary">
            <div class="flex flex-row justify-center mt-2 lg:mt-0 lg:justify-start">
                <a href="index.php" class="button-primary my-auto ml-2 text-primaryText">Home</a>
                <a id="openInNewTab" class="button-primary my-auto ml-2 text-primaryText" aria-label="Opens Project in a New Tab" data-balloon-pos="down">New Tab</a>
            </div>
            <div class="flex justify-end m-2 lg:hidden">
                <button id="nav-toggle" class="flex items-center px-3 py-2 border rounded text-gray-500 cursor-pointer text-primaryText hover:text-secondaryText">
                    <i class="fa fa-bars" aria-hidden="true"></i>
                </button>
            </div>
            <div class="lg:flex lg:items-center lg:w-auto hidden lg:block pt-6 lg:pt-0 p-4 lg:p-0" id="nav-content">
                <ul class="list-reset lg:flex justify-end flex-1 items-center">
                    <?php
                    if ($isLoggedIn) {
                    ?>
                    <li class="mb-2 lg:mr-2 lg:mb-0 h-10 lg:h-auto mx-auto lg:mx-0 sm:w-1/2 xs:w-full lg:w-auto">
                        <a href="profile.php" class="flex button-primary text-primaryText">Profile</a>
                    </li>

                        <?php
                            require_once('php/dbconnect.php');
                        ?>
                    <?php
                        if (isProjectOpen($mysqli) != "false") {
                    ?>
                        <li class="mb-2 lg:mr-2 lg:mb-0 h-10 lg:h-auto mx-auto lg:mx-0 sm:w-1/2 xs:w-full lg:w-auto">
                            <a href="vote.php" class="flex button-primary text-primaryText">Vote</a>
                        </li>
                        <?php
                            }
                        ?>
                    <?php
                    }
                    ?>

                    <li class="mb-2 lg:mr-2 lg:mb-0 h-10 lg:h-auto mx-auto lg:mx-0 sm:w-1/2 xs:w-full lg:w-auto">
                        <a href="viewResults.php" class="flex button-primary text-primaryText">View Results</a>
                    </li>
                    <li class="mb-2 lg:mr-2 lg:mb-0 h-10 lg:h-auto mx-auto lg:mx-0 sm:w-1/2 xs:w-full lg:w-auto">
                        <a href="projectViewer.php" class="flex button-primary text-primaryText">View Projects</a>
                    </li>
                    <?php
                    if ($isAdmin) {
                    ?>
                    <li class="mb-2 lg:mr-2 lg:mb-0 h-10 lg:h-auto mx-auto lg:mx-0 sm:w-1/2 xs:w-full lg:w-auto">
                        <a href="admin.php" class="flex button-primary text-primaryText">Admin</a>
                    </li>
                    <?php
                    }
                    ?>
                                <?php
                    if ($isLoggedIn) {
                    ?>
                    <li class="h-10 lg:ml-2 mb-2 lg:mr-2 lg:mb-0 h-10 lg:h-auto mx-auto lg:mx-0 sm:w-1/2 xs:w-full lg:w-auto">
                        <a id="logoutButton" href="logout.php" class="flex button-primary text-primaryText">Logout</a>
                    </li>
                    <?php
                    }
                    ?>
                </ul>
            </div>
        </div>
        <div class="flex flex-row justify-center mx-auto w-auto lg:w-2/3">
            <div class="flex flex-row">
                <input id="amountOfProjects" hidden value="<?php echo htmlentities(count(getProjects($mysqli)))?>"/>
                <div id="leftTooltip" class="justify-start" aria-label="Project Controls" data-balloon-pos="down-right">
                    <i id="navLeft" class="fa fa-arrow-left text-2xl my-auto p-2 cursor-pointer ml-10 text-primaryText" aria-hidden="true"></i>
                </div>
                <h2 aria-label="Open in New Tab" data-balloon-pos="down" class="text-center select-none text-sm w-full mx-4 text-primaryText cursor-pointer">Change Project</h2>
                <div id="rightTooltip" class="justify-end" aria-label="Project Controls" data-balloon-pos="down-left">
                    <i id="navRight" class="fa fa-arrow-right text-2xl my-auto p-2 cursor-pointer mr-10 text-primaryText" aria-hidden="true"></i>
                </div>
            </div>
            <div class="flex flex-row w-full">
                <h2 id="siteTitle" aria-label="Open in New Tab" data-balloon-pos="down" class="text-center select-none text-2xl w-full mx-10 text-primaryText my-auto cursor-pointer">Project Viewer</h2>
            </div>
            <div class="flex flex-row">
                <div id="leftUserTooltip" class="justify-start" aria-label="User Controls" data-balloon-pos="down-right">
                    <i id="navUserLeft" class="fa fa-arrow-left text-2xl my-auto p-2 cursor-pointer ml-10 text-primaryText" aria-hidden="true"></i>
                </div>
                <h2 aria-label="Open in New Tab" data-balloon-pos="down" class="text-center select-none text-sm w-full mx-4 text-primaryText cursor-pointer">Change User</h2>
                <div id="rightUserTooltip" class="justify-end" aria-label="User Controls" data-balloon-pos="down-left">
                    <i id="navUserRight" class="fa fa-arrow-right text-2xl my-auto p-2 cursor-pointer mr-10 text-primaryText" aria-hidden="true"></i>
                </div>
            </div>
        </div>

        <div class="w-screen h-full">
            <iframe id="siteIframe" class="w-screen h-full" src="viewerStart.php"></iframe>
        </div>
    </div>
        <script>
            // Javascript to toggle the menu
            document.getElementById('nav-toggle').onclick = function(){
                document.getElementById("nav-content").classList.toggle("hidden");
            }
        </script>
</body>

</html>