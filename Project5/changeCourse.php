<?php
session_start();

// If the user isn't an admin redirect them!
if(!isset($_SESSION['role'] ) || $_SESSION["role"] !== 'admin') {
    header("location: index.php");
}

if($_SERVER["REQUEST_METHOD"] == "POST") {
    if(!empty(trim($_POST["course"]))) {
        $_SESSION["course_id"] = $_POST["course"];
        
        $course_msg = "Successfully changed the course!";
    }
}

?>
<html lang="en">

<head>
    <title>PC | Change Course</title>
    <?php require('header.php'); ?>
    <script src="js/changeCourse.js"></script>
</head>

<body class="theme-light page-background font-sans">
<div class="card-header flex justify-center text-ternaryText text-4xl bg-secondary">
        <a href="admin.php" class="circled back-button m-2 text-primary border-primary">
            <i class="h-10 w-10" aria-label="Admin Page" data-balloon-pos="right">
                <i class="fa fa-arrow-left cursor-pointer" aria-hidden="true"></i>
            </i>
        </a>
    <h3 class="text-center text-primary p-4">Change Course</h3>
</div>
<div class="<?php echo htmlentities((!empty($course_msg))) ? '' : 'hidden'; ?> bg-green-400 z-10 border border-green-400 text-white px-4 py-3 rounded relative my-0 flex flex-row justify-between"><div><p class="font-bold">Notice!</p><p class="text-sm"><?php echo htmlentities($course_msg); ?></p></div><div><i class="fa fa-times-circle text-lg cursor-pointer m-4" aria-hidden="true" onclick="this.parentElement.parentElement.style.display='none';"></i></div></div>
<div class="main-wrapper content-center flex flex-col mx-auto w-screen">
    <div class="w-2/3 p-2 mx-auto bg-secondary mt-2 rounded-lg">
        <h2 class="text-center text-primaryText text-2xl mb-1">Select Course</h2>
        <input hidden id="currentCourseId" type="text" value="<?php echo htmlentities($_SESSION["course_id"]); ?>"/>
        <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
            <select value="course" class="courseList text-primaryText cursor-pointer block appearance-none w-full bg-secondary border border-gray-400 hover:border-gray-500 px-4 py-2 pr-8 rounded shadow leading-tight focus:outline-none focus:shadow-outline">
            </select>
            <input hidden name="course" id="courseID" type="text"/>
            <input type="submit" value="Change Course" class="button-secondary mt-2"/>
        </form>
    </div>
</div>
</body>

</html>