// This is the configuration file for tailwind css
module.exports = {
    theme: {
        extend: {
            transitionDuration: {
                '25': '25ms',
                '50': '50ms',
            },
            scale: {
                '101': '1.01',
                '102': '1.02',
                'barely': '1.02',
            },
            width: {
                'third--1': 'calc(33.33333% - 1rem)',
                'half--1': 'calc(50% - 1rem)',
                '2third--1': 'calc(66.66666% - 1rem)',
                'screen-1': 'calc(100vw - 1rem)',
                'full-1': 'calc(100% - 1rem)',
            },

            // Note that all the colors are using CSS variables defined in css/tailwind/tailwind.css
            // these variables make it very easy to switch between dark mode and any other future
            // color schemes
            colors: {
                'primary': 'var(--bg-background-primary)',
                'secondary': 'var(--bg-background-secondary)',
                'ternary': 'var(--bg-background-ternary)',

                'primaryText': 'var(--text-copy-primary)',
                'secondaryText': 'var(--text-copy-secondary)',
                'ternaryText': 'var(--text-copy-ternary)',

                'primaryBgText': 'var(--bg-background-primary)',

                'primaryComplement': 'var(--bg-primary-complement)',
                'primaryChromatic': 'var(--bg-primary-chromatic)',

                'gold': 'var(--gold-color)',
                'silver': 'var(--silver-color)',
                'bronze': 'var(--bronze-color)',
            },
            borderColor: {
                'primary': 'var(--bg-background-primary)',
                'secondary': 'var(--bg-background-secondary)',
                'ternary': 'var(--bg-background-ternary)',
            },
        },
    },
    variants: {},
    plugins: [],
}