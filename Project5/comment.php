<div id="commentWrapper" class="text-primaryText justify-center flex flex-col">
        <div id="templateComment" class="hidden sortTeam w-full mx-auto flex flex-col items-start card-footer my-2 max-w-4xl px-10 py-6 bg-secondary rounded-lg shadow-md">
            <div class="w-full">
                <p class="flex items-baseline">
                    <div id="profileIconList" class="flex flex-row items-center">
                    </div> 
                    <div id="commentNameList" class="w-full font-bold text-2xl">
                    </div>
                </p>
                <div id="commentList" class="mt-3">
                </div>
            </div>
        </div> 
    </div>