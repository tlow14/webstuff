<?php
session_start();

require_once('php/dbconnect.php');
require_once('php/helpers.php');

// If the user isn't an admin redirect them!
if(!isset($_SESSION['role'] ) || $_SESSION["role"] !== 'admin') {
    header("location: index.php");
}
?>
<html lang="en">

<head>
    <title>PC | Admin</title>

    <?php require('header.php'); ?>
    <script src="js/admin.js"></script>

    <style>
        input:checked+span {
            background-color: var(--bg-background-primary);
            color: var(--text-copy-ternary);
            box-shadow: 0 0 0 3px rgba(66, 153, 225, 0.5);
        }
        input[type='number']::-webkit-inner-spin-button,
        input[type='number']::-webkit-outer-spin-button {
            -webkit-appearance: none;
            margin: 0;
        }

        .custom-number-input input:focus {
            outline: none !important;
        }

        .custom-number-input button:focus {
            outline: none !important;
        }
    </style>
</head>

<body class="theme-light page-background font-sans">

    <!-- HEADER -->
    <div class="bg-secondary flex justify-between p-2 select-none">
    <div class="my-auto">
            <!-- <h3 class="text-primaryText font-bold text-2xl">Welcome, Admin</h3> -->
            <a href="index.php" class="ml-2 button-primary text-primaryText">Course Home</a>
            </div>
        <div class="flex flex-row">
            <a href="changeCourse.php" class="ml-2 button-primary text-primaryText">Change Course</a>
            <form action="vote.php" class="ml-2" method="post">
                <input type="submit" value="Vote" class="button-primary text-primaryText" />
            </form>
            <a href="viewResults.php" class="ml-2 button-primary text-primaryText">View Results</a>
            <a href="teamSelection.php" class="ml-2 button-primary text-primaryText">Team Selection</a>
        </div>
    </div>
    <div class="flex flex-col w-2/3 mx-auto">
        <div class="bg-secondary text-center rounded-lg p-2 m-2">
        <h2 class="mt-2 py-2 px-4 mx-auto font-bold text-primaryText rounded-lg text-2xl">Welcome, Admin</h2>
        <p class="text-secondaryText mx-auto font-light text-sm">Current Course: <?php 
        $courseList = currentCourseInfo($mysqli); 
            foreach ($courseList as $course) {
                echo htmlentities($course['name']);
            }
        ?>
        </br>
        Non-Admin Users: 
        <?php 
            echo htmlentities(count(getNonAdminUsers($mysqli)));
        ?>
        </br>
        Total Users: 
        <?php 
            echo htmlentities(count(getUsers($mysqli)));
        ?>
        </br>
        Amount of Projects: 
        <?php 
            echo htmlentities(count(getProjects($mysqli)));
        ?>
        </p>
        </div>
        <div class="bg-secondary px-10 pb-10 pt-4 m-6 flex flex-col rounded-lg">
            <h2 class="text-2xl pb-2 font-bold text-center">Open / Close Polls</h2>
            <div class="flex flex-col lg:flex-row">
                <select value="project" class="projectList text-primaryText cursor-pointer block appearance-none w-full bg-secondary border border-gray-400 hover:border-gray-500 px-4 py-2 pr-8 rounded shadow leading-tight focus:outline-none focus:shadow-outline">
                </select>
                <div>
                    <label class="inline-flex items-center" aria-label="Opens the selected project, and closes any open project" data-balloon-pos="up">
                        <input id="openCheckbox" value="close" class="hidden" type="radio" name="actionType">
                        <span class="text-center text-primaryText m-2 px-8 py-2 bg-ternary rounded-r-full rounded-l-full cursor-pointer font-bold">Open</span>
                      </label>
                </div>
                <div>
                    <label class="inline-flex items-center" aria-label="Closes the selected project" data-balloon-pos="up">
                        <input id="closeCheckbox" value="open" class="hidden" type="radio" name="actionType">
                        <span class="text-center text-primaryText m-2 px-8 py-2 bg-ternary rounded-r-full rounded-l-full cursor-pointer font-bold">Close</span>
                      </label>
                </div>
            </div>
        </div>
        <div class="bg-secondary px-10 pb-10 pt-4 m-6 flex flex-col rounded-lg">
            <h2 class="text-2xl pb-2 font-bold text-center">Create User</h2>
            <div class="flex justify-center flex-col lg:flex-row">
                <input id="addLinuxUsername" name="addLinuxUsername" class="w-64 h-10 mr-2 appearance-none block bg-ternary text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" type="text" placeholder="Linux Username">
                <input id="addFullName" name="addFullName" class="w-64 h-10 mr-2 appearance-none block bg-ternary text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" type="text" placeholder="Full Name">
                <input id="createUserSubmit" type="submit" class="button-secondary"/>
            </div>
        </div>
        <div class="bg-secondary px-10 pb-10 pt-4 m-6 flex flex-col rounded-lg">
            <h2 class="text-2xl pb-2 font-bold text-center">Edit Username</h2>
            <div class="flex flex-col lg:flex-row justify-center items-center">
                <select id="editusername" value="username" class="usernameList text-primaryText cursor-pointer block appearance-none bg-secondary border border-gray-400 hover:border-gray-500 px-4 py-2 pr-8 rounded shadow leading-tight focus:outline-none focus:shadow-outline">
                </select>
                <div class="ml-2">
                    <input id="editFullName" name="editFullName" class="w-64 h-10 mr-2 appearance-none block bg-ternary text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" type="text" placeholder="Full Name">
                </div>
                <input id="editUserSubmit" type="submit" class="button-secondary"/>
            </div>
        </div>


        <h2 class="bg-secondary text-center py-2 px-4 m-10 mx-auto font-bold text-red-400 rounded-lg text-4xl">Danger Zone <i class="fa fa-exclamation-triangle" aria-hidden="true"></i></h2>
        <div class="bg-secondary px-10 pb-10 pt-4 m-6 flex flex-col rounded-lg">
            <h2 class="text-2xl pb-2 font-bold text-center text-primaryText">User Management</h2>
            <!-- <h4 class="text-red-400 m-2 font-bold">Warning! This will require the user to login with the new password.</h4> -->
            <div class="flex flex-col lg:flex-row">
                <select id="usernameManagement" value="username" class="otherUserList text-primaryText cursor-pointer block appearance-none w-full bg-secondary border border-gray-400 hover:border-gray-500 px-4 py-2 pr-8 rounded shadow leading-tight focus:outline-none focus:shadow-outline">
                </select>
                <button id="setRoleAdmin" class="text-center bg-red-400 text-white py-2 px-8 rounded-r-full rounded-l-full m-2 cursor-pointer font-bold" aria-label="Sets the user's role to admin!" data-balloon-pos="up">Make Admin</button>
                <button id="setRoleUser" class="text-center bg-red-400 text-white py-2 px-8 rounded-r-full rounded-l-full m-2 cursor-pointer font-bold" aria-label="Sets the user's role to user" data-balloon-pos="up">Make User</button>
                <button id="resetPassword" class="text-center bg-red-400 text-white py-2 px-8 rounded-r-full rounded-l-full m-2 cursor-pointer font-bold" aria-label="Reset's the user's password to 'password'" data-balloon-pos="up">Reset Password</button>
                <button id="deleteUser" class="text-center bg-red-400 text-white py-2 px-8 rounded-r-full rounded-l-full m-2 cursor-pointer font-bold" aria-label="Deletes the user from the database" data-balloon-pos="up">Delete User</button>
            </div>
        </div>
        <div class="bg-secondary px-10 pb-10 pt-4 m-6 flex flex-col rounded-lg">
            <h2 class="text-2xl pb-2 font-bold text-center text-primaryText">Course Management</h2>
            <div class="flex justify-center flex-col lg:flex-row">
                <label for="custom-input-number" class="text-center text-sm font-semibold mr-2 text-secondaryText flex items-center">Project Count</label>
                <div class="custom-number-input mx-2 h-10 w-32">
                    <div class="flex flex-row h-10 w-full rounded-lg relative bg-transparent">
                        <button id="projectDecrement" class=" bg-gray-300 text-gray-600 hover:text-gray-700 hover:bg-gray-400 h-full w-20 rounded-l cursor-pointer outline-none">
                        <span class="m-auto text-2xl font-thin">−</span>
                        </button>
                        <input id="projectCount" type="number" class="outline-none focus:outline-none text-center w-full bg-gray-300 font-semibold text-md hover:text-black focus:text-black  md:text-basecursor-default flex items-center text-gray-700  outline-none" name="custom-input-number" value="<?php echo htmlentities(numberOfProjects($mysqli));?>"/>
                        <button id="projectIncrement" class="bg-gray-300 text-gray-600 hover:text-gray-700 hover:bg-gray-400 h-full w-20 rounded-r cursor-pointer">
                            <span class="m-auto text-2xl font-thin">+</span>
                        </button>
                    </div>
                </div>
                <input id="addCourseName" name="courseName" class="w-64 h-10 mx-4 appearance-none block bg-ternary text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" type="text" placeholder="Course Name">
                <!-- <button id="resetCourse" class="text-center bg-red-400 text-white ml-2 py-2 px-8 rounded-r-full rounded-l-full cursor-pointer font-bold">Reset Course</button> -->
                <button id="addCourse" class="text-center bg-red-400 text-white py-2 px-8 rounded-r-full rounded-l-full cursor-pointer font-bold" data-balloon-length="large" aria-label="Adds a new course to the database with the # of projects and name specified. Once the course is created it will be visible to all users and the projects will be autogenerated" data-balloon-pos="up">Add Course</button>
           </div>

           <div class="flex justify-center flex-col lg:flex-row pt-2">
           <select id="removeCourseList" value="course" class="courseList text-primaryText cursor-pointer block appearance-none bg-secondary border border-gray-400 hover:border-gray-500 px-4 py-2 pr-8 rounded shadow leading-tight focus:outline-none focus:shadow-outline">
            </select>
                <button id="removeCourse" class="text-center bg-red-400 text-white rounded-r-full ml-2 px-4 rounded-l-full cursor-pointer font-bold" data-balloon-length="large" aria-label="Completely wipes course data from the database. This includes all users, projects, teams, write-ins, etc." data-balloon-pos="up">Remove Course</button>
           </div>
        </div>
    </div>
</body>

</html>